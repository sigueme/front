export default function merge(obj1, obj2, merged = {}) {
  merged = Object.assign(merged, obj1);

  for (let attr in obj2) {
    if (obj1[attr] !== undefined && obj1[attr].constructor === Object) {
      merged[attr] = merge(obj1[attr], obj2[attr]);
    } else {
      merged[attr] = obj2[attr];
    }
  }

  return merged;
}
