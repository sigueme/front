import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import moment from 'moment';

import DeviceStatusBadge from '../../components/DeviceStatusBadge';

class Info extends Component {
  render() {
    const { device, t } = this.props;
    const { fields } = device;
    const {
      code,
      name,
      description,
      status,
      last_pos,
      last_update,
      dynamic
    } = device;
    const googleMapLink = 'https://maps.google.com/maps/place/'+last_pos.lat+','+last_pos.lon;

    return (
      <div className="container-fluid" style={{ marginTop: '20px' }}>
        <div className="media">
          <div className="media-left">
            <img
              alt="64x64"
              className="media-object"
              src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWRhZWUwNDEwNyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1ZGFlZTA0MTA3Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi44Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg=="
              style={{ width: '64px', height: '64px' }}
            />
          </div>
          <div className="media-body">
            <div className="media-heading">{name || code}</div>
            <div><small>{description}</small></div>
          </div>
          <div className="media-right" style={{ textAlign: 'right' }}>
            <Link
              style={{ marginBottom: '10px' }}
              to={`/device/${device.id}/update`}
              className="btn btn-primary btn-ghost"
            >
              Editar
            </Link>

            <Link
              to={`/device/${device.id}/delete`}
              className="btn btn-grapefruit btn-ghost"
            >
              Borrar
            </Link>
          </div>
        </div>

        <br />

        <div className="info-presenter">
          <div className="info-title">{t('devices.code')}</div>
          <div className="info-content">{api.org.code}{code}</div>
        </div>
        <div className="info-presenter">
          <div className="info-title">{t('devices.status')}</div>
          <div className="info-content">
            <DeviceStatusBadge device={device} />
          </div>
        </div>
        <div className="info-presenter">
          <div className="info-title">{t('devices.location')}</div>
          <div className="info-content">
            <a target="_blank" href={googleMapLink}>{last_pos.lat.toFixed(4) + ',' + last_pos.lon.toFixed(4)}</a>
          </div>
        </div>
        <div className="info-presenter">
          <div className="info-title">{t('devices.last_report')}</div>
          <div className="info-content">{moment(last_update).format('YYYY-MM-DD HH:mm:ss')}</div>
        </div>

        {fields.filter(field => dynamic[field.id]).map(field => (
          <div key={field.id} className="info-presenter">
            <div className="info-title">{field.label}</div>
            <div className="info-content">{dynamic[field.id]}</div>
          </div>
        ))}
        <br />
      </div>
    );
  }
}

export default withTranslation()(Info);
