class PermissionTree {
  constructor(permissions = []) {
    this.head = this._newNode();

    permissions.forEach(permission => this._append(permission));
  }

  _newNode() {
    return {
      allowedActions: new Set(),
      childs: new Map()
    };
  }

  _getChild(childName, node, append=true) {
    let child = node.childs.get(childName);

    if (child !== undefined) {
      return child;
    }

    if (append === false) {
      return null;
    }

    child = this._newNode();
    node.childs.set(childName, child);

    return child;
  }

  _append(literal) {
    let pieces = literal.split('/');
    let parts = pieces[0].split(':');
    let verb;

    if (pieces.length == 1) {
      verb = 'admin';
    } else {
      verb = pieces[1];
    }

    let node = this.head;

    for (let i=0; i<parts.length; i++) {
      node = this._getChild(parts[i], node);
    }

    node.allowedActions.add(verb);
  }

  getNode(key) {
    let parts = key.split(':');

    let node = this.head;
    for (let part of parts) {
      node = this._getChild(part, node, false);

      if (node === null) {
        return null;
      }
    }

    return node;
  }

  hasChilds(key) {
    return !!this.getNode(key);
  }

  allowedActions(key) {
    let parts = key.split(':');

    let permissions = new Set();
    let node = this.head;
    for (let i=0; i<parts.length; i++) {
      node = this._getChild(parts[i], node, false);

      if (node === null) {
        break;
      }

      for (let item of node.allowedActions) {
        if (item === 'admin') {
          return ['admin'];
        }

        permissions.add(item);
      }
    }

    return [...permissions];
  }
}

export default PermissionTree;
