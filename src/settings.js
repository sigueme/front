// This file takes settings from env and returns it to the loader
const settings = window.settings || {};

export default settings;
