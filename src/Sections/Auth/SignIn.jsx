import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { Header } from '../../Partials/Layout';
import { AjaxButton } from '../../Partials/Helpers';
import PageTitle from '../../components/PageTitle';

class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      rememberMe: false,
      showPassword: false,
      errors: [],
      loading: false
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    if (this.state.loading) {
      return;
    }

    const credentials = {
      email: this.state.email,
      password: this.state.password
    };

    this.setState({
      loading: true,
      errors: []
    });

    api.signin(credentials)
      .then(body => {
        this.setState({
          loading: false
        });
      }).catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        let state = {
          loading: false,
          errors: []
        };

        errors.forEach(error => {
          if (!error.field) {
            state.errors.push(error);
            return;
          }

          state[error.field + 'Error'] = error;
        });

        this.setState(state);
      });
  }

  renderEmail() {
    const { state } = this;
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (state.emailError) {
      className += ' has-error';
      helpBlock = t(state.emailError.i18n);
    }

    return (
      <div className={className}>
        <label htmlFor="inputEmail" className="control-label">
          { t('forms.email') }
        </label>
        <input
          id="inputEmail"
          type="text"
          className="form-control with-border"
          placeholder={ t('forms.email_placeholder') }
          autoFocus="autofocus"
          value={state.email}
          onChange={event => this.setState({
            email: event.target.value,
            emailError: null
          })}
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  renderPassword() {
    const { state } = this;
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (state.passwordError) {
      className += ' has-error';
      helpBlock = t(state.passwordError.i18n);
    }

    return (
      <div className={className}>
        <small className="pull-right">
          <div className="checkbox" style={{ margin: '0' }}>
            <label>
              <input
                type="checkbox"
                tabIndex="-1"
                checked={this.state.showPassword}
                onChange={event => this.setState({ showPassword: event.target.checked })}
              /> { t('forms.password_show') }
            </label>
          </div>
        </small>
        <label htmlFor="inputPassword" className="control-label">
          { t('forms.password') }
        </label>
        <input
          id="inputPassword"
          type={this.state.showPassword ? "text" : "password"}
          className="form-control with-border"
          placeholder={ t('forms.password_placeholder') }
          value={this.state.password}
          onChange={event => this.setState({
            password: event.target.value,
            passwordError: null
          })}
        />
        <p className="help-block">{helpBlock}</p>
        <div className="help-block">
          <Link to="forgot-password">
            { t('sign_in.forgot_password') }
          </Link>
        </div>
      </div>
    );
  }

  render() {
    const { t } = this.props;

    return (
      <div>
        <PageTitle title={t("sign_in.title")} />
        <Header />

        <div className="container container-micro">
          <div className="panel">
            <div className="panel-body">
              <h3>
                { t('sign_in.title') }
              </h3>
              <br />
              {this.state.errors.map(error => (
                <div className="alert alert-danger">
                  <i className="material-icons">info</i>
                  &nbsp;
                  {t(error.i18n)}
                </div>
              ))}
              <form onSubmit={this.handleSubmit.bind(this)}>

                {this.renderEmail()}
                {this.renderPassword()}

                <br />
                <div className="pull-right">
                  <AjaxButton
                    type="submit"
                    loading={this.state.loading}
                    className="btn btn-lg btn-primary"
                    content={t('sign_in.submit')}
                    loadingClassName="btn-loading"
                    loadingContent={t('sign_in.signing')}
                  />
                </div>
                <div className="checkbox pull-left" style={{ margin: '12.5px 0' }}>
                  <label>
                    <input
                      type="checkbox"
                      tabIndex="-1"
                      checked={this.state.rememberMe}
                      onChange={event => this.setState({ rememberMe: event.target.checked })}
                    /> { t('sign_in.remember_me') }
                  </label>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(SignIn);
