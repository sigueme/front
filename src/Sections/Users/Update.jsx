import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import settings from '../../settings';
import { Sidebar } from '../../Partials/Layout';
import Loading from '../../Partials/Loading';

class Update extends Component {
  constructor(props) {
    super(props);

    const { user } = this.props;

    this.state = {
      name: user.name,
      lastName: user.last_name,
      email: user.email,
      password: settings.fakepassword,
      loading: false,
      errors: []
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    if (this.state.loading) {
      return;
    }

    const params = {
      name: this.state.name,
      last_name: this.state.lastName,
      email: this.state.email
    };

    if (this.state.password !== settings.fakepassword) {
      params.password = this.state.password;
    }

    this.setState({
      loading: true,
      errors: []
    });

    const { user } = this.props;
    api.put(`/v1/:organization/user/${user.id}`, params)
      .then(body => {
        this.props.history.push('/users');
      }).catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        let state = {
          loading: false,
          errors: []
        };

        errors.forEach(error => {
          if (!error.field) {
            state.errors.push(error);
            return;
          }

          state[error.field + 'Error'] = error;
        });

        this.setState(state);
      });
  }

  renderName() {
    const { state } = this;
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = [];

    if (state.nameError || state.last_nameError) {
      className += ' has-error';
    }

    if (state.nameError) {
      helpBlock.push(t(state.nameError.i18n));
    }

    if (state.last_nameError) {
      helpBlock.push(t(state.last_nameError.i18n));
    }

    helpBlock = helpBlock.join(', ');

    return (
      <div className={className}>
        <label htmlFor="inputName" className="control-label">
          { t('forms.fullname') }
        </label>
        <div className="row">
          <div className="col-xs-6">
            <input
              id="inputName"
              type="text"
              className="form-control"
              placeholder={ t('forms.name') }
              value={state.name}
              onChange={event => this.setState({
                name: event.target.value,
                nameError: null
              })}
            />
          </div>
          <div className="col-xs-6">
            <input
              id="inputLastName"
              type="text"
              className="form-control"
              placeholder={ t('forms.lastName') }
              value={state.lastName}
              onChange={event => this.setState({
                lastName: event.target.value,
                last_nameError: null
              })}
            />
          </div>
        </div>
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  renderEmail() {
    const { state } = this;
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (state.emailError) {
      className += ' has-error';
      helpBlock = t(state.emailError.i18n);
    }

    return (
      <div className={className}>
        <label htmlFor="inputSitename" className="control-label">
          { t('forms.email') }
        </label>
        <input
          id="inputSitename"
          type="text"
          className="form-control"
          placeholder={ t('forms.email_placeholder') }
          value={state.email}
          onChange={event => this.setState({
            email: event.target.value,
            emailError: null
          })}
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  renderPassword() {
    const { state } = this;
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (state.passwordError) {
      className += ' has-error';
      helpBlock = t(state.passwordError.i18n);
    }

    return (
      <div className={className}>
        <label htmlFor="inputSitename" className="control-label">
          { t('forms.password') }
        </label>
        <input
          id="inputSitename"
          type="password"
          className="form-control"
          placeholder={ t('sign_up.password_hint') }
          value={state.password}
          onFocus={event => this.setState({ password: '' })}
          onChange={event => this.setState({
            password: event.target.value,
            passwordError: null
          })}
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  render() {
    const { t } = this.props;

    return (
      <div>
        <Sidebar />

        <div className="has-sidebar">
          <div className="container container-small">
            <h3>{ t('users.update') }</h3>
            <hr />
            {this.state.errors.map(error => (
              <div key={error.i18n} className="alert alert-danger">
                <i className="material-icons">info</i>
                &nbsp;
                { t(error.i18n) }
              </div>
            ))}
            <form onSubmit={this.handleSubmit.bind(this)}>

              {this.renderName()}
              {this.renderEmail()}
              {this.renderPassword()}

              <br />
              <div className="pull-right">
                <a href="javascript:void(0);" onClick={event => this.props.history.goBack()}>
                  {t('commons.cancel')}
                </a>
                &nbsp;
                <button
                  type="submit"
                  disabled={this.state.loading}
                  className="btn btn-lg btn-primary"
                >
                  { t('users.save') }
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    const { id } = this.props.match.params;

    api.get(`/v1/:organization/user/${id}`)
      .then(body => {
        this.setState({
          loading: false,
          user: body.data
        });
      })
      .catch(err => {
        console.error(err);

        this.setState({
          loading: false,
          error: true
        });
      });
  }

  render() {
    if (this.state.loading) {
      return <Loading />;
    }

    const { props } = this;
    const { t } = this.props;

    if (this.state.error) {
      return (
        <div>
          <Sidebar />

          <div className="has-sidebar">
            <div className="container container-small list-empty">
              <h2>{ t('errors.http404.title') }</h2>
              { t('errors.http404.description') }
              <br />
              <button
                onClick={event => this.props.history.goBack()}
                className="btn btn-lg btn-mint"
              >{ t('helpers.back_button') }</button>
            </div>
          </div>
        </div>
      );
    }

    return (
      <Update
        user={this.state.user}
        {...props}
      />
    )
  }
};

export default withTranslation()(Container);
