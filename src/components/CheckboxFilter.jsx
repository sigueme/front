import React, { Component } from 'react';

class CheckboxFilter extends Component {
  emitChange(option, checked) {
    const { value, onChange } = this.props;

    if (typeof onChange === 'function') {
      const newValue = Object.assign({}, value);
      newValue[option] = checked;

      onChange(newValue);
    }
  }

  render() {
    const { label, options, value } = this.props;

    return (
      <div className="row">
        <div className="col-xs-3"><h4>{label}</h4></div>
        <div className="col-xs-9">
          {options.map(option => (
            <label key={option.value} className="checkbox">
              <input
                type="checkbox"
                checked={value[option.value]}
                onChange={event => this.emitChange(option.value, event.target.checked)}
              />
              {option.label}
            </label>
          ))}
        </div>
      </div>
    )
  }
}

export default CheckboxFilter;
