import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import {
  Link,
  withRouter
} from 'react-router-dom';
import { AjaxButton } from '../Helpers';

class Update extends Component {
  constructor(props) {
    super(props);

    const { device } = props;

    const dynamicFields = device.fields.map(field => new Object({
      _id: field.id,
      id: field.id,
      virtual: false,
      label: field.label,
      saved_label: field.label,
      value: device.dynamic[field.id]
    }));

    this.state = {
      name: device.name,
      description: device.description,
      dynamicFields,
      saving: false
    };
  }

  componentDidUpdate(prevProps, prevState) {
    const prevDF = prevState.dynamicFields;
    const currentDF = this.state.dynamicFields;

    if (prevDF.length < currentDF.length) {
      const field = currentDF[currentDF.length - 1];
      const newLabel = this.refs[`dynamiclabel-${field._id}`];

      if (newLabel !== undefined && typeof newLabel.focus === 'function') {
        newLabel.focus();
      }
    }
  }

  onCancel() {
    const { history } = this.props;

    history.goBack();
  }

  onSubmit(event) {
    event.preventDefault();

    const data = {
      name: this.state.name,
      description: this.state.description
    }

    // Append dynamic fields
    this.state.dynamicFields.forEach(field => {
      if (field.virtual) {
        return;
      }

      data[`dynamic_${field.id}`] = field.value;
    });

    this.setState({
      saving: true
    });

    const { id } = this.props.device;
    api.put(`/v1/:organization/device/${id}`, data)
      .then(body => {
        this.setState({
          saving: false
        });

        if (typeof this.props.onSave === 'function') {
          this.props.onSave(body.data);
        }

        this.props.history.goBack();
      })
      .catch(errors => {
        // @TODO display errors to user

        this.setState({
          saving: false
        });
      });
  }

  appendDynamicField(event) {
    event.preventDefault();

    let fields = this.state.dynamicFields.map(f => f);
    fields.push({
      _id: +(new Date()),
      virtual: true,
      label: '',
      saved_label: '',
      value: ''
    });

    this.setState({
      dynamicFields: fields
    });
  }

  saveDynamicField(field) {
    if (field.label === field.saved_label) {
      return;
    }

    let req;
    if (field.virtual) {
      req = api.post('/v1/:organization/device_dynamic_attribute', {
        label: field.label
      })
    } else {
      req = api.put(`/v1/:organization/device_dynamic_attribute/${field.id}`, {
        label: field.label
      })
    }

    req.then(body => {
        const { data } = body;

        const fields = this.state.dynamicFields.map(f => {
          if (f._id == field._id) {
            f.virtual = false;
            f.id = data.id;
            f.label = data.label;
            f.saved_label = f.label;
          }

          return f;
        })

        this.setState({
          dynamicFields: fields
        });
      })
  }

  setDynamicField(field, label, value) {
    const fields = this.state.dynamicFields.map(f => {
      if (f._id === field._id) {
        f[label] = value;
      }

      return f;
    });

    this.setState({
      dynamicFields: fields
    });
  }

  renderDynamicFields() {
    const { dynamicFields } = this.state;
    const { t } = this.props;

    if (dynamicFields.length === 0) {
      return null;
    }

    return (
      <div>
        <hr />

        {dynamicFields.map(field => (
          <div key={field._id} className="form-group">
            <input
              ref={`dynamiclabel-${field._id}`}
              className="input-label"
              placeholder={t('forms.dynamiclabel_placeholder')}
              value={field.label}
              onChange={event => this.setDynamicField(field, 'label', event.target.value)}
              onBlur={event => this.saveDynamicField(field)}
            />
            <input
              ref={`dynamicvalue-${field._id}`}
              type="text"
              className="form-control with-border"
              placeholder={t('forms.dynamicvalue_placeholder')}
              value={field.value}
              onChange={event => this.setDynamicField(field, 'value', event.target.value)}
            />
          </div>
        ))}
      </div>
    )
  }

  render() {
    const { t } = this.props;

    return (
      <div className="container-fluid" style={{ background: 'white' }}>
        <br />
        <form onSubmit={this.onSubmit.bind(this)}>
          <div className="form-group">
            <label htmlFor="inputEmail" className="control-label">
              {t('forms.device_name')}
            </label>
            <input
              id="inputEmail"
              type="text"
              className="form-control with-border"
              value={this.state.name}
              onChange={event => this.setState({
                name: event.target.value
              })}
            />
          </div>

          <div className="form-group">
            <label htmlFor="inputEmail" className="control-label">
              {t('forms.device_description')}
            </label>
            <input
              id="inputEmail"
              type="text"
              className="form-control with-border"
              value={this.state.description}
              onChange={event => this.setState({ description: event.target.value })}
            />
          </div>

          {this.renderDynamicFields()}

          <div className="form-group">
            <button
              onClick={this.appendDynamicField.bind(this)}
              className="btn btn-aqua btn-block"
            >
              <i className="material-icons">add</i>
              &nbsp;
              {t('forms.add_dynamic_field')}
            </button>
          </div>

          <div className="form-group">
            <div className="pull-right">
              <a href="javascript:void(0);" onClick={this.onCancel.bind(this)}>{ t('commons.cancel') }</a>
              &nbsp;
              <AjaxButton
                loading={this.state.saving}
                className="btn btn-mint"
                content={t('devices.save')}
                loadingContent={t('helpers.saving')}
                loadingClassName="btn-loading"
                disabled={this.state.saving || this.state.name === ''}
              />
            </div>
            <div className="clearfix"></div>
          </div>
        </form>
      </div>
    )
  }
}

export default withTranslation()(withRouter(Update));
