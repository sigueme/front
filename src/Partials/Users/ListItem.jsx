import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import moment from 'moment';

class ListItem extends Component {

  renderStatus() {
    const { item, t } = this.props;
    const status = item.is_active ? 'active' : 'disabled';

    const statusLabels = {
      active: 'label label-success',
      disabled: 'label label-danger',
    };

    return (
      <div className="pull-right">
        <span className={statusLabels[status]}>{t('statuses.user.' + status)}</span>
      </div>
    );
  }

  renderName() {
    const { item } = this.props;

    return (
      <h5>{item.name} {item.last_name}</h5>
    );
  }

  renderEmail() {
    const { item } = this.props;

    return (
      <div>
        <i className="material-icons">mail</i>
        {item.email}
      </div>
    );
  }

  renderDescription() {
    const { item } = this.props;

    return (<p>{item.description}</p>);
  }

  renderFooter() {
    const { item, t } = this.props;

    let actions = [];

    const perm = `${api.organization}:user:${item.id}`;
    const allowedActions = api.permissions.allowedActions(perm);

    if (allowedActions.indexOf('admin') !== -1) {
      actions.push(
        <Link key="user-read" to={`/user/${item.id}`} className="action">
          <i className="material-icons">remove_red_eye</i>
          &nbsp;
          {t('commons.permissions')}
        </Link>
      );
      actions.push(
        <Link key="user-update" to={`/user/${item.id}/update`} className="action">
          <i className="material-icons">mode_edit</i>
          &nbsp;
          {t('commons.update')}
        </Link>
      );
      actions.push(
        <Link key="user-delete" to={`/user/${item.id}/delete`} className="action">
          <i className="material-icons">delete</i>
          &nbsp;
          {t('commons.delete')}
        </Link>
      );
    }

    return (
      <ul className="footer-actions">{actions}</ul>
    );
  }

  renderCreatedAt() {
    const { item, t } = this.props;
    const created_at = moment(item.created_at).format('lll');

    return (
      <span>
        <i className="material-icons">query_builder</i>
        {t('commons.created_at')} {created_at}
      </span>
    );
  }

  render() {
    const { item } = this.props;

    return (
      <div className="panel panel-default">
        <div className="panel-body">
          <div className="media">
            <div className="media-body">
              {this.renderStatus()}
              {this.renderName()}
              {this.renderEmail()}
              {this.renderDescription()}
              {this.renderCreatedAt()}
            </div>
          </div>
        </div>
        <div className="panel-footer">
          {this.renderFooter()}
        </div>
      </div>
    );
  }
}

export default withTranslation()(ListItem);
