const SVGNS = 'http://www.w3.org/2000/svg';

const describeArc = function(ir, or, a1, a2) {
  a1 -= Math.PI/2;
  a2 -= Math.PI/2;

  const p0 = {
    x: or*Math.cos(a1),
    y: or*Math.sin(a1),
  };

  const p1 = {
    x: or*Math.cos(a2),
    y: or*Math.sin(a2),
  };

  const p2 = {
    x: ir*Math.cos(a2),
    y: ir*Math.sin(a2),
  };

  const p3 = {
    x: ir*Math.cos(a1),
    y: ir*Math.sin(a1),
  };

  let largeArc = (a2 - a1) > Math.PI;

  const d = [
    `M${p0.x},${p0.y}`,
    `A${or},${or}, 0 ${+largeArc},1 ${p1.x},${p1.y}`,
    `L${p2.x},${p2.y}`,
    `A${ir},${ir}, 0 ${+largeArc},0 ${p3.x},${p3.y}`,
    'Z'
  ].join(' ');

  return d;
};

const buildSlice = function (a0, a1, color, count) {
  const slice = document.createElementNS(SVGNS, 'g');
  const outer = document.createElementNS(SVGNS, 'path');
  const inner = document.createElementNS(SVGNS, 'path');

  inner.setAttributeNS(null, 'd', describeArc(8, 20, a0, a1));
  inner.setAttributeNS(null, 'fill', color);
  outer.setAttributeNS(null, 'opacity', '0.7');

  outer.setAttributeNS(null, 'd', describeArc(20, 25, a0, a1));
  outer.setAttributeNS(null, 'fill', color);
  outer.setAttributeNS(null, 'opacity', '0.3');

  slice.appendChild(outer);
  slice.appendChild(inner);

  if (count) {
    const text = document.createElementNS(SVGNS, 'text');
    const mida = -(Math.PI/2) + (a0 + a1) / 2;
    const tx = 14*Math.cos(mida);
    const ty = 14*Math.sin(mida);
    text.setAttributeNS(null, 'fill', '#fff');
    text.textContent = count;
    text.setAttributeNS(null, 'style', 'text-anchor: middle;font-weight: bold;font-size: 0.9em;');
    text.setAttributeNS(null, 'dy', '0.35em');
    text.setAttributeNS(null, 'transform', `translate(${tx},${ty})`);
    slice.appendChild(text);
  }

  return slice;
};

export default function builder(maps) {
  function ClusterMarker(options) {
    options.course = options.course || 0;

    this.refs = {
      div: null
    };
    this.latlng = options.position;
    this.setMap(options.map);
    this.options = options;
  }

  ClusterMarker.prototype = new maps.OverlayView();

  ClusterMarker.prototype.draw = function () {
    const map = this.map;
    const panes = this.getPanes();
    const projection = this.getProjection();

    // If there is no map or the map is not ready, skip render
    if (!map || !panes || !projection) {
      return;
    }

    const div = document.createElement('div');
    div.className = 'custom-marker _cluster';
    div.style.visibility = this.options.visibility;

    const svg = document.createElementNS(SVGNS, 'svg');
    svg.setAttributeNS(null, 'width', '50');
    svg.setAttributeNS(null, 'height', '50');

    const gt = document.createElementNS(SVGNS, 'g');
    gt.setAttributeNS(null, 'transform', 'translate(25,25)');

    const { count, summary } = this.options;

    let l1 = 0;
    let l2 = l1 + (2*Math.PI)*(summary.moving / count);
    const movingSlice = buildSlice(l1, l2, '#48cfad', summary.moving);

    l1 = l2;
    l2 = l1 + (2*Math.PI)*(summary.stopped / count);
    const stoppedSlice = buildSlice(l1, l2, '#ed5565', summary.stopped);

    l1 = l2;
    l2 = l1 + (2*Math.PI)*(summary.offline / count);
    const offlineSlice = buildSlice(l1, l2, '#656d78', summary.offline);

    l1 = l2;
    l2 = l1 + (2*Math.PI)*(summary.alarm / count);
    const alarmSlice = buildSlice(l1, l2, '#ffce54', summary.alarm);

    const border = document.createElementNS(SVGNS, 'circle');
    border.setAttributeNS(null, 'r', 20);
    border.setAttributeNS(null, 'fill', 'none');
    border.setAttributeNS(null, 'stroke', 'white');
    border.setAttributeNS(null, 'stroke-width', '1.5');

    gt.appendChild(movingSlice);
    gt.appendChild(stoppedSlice);
    gt.appendChild(offlineSlice);
    gt.appendChild(alarmSlice);
    gt.appendChild(border);

    svg.appendChild(gt);
    div.appendChild(svg);

    // Delete previous div
    if (this.refs.div && this.refs.div.parentNode) {
      this.refs.div.parentNode.removeChild(this.refs.div);
    }

    maps.event.addDomListener(div, 'click', () => {
      maps.event.trigger(this, 'click');
    });

    panes.overlayImage.appendChild(div);

    // Store new div
    this.refs.div = div;

    const point = projection.fromLatLngToDivPixel(this.latlng);
    if (point) {
      div.style.left = (point.x - 25) + 'px';
      div.style.top = (point.y - 25) + 'px';
    }
  };

  ClusterMarker.prototype.onRemove = function() {
    if (this.refs.div) {
      if (this.refs.div.parentNode) {
        this.refs.div.parentNode.removeChild(this.refs.div);
      }

      this.refs.div = null;
    }
  };

  ClusterMarker.prototype.hide = function() {
    if (this.refs.div) {
      this.refs.div.style.visibility = 'hidden';
    }
  };

  ClusterMarker.prototype.show = function() {
    if (this.refs.div) {
      this.refs.div.style.visibility = 'visible';
    }
  };

  ClusterMarker.prototype.getPosition = function() {
    return this.latlng;
  };

  ClusterMarker.prototype.setPosition = function(latlng) {
    this.latlng = latlng;
  };

  ClusterMarker.prototype.setOptions = function(options) {
    for (let attr in options) {
      this.options[attr] = options[attr];
    }
  };

  return ClusterMarker
}
