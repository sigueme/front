import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

class Header extends Component {
  renderPublicNavbar() {
    const { organization } = api;
    const { t } = this.props;

    if (organization) {
      return (
        <ul className="nav navbar-nav navbar-right">
          <li><Link to="/signin">{ t('header.signin') }</Link></li>
        </ul>
      );
    } else {
      return (
        <ul className="nav navbar-nav navbar-right">
          <li><Link to="/signin">{ t('header.signin') }</Link></li>
          <li><Link to="/signup">{ t('header.signup') }</Link></li>
        </ul>
      );
    }
  }

  render() {
    const navbarLinks = this.renderPublicNavbar();
    const { hostname } = api;

    return (
      <div className='navbar navbar-default'>
        <div className="container">
          <div className="navbar-header">
            <a className="navbar-brand" href={ `//${ hostname }` }>Fleety</a>
            <button className="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
          </div>
          <div className="navbar-collapse collapse" id="navbar-main">
            {navbarLinks}
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(Header);
