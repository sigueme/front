import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Media from 'react-media';

import Loading from '../../Partials/Loading';

import ReadMobile from './Read.mobile';
import ReadDesktop from './Read.desktop';

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      device: {},
    };
  }

  componentDidMount() {
    // fetch model
    this.fetchModel((device) => {
      this.subscribe(device);
    });

    // re-fetch model every 5 minutes
    this.subscribeInterval = setInterval(() => {
      this.fetchModel();
    }, 5*60*1000);
  }

  subscribe(device) {
    // Create socket
    const organization = api.organization;
    const fleetChannel = `${organization}:fleet:${device.fleet.id}`;

    this.socket = api.socket([fleetChannel], (eventData) => {
      switch (eventData.event) {
        case 'device-update':
        case 'update':
          if (!this.state.device) {
            break;
          }

          // ignore positions from other devices
          if (eventData.data.id != this.state.device.id) {
            break;
          }

          let device = Object.assign(this.state.device, eventData.data);
          device.last_update = new Date(device.last_update);

          this.setState({ device });
          break;
      }
    });
  }

  componentWillUnmount() {
    clearInterval(this.subscribeInterval);

    if (this.socket && typeof this.socket.disconnect == 'function') {
      this.socket.disconnect();
    }
  }

  fetchModel(callback) {
    const { id } = this.props.match.params;

    api.get(`v1/:organization/device/${id}?embed=fleet.id`)
      .then(body => {
        const device = body.data;

        this.setState({
          loading: false,
          device: device
        });

        if (typeof callback == 'function') {
          callback(device);
        }
      })
      .catch(error => {
        console.error(error);

        this.setState({
          loading: false,
          error: true
        });
      });
  }

  render() {
    if (this.state.loading) {
      return <Loading />
    }

    const { props } = this;
    const { t } = this.props;

    if (this.state.error) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('errors.http404.title') }</h2>
          { t('errors.http404.description') }
          <br />
          <Link to="/" className="btn btn-lg btn-mint">{ t('errors.http404.button') }</Link>
        </div>
      );
    }

    const { device } = this.state;

    return (
      <Media query="(max-width: 991px)">
        {matches => matches ? (
          <ReadMobile
            reloadDevice={this.fetchModel.bind(this)}
            device={device}
            {...props}
          />
        ) : (
          <ReadDesktop
            reloadDevice={this.fetchModel.bind(this)}
            device={device}
            {...props}
          />
        )}
      </Media>
    );
  }
};

export default withTranslation()(Container);
