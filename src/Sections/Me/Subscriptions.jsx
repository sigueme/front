import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import Select from 'react-select';

import Loading from '../../Partials/Loading';
import PageTitle from '../../components/PageTitle';
import { Sidebar } from '../../Partials/Layout';

class List extends Component {
  constructor(props) {
    super(props);
    const { t } = props;

    this.events = [
      {value: 'alarm', label: t('events.names.alarm')},
      {value: 'geofence-enter', label: t('events.names.geofence-enter')},
      {value: 'geofence-leave', label: t('events.names.geofence-leave')},
      {value: 'trip-finished', label: t('events.names.trip-finished')},
      {value: 'trip-started', label: t('events.names.trip-started')},
      {value: '*', label: t('events.all')},
    ];
    this.handlers = [
      {value: 'Email', label: 'Email'},
      {value: 'Telegram', label: 'Telegram'},
    ];

    this.state = {
      loading: true,
      items: [],
      channels: [],
      selectedEvent: '',
      selectedChannel: '',
      selectedChannelName: '',
      selectedHandler: '',
    };
  }

  componentDidMount() {
    this.fetchModel();
    this.loadFleets();
  }

  loadFleets() {
    const { t } = this.props;

    api.get('/v1/:organization/fleet')
      .then(body => {
        const fleetOptions = body.data.map(fleet => {
          return {
            value: api.org.subdomain + ':fleet:' + fleet.id,
            label: fleet.name,
          }
        });

        let extraChannels = [{
          value: api.org.subdomain + ':fleet',
          label: t('subscriptions.all_fleets'),
        }, {
          value: api.org.subdomain + ':user:' + api.user.id,
          label: t('subscriptions.my_events'),
        }];

        if (api.user.is_god) {
          extraChannels.push({
            value: 'meta',
            label: t('events.meta'),
          });
        }

        this.setState({
          channels: fleetOptions.concat(extraChannels),
        });
      })
      .catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        errors.map(error => console.error(error));
      });
  }

  fetchModel() {
    const { id } = api.user;

    this.setState({
      loading: true
    });

    api.get(`/v1/:organization/user/${id}/subscriptions`)
      .then(body => {
        this.setState({
          loading: false,
          items: body.data
        });
      })
      .catch(errors => {
        this.setState({
          loading: false,
          errors: errors
        });
      });
  }

  handleSubmit() {
    api.post('/v1/:organization/subscription', {
      user_id: api.user.id,
      channel: this.state.selectedChannel,
      channel_name: this.state.selectedChannelName,
      event: this.state.selectedEvent,
      handler: this.state.selectedHandler,
    }).then(msg => {
      let items = this.state.items;

      items.push(msg.data);

      this.setState({
        items: items,
        selectedEvent: '',
        selectedChannel: '',
        selectedChannelName: '',
        selectedHandler: '',
      });
    });
  }

  deleteSubscription(sub_id) {
    api.delete(`/v1/:organization/subscription/${sub_id}`).then(() => {
      this.setState({
        items: this.state.items.filter(sub => sub.id != sub_id),
      });
    });
  }

  renderSubscriptions(subscriptions) {
    const { t } = this.props;

    if (subscriptions.length === 0) {
      return [
        <tr key="empty">
          <td colSpan="4">
            <div style={{ paddingBottom: '40px', textAlign: 'center' }}>
              <h3>{ t('subscriptions.empty') }</h3>
              { t('subscriptions.add_subscription_tutorial') }
            </div>
          </td>
        </tr>
      ];
    }

    return subscriptions.map(sub => (
      <tr key={sub.id}>
        <td>{sub.channel_name}</td>
        <td>{ t('events.names.{sub.event}') }</td>
        <td>{sub.handler}</td>
        <td className="minimal">
          <div
            className="delete-icon"
            onClick={this.deleteSubscription.bind(this, sub.id)}
          >
            <i className="material-icons">delete</i>
          </div>
        </td>
      </tr>
    ));
  }

  renderContent() {
    const { t } = this.props;

    return (
      <div>
        <div className="panel-body">
          <h3><center>{ t('me.subscriptions') }</center></h3>
        </div>

        <table className="items-list">
          <thead>
            <tr className="form-row">
              <td style={{ width: '150px' }}>
                <Select
                  placeholder={t('commons.channel')}
                  options={this.state.channels}
                  value={this.state.selectedChannel}
                  onChange={option => this.setState({ selectedChannel: option.value, selectedChannelName: option.label })}
                  autosize={false}
                  clearable={false}
                />
              </td>

              <td>
                <Select
                  placeholder={t('commons.event')}
                  options={this.events}
                  value={this.state.selectedEvent}
                  onChange={option => this.setState({ selectedEvent: option.value })}
                  autosize={false}
                  clearable={false}
                />
              </td>

              <td>
                <Select
                  placeholder={t('commons.handler')}
                  options={this.handlers}
                  value={this.state.selectedHandler}
                  onChange={option => this.setState({ selectedHandler: option.value })}
                  autosize={false}
                  clearable={false}
                />
              </td>

              <td className="shrink">
                <button
                  disabled={this.state.selectedChannel === null || this.state.selectedEvent === null || this.selectedHandler === null}
                  onClick={this.handleSubmit.bind(this)}
                  className="btn btn-default btn-block"
                >
                  { t('commons.save') }
                </button>
              </td>
            </tr>
          </thead>
          <tbody>
            {this.renderSubscriptions(this.state.items)}
          </tbody>
        </table>
      </div>
    );
  }

  render() {
    if (this.state.loading) {
      return <Loading />
    }

    const { t } = this.props;

    return (
      <div>
        <PageTitle title={t("me.subscriptions")} />
        <Sidebar />

        <div className="has-sidebar">
          <div className="spacer40"></div>
          <div className="container container-small">
            <div className="panel panel-default">
              {this.renderContent()}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withTranslation()(List);
