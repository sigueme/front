import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import CheckboxFilter from './CheckboxFilter';
import TagsFilter from './TagsFilter';
import merge from '../utils/merge';

class ListFilter extends Component {
  constructor(props) {
    super(props);

    let state = this._getAppliedValue(props);
    state._open = false;

    this.state = state;
  }

  _getNeutralValue(props) {
    const { fields } = props;

    let state = {};
    if (fields && fields.length) {
      fields.forEach(filter => {
        switch (filter.type) {
          case 'checkbox':
            state[filter.name] = filter.options.reduce((obj, opt) => {
              obj[opt.value] = true;
              return obj;
            }, new Object());
            break;
          case 'tags':
            state[filter.name] = new Object();
            break;
        }
      });
    }

    return state;
  }

  _getAppliedValue(props) {
    let state = this._getNeutralValue(props);

    if (props.filters !== undefined && props.filters.constructor === Object) {
      state = merge(state, props.filters);
    }

    return state;
  }

  setText(text) {
    const { onChangeSearch } = this.props;

    if (typeof onChangeSearch === 'function') {
      onChangeSearch(text);
    }
  }

  toggleOpen() {
    const open = !this.state._open;
    let state;
    if (open) {
      state = this._getAppliedValue(this.props);
      state._open = true;
    } else {
      state = {
        _open: false
      };
    }

    this.setState(state);
  }

  setFilters(filters) {
    const { onChangeFilters } = this.props;

    filters._build = new Date();

    if (typeof onChangeFilters === 'function') {
      onChangeFilters(filters);
    }
  }

  applyFilters() {
    let state = Object.assign({}, this.state);

    for (let attr in state) {
      if (attr.charAt(0) === '_') {
        delete state[attr];
      }
    }

    this.setFilters(state);
    this.setState({
      _open: false
    });
  }

  clearFilters() {
    let neutral = this._getNeutralValue(this.props);

    this.setFilters(neutral);
    this.setState({
      _open: false
    });
  }

  renderSearchBox() {
    const { searchPlaceholder, fields, t } = this.props;

    let filterBtn = null;
    if (fields && fields.length) {
      filterBtn = (
        <span className="input-group-btn">
          <button
            className="btn btn-primary"
            onClick={this.toggleOpen.bind(this)}
          >
            {t('commons.filters')}
            &nbsp;
            <i className="material-icons">tune</i>
          </button>
        </span>
      );
    }

    let placeholder = t('commons.search');
    if (searchPlaceholder) {
      placeholder = searchPlaceholder;
    }

    return (
      <div className="search-box input-group input-group-lg">
        <span className="input-group-addon">
          <i className="material-icons">search</i>
        </span>
        <input
          type="text"
          className="form-control search-input"
          value={this.props.search}
          onChange={event => this.setText(event.target.value)}
          placeholder={placeholder}
        />
        {filterBtn}
      </div>
    );
  }

  renderFilterBox() {
    const { t } = this.props;

    let className = 'popover bottom';
    if (this.state._open) {
      className += ' open';
    }

    const { fields } = this.props;

    const RenderedFields = fields.map(filter => {
      switch (filter.type) {
        case 'checkbox':
          return (
            <CheckboxFilter
              key={filter.name}
              label={filter.label}
              options={filter.options}
              value={this.state[filter.name]}
              onChange={value => {
                this.setState({ [filter.name]: value })
              }}
            />
          );
          break;
        case 'tags':
          return (
            <TagsFilter
              key={filter.name}
              label={filter.label}
              options={filter.options}
              value={this.state[filter.name]}
              onChange={value => {
                this.setState({ [filter.name]: value })
              }}
            />
          );
          break;
      }
    });

    return (
      <div style={{
        position: 'relative'
      }}>
        <div className={className}>
          <div className="arrow"></div>
          <div className="popover-content">
            {RenderedFields}

            <hr />

            <div className="pull-right">
              <span
                className="btn btn-darkgray btn-clear"
                onClick={this.toggleOpen.bind(this)}
              >{t('commons.cancel')}</span>
              &nbsp;
              <span
                className="btn btn-mint btn-ghost"
                onClick={this.applyFilters.bind(this)}
              >{t('commons.apply')}</span>
            </div>

            <span
              className="btn btn-darkgray btn-clear"
              onClick={this.clearFilters.bind(this)}
            >{t('commons.clear_filters')}</span>

            <div className="clearfix" />
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="list-filter">
        {this.renderSearchBox()}
        {this.renderFilterBox()}
      </div>
    );
  }
}

export default withTranslation()(ListFilter);
