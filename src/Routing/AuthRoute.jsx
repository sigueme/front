import React from 'react';
import {
  Route,
  Redirect
} from 'react-router-dom'

function AuthRoute(props) {
  let routeProps = new Object();
  for (let i in props) {
    if (i === 'public' || i === 'private') {
      if (typeof props[i] !== 'function') {
        throw 'Prop ' + i + ' must be a function';
      }

      continue;
    }

    if (props.hasOwnProperty(i)) {
      routeProps = props[i];
    }
  }

  const {
    private: Private,
    public: Public
  } = props;

  function renderFunction(props) {
    if (api.user !== null) {
      return (
        Private ? (
          <Private {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/',
              state: { from: props.location }
            }}
          />
        )
      );
    } else {
      return (
        Public ? (
          <Public {...props} />
        ) : (
          <Redirect to="/signin" />
        )
      );
    }
  }

  return (
    <Route {...routeProps} render={renderFunction} />
  );
};

export default AuthRoute;
