import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Select from 'react-select';

import { Sidebar } from '../../Partials/Layout';

class Update extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: null,
    };
  }

  componentDidMount() {
    this.loadData(this.props);
  }

  componentWillReceiveProps(props) {
    this.loadData(props);
  }

  loadData(props) {
    const { id } = this.props.match.params;

    api.get(`/v1/:organization/fleet/${id}?embed=devices`)
      .then(body => {
        this.setState({
          fleet: body.data
        })
      });
  }

  addDevice(event) {
    event.preventDefault();

    const { fleet } = this;

    const deviceId = this.state.selected.id;

    const params = {
      fleet: this.props.match.params.id
    };

    api.put(`/v1/:organization/device/${deviceId}`, params)
      .then(body => {
        this.setState({
          selected: null
        });

        this.loadData();
      })
  }

  suggest(input, callback) {
    api.search('device', input, callback, (device) => {
      return [device.code, device.name, device.description].filter(x => x).join(' - ');
    });
  }

  renderDevices(devices) {
    const { t } = this.props;

    if (devices.length === 0) {
      return (
        <tr>
          <td colSpan="2">
            <div style={{ paddingBottom: '40px', textAlign: 'center' }}>
              <h3>{ t('fleets.empty_fleet') }</h3>
              { t('fleets.add_device_tutorial') }
            </div>
          </td>
        </tr>
      );
    }

    return devices.map(device => (
      <tr key={device.id}>
        <td colSpan="2">
          <Link to={`/device/${device.id}`}>
            <span className="name">{device.code} {device.name} {device.description}</span>
          </Link>
        </td>
      </tr>
    ));
  }

  renderContent() {
    const { t } = this.props;
    const { fleet } = this.state;

    if (!fleet) {
      return null;
    }

    const { devices } = fleet;

    return (
      <div>
        <div className="panel-body">
          <div className="content-title">
            <span className="label label-primary">{fleet.abbr}</span>
            &nbsp;
            {fleet.name}
          </div>
        </div>

        <table className="items-list">
          <thead>
            <tr className="form-row">
              <td>
                <Select.Async
                  placeholder={t('fleets.device_suggestion_placeholder')}
                  loadOptions={this.suggest.bind(this)}
                  value={this.state.selected}
                  onChange={option => this.setState({ selected: option })}
                  labelKey="name"
                  valueKey="id"
                />
              </td>
              <td className="shrink">
                <button
                  onClick={this.addDevice.bind(this)}
                  className="btn btn-default btn-block"
                >
                  { t('fleets.add_device') }
                </button>
              </td>
            </tr>
          </thead>
          <tbody>
            {this.renderDevices(devices)}
          </tbody>
        </table>
      </div>
    );
  }

  render() {
    return (
      <div>
        <Sidebar />

        <div className="has-sidebar">
          <div className="spacer40"></div>
          <div className="container container-small">
            <div className="panel panel-default">
              {this.renderContent()}
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default withTranslation()(Update);
