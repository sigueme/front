import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import moment from 'moment';

import settings from '../../settings';

class Trips extends Component {
  constructor(props) {
    super(props);

    this.state = {
      trips: []
    };
  }

  componentDidMount() {
    const { device } = this.props;

    api.get(`/v1/:organization/device/${device.id}/trips`)
      .then(body => {
        this.setState({
          trips: body.data
        });
      })
      .catch(errs => {
        console.error(errs);
      });
  }

  renderTrips(trips) {
    const { t } = this.props;

    if (trips.length === 0) {
      return [
        <tr key="empty">
          <td colSpan="4">
            <div style={{ paddingBottom: '40px', textAlign: 'center' }}>
              <h4>{ t('trips.empty_list') }</h4>
              { t('trips.empty_list_sub') }
            </div>
          </td>
        </tr>
      ];
    }

    return trips
      .map(trip => {
        if (trip.polyline) {
          let thumbnail = 'https://maps.googleapis.com/maps/api/staticmap?size=100x100&sensor=true&path=weight:3|color:0x206D5AAA|enc:';

          thumbnail += encodeURIComponent(trip.polyline);
          thumbnail += '&key=' + encodeURIComponent(settings.google_maps.staticmap_api_key);

          trip.backgroundImage = `url(${thumbnail})`;
        } else {
          trip.backgroundImage = 'none';
        }

        return trip;
      })
      .map(trip => (
        <tr key={trip.id}>
          <td>
            <div className="media">
              <div className="media-left">
                <Link
                  to={`/trip/${trip.id}`}
                  style={{
                    display: 'block',
                    width: '100px',
                    height: '100px',
                    background: '#333',
                    color: '#999',
                    fontWeight: 'bold',
                    textAlign: 'center',
                    fontSize: '0.9em',
                    backgroundImage: trip.backgroundImage,
                  }}
                />
              </div>
              <div className="media-body">
                <h6>{t('trips.trip_prefix') + ' ' + trip.virtuals.title}</h6>
                <p>{t('statuses.trip.' + trip.virtuals.status.toUpperCase())}</p>
              </div>
            </div>
          </td>
        </tr>
      ));
  }

  render() {
    const { t } = this.props;
    const trips = this.state.trips.map(trip => {
      trip.virtuals = {
        status: (trip.arrival ? 'finished' : 'ongoing')
      };

      if (trip.virtuals.status === 'finished') {
        const departure = new Date(trip.departure);
        const arrival = new Date(trip.arrival);
        const duration = arrival - departure;

        trip.virtuals.title = moment(departure).format('YYYY-MM-DD HH:ss') + ' • ' + moment.duration(duration).humanize();
      } else {
        const departure = new Date(trip.departure);
        trip.virtuals.title = moment(departure).format('YYYY-MM-DD HH:ss');
      }

      return trip;
    });

    return (
      <div className="widget-box">
        <div className="widget-padding">
          <small>{t('devices.trips')}</small>
        </div>

        <div className="location-events">
          <table className="table table-striped">
            <tbody>
              {this.renderTrips(trips)}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default withTranslation()(Trips);
