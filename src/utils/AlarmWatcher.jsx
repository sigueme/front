import React from 'react';
import { toast } from 'react-toastify';
import { Trans } from 'react-i18next';
import { Link } from 'react-router-dom';

class AlarmWatcher {
  constructor() {
    // Establish socket
    this.socket = api.socket(
      [`${api.organization}:fleet:*`],
      this.socketHandler.bind(this)
    );
  }

  socketHandler(eventData) {
    const model = eventData.data;

    switch (eventData.event) {
      case 'alarm':
        const { device } = model;

        toast.error((
          <strong>
            <Link to={`/device/${device.id}`}>{device.name || device.code}</Link>
            &nbsp;-&nbsp;
            <span>{`events.alarm.${model.type}`}</span>
          </strong>
        ));

        break;
    }
  }
}

export default AlarmWatcher;
