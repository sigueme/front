import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import ReadDesktop from './Read.desktop.jsx';
import haversine from '../../utils/haversine';
import Loading from '../../Partials/Loading';

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
    }
  }

  componentDidMount() {
    this.subscribe();
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  fetchModel(callback) {
    const { id } = this.props.match.params;

    api.get('v1/:organization/trip/' + id)
      .then(body => {
        const trip = body.data;

        trip.location_history = trip.location_history.map(position => {
          position.server_time = new Date(position.server_time);
          position.device_time = new Date(position.device_time);

          return position;
        });

        this.setTrip(trip);
        this.setState({
          loading: false
        });

        callback(trip);
      })
      .catch(error => {
        this.setState({
          loading: false,
          error: true
        });
      });
  }

  subscribe() {
    // fetch model
    this.fetchModel((trip) => {
      if (trip.arrival) {
        return this.unsubscribe(); // do not subscribe if the trip is finished
      }

      let fleetChannel = `${api.org.subdomain}:fleet:${trip.fleet_id}`;

      // the socket
      this.socket = api.socket([fleetChannel], (eventData) => {
        let { trip } = this.state;

        if (eventData.event == 'device-update') {
          // got a new position from the device
          if (eventData.data.id != this.state.device.id) {
            return; // ignore positions from other devices
          }
          let model = eventData.data;

          model.last_update = new Date(model.last_update);

          let position = Object.assign(model.last_pos, {
            server_time: model.last_update,
            device_time: model.last_update,
            speed: 0,
          });

          trip.location_history.push(position);
        } else if (eventData.event == 'update') {
          // this could be a trip update
          if (typeof eventData.data.trip == 'undefined') {
            return; // ignore other types of updates
          }
          let trip = Object.assign(trip, eventData.data.trip);

          if (trip.arrival) {
            this.socket.disconnect();
          }
        }

        this.setTrip(trip);
      });

      // re-fetch model every 5 minutes
      this.subscribeInterval = setInterval(() => {
        this.fetchModel();
      }, 300000);
    });
  }

  unsubscribe() {
    clearInterval(this.subscribeInterval);

    if (this.socket && typeof this.socket.disconnect == 'function') {
      this.socket.disconnect();
    }
  }

  componentWillUnmount() {
    clearInterval();

    const { socket } = this;

    if (socket && typeof socket.disconnect === 'function') {
      socket.disconnect();
    }
  }

  setTrip(trip) {
    const clusters = this.clusterize(trip.location_history);
    const events = this.eventize(clusters);

    trip.virtuals = {
      clusters,
      events
    };

    this.setState({
      trip
    });
  }

  clusterize(points) {
    let clusters = [{
      head: 0,
      tail: 0
    }];

    if (!points || !points.length) {
      return clusters;
    }

    let last_cluster = clusters[0];
    for (let i=1, l=points.length; i<l; i++) {
      const ref = points[last_cluster.head];
      const point = points[i];

      const distance = haversine(
        ref.lon,
        ref.lat,
        point.lon,
        point.lat
      );

      if (distance < 50) {
        last_cluster.tail = i;
      } else {
        if (last_cluster.head !== last_cluster.tail) {
          let head_time = points[last_cluster.head].device_time;
          let tail_time = points[last_cluster.tail].device_time;

          last_cluster.nodes = last_cluster.tail - last_cluster.head + 1;
          last_cluster.idletime = tail_time - head_time;
        } else {
          last_cluster.nodes = 1;
          last_cluster.idletime = 0;
        }

        last_cluster = {
          head: i,
          tail: i
        };

        clusters.push(last_cluster);
      }
    }

    if (last_cluster.head !== last_cluster.tail) {
      let head_time = points[last_cluster.head].device_time;
      let tail_time = points[last_cluster.tail].device_time;

      last_cluster.nodes = last_cluster.tail - last_cluster.head + 1;
      last_cluster.idletime = tail_time - head_time;
    } else {
      last_cluster.nodes = 1;
      last_cluster.idletime = 0;
    }

    return clusters.map(cluster => {
      let head = Object.assign({}, points[cluster.head]);

      head.idletime = cluster.idletime;
      head.nodes = cluster.nodes;

      return head;
    });
  }

  eventize(clusters) {
    let events = [{
      type: 'stop',
      ref: 0,
      start: clusters[0].device_time,
      duration: clusters[0].idletime
    }];

    for (let i=1, l=clusters.length; i<l; i++) {
      const cluster = clusters[i];

      if (cluster.idletime > 300000 || i === l-1) {
        const prevEvent = events[events.length - 1];
        const prevCluster = clusters[prevEvent.ref];
        const prevFinish = new Date(prevCluster.device_time.getTime() + prevCluster.idletime);

        events.push({
          type: 'driving',
          head: events[events.length - 1].ref,
          tail: i,
          start: prevFinish,
          duration: cluster.device_time - prevFinish
        });

        events.push({
          type: 'stop',
          ref: i,
          start: cluster.device_time,
          duration: cluster.idletime || 0,
        });
      }
    }

    return events;
  }

  render() {
    if (this.state.loading) {
      return <Loading />
    }

    const { props } = this;
    const { t } = this.props;

    if (this.state.error) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('errors.http404.title') }</h2>
          { t('errors.http404.description') }
          <br />
          <Link to="/" className="btn btn-lg btn-mint">{ t('errors.http404.button') }</Link>
        </div>
      );
    }

    const { trip } = this.state;

    return (
      <ReadDesktop
        trip={trip}
        {...props}
      />
    );
  }
};

export default withTranslation()(Container);
