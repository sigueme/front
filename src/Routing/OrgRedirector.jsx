import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import { Header } from '../Partials/Layout';

class OrgRedirector extends Component {
  constructor(props) {
    super(props);

    this.state = {
      subdomain: '',
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    const { hostname } = window.location;
    const { subdomain } = this.state;

    window.location = `http://${subdomain}.${hostname}`;
  }

  render() {
    const { t } = this.props;
    const { hostname } = window.location;

    return (
      <div>
        <Header />

        <div className="container container-micro">
          <div className="panel">
            <div className="panel-body">
              <h3>
                { t('org_redirector.title') }
              </h3>
              <br />
              <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="form-group">
                  <label htmlFor="inputSubdomain" className="control-label">
                    { t('org_redirector.subdomain') }
                  </label>
                  <div className="input-group">
                    <input
                      id="inputSubdomain"
                      type="text"
                      className="form-control with-border"
                      placeholder={ t('org_redirector.subdomain_placeholder') }
                      value={this.state.subdomain}
                      onChange={event => this.setState({
                        subdomain: event.target.value,
                        subdomainError: null
                      })}
                    />
                    <span className="input-group-addon with-border">.{ hostname }</span>
                  </div>
                </div>

                <br />
                <div className="pull-right">
                  <button className="btn btn-lg btn-primary">
                    { t('org_redirector.submit') }
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(OrgRedirector);
