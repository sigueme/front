const es = {
  translation: {
    commons: {
      save: 'Guardar',
      cancel: 'Cancelar',
      not_set: '--',
      update: 'Editar',
      delete: 'Eliminar',
      share: 'Compartir al público',
      created_at: 'Creado',
      permissions: 'Permisos',
      view_in_map: 'Ver en mapa',
      search: 'Buscar',
      filters: 'Filtros',
      apply: 'Aplicar',
      clear_filters: 'Limpiar filtros',
      channel: 'Canal',
      event: 'Evento',
      handler: 'Medio',
      settings: 'Configuraciones',

      no_results: 'Sin Resultados',
      no_results_feedback: 'La búsqueda actual no arrojó nungún resultado',

      clear_field: 'Limpiar filtro',
      clear_option: 'Quitar opción',
      type_to_search: 'Escribe para buscar',

      '1m': '1 minuto',
      '10m': '10 minutos',
      '30m': '30 minutos',
      '60m': '1 hora',
      '120m': '2 horas',

      requested_at: 'Solicitado',
      completed_at: 'Completado',

      download: 'Descargar',

      startdate: 'Fecha de inicio',
      enddate: 'Fecha de fin',
    },

    header: {
      signin: 'Iniciar sesión',
      signup: 'Crear una cuenta',
      signout: 'Cerrar sesión'
    },

    home: {
      title: 'Administra tu flota sin complicaciones',
      app_name: 'Fleety',
      description: 'Facil de usar y soporta multiples dispositivos GPS. Rastrea, analiza y mejora la operación de tu flota con Fleety',
      signup: 'Pruebalo por 14 días'
    },

    sign_in: {
      title: 'Iniciar sesión',
      forgot_password: '¿Olvidaste tu contraseña?',
      remember_me: 'Recordar contraseña',
      submit: 'Iniciar sesión',
      signing: 'Iniciando sesión ...'
    },

    sign_up: {
      title: 'Prueba Fleety ahora',
      terms_and_conditions: 'Al registrarte, aceptas los <1>términos y condiciones</1>',
      password_hint: 'Usa letras, números y signos',
      claim_subdomain: 'Organización',
      claim_subdomain_placeholder: 'tuorganizacion',

      submit: 'Iniciar ahora',
      registering: 'Registrando'
    },

    sign_out: {
      signing_out: 'Cerrando sesión...'
    },

    org_redirector: {
      title: 'Iniciar Sesión',
      subdomain: 'Organización',
      subdomain_placeholder: 'tuorganizacion',
      submit: 'Continuar'
    },

    forgot_password: {
      title: '¿Olvidaste tu contraseña?',
      submit: 'Envíame instrucciones'
    },

    map: {
      sign_out: 'Cerrar sesión',
      search_box: 'Buscar en Fleety'
    },

    me: {
      subscriptions: 'Mis suscripciones',
    },

    subscriptions: {
      empty: 'No estás suscrito a ningún evento',
      add_subscription_tutorial: 'Crea una suscripción para ser notificado por distintos medios cuando ocurra un evento de tu interés en alguna flota',
      all_fleets: 'Todas las flotas',
      my_events: 'Notificaciones personales',
    },

    geofences: {
      title: 'Geocercas',
      create: 'Crear geocerca',
      update: 'Actualizar geocerca',
      first: 'Crea tu primer geocerca',
      description: '¿Quieres saber cuando llegan y salen tus unidades de una zona delimitada?',
      tutorial: '<0>Instrucciones</0><1><0>Da clic en el mapa para empezar a dibujar la geocerca</0><0>Para terminar cierra el polígono</0></1>',
      delete_draft: 'Borrar geocerca',
      save_draft: 'Guardar geocerca',
    },

    routes: {
      title: 'Rutas',
      create: 'Crear ruta',
      update: 'Actualizar ruta',
      first: 'Crea tu primera ruta',
      description: '¿Hay caminos que frecuentas mucho entre dos puntos?',

      name: 'Nombre de la ruta',
      origin: 'Origen',
      destination: 'Destino',

      search: 'Buscar ruta',
    },

    users: {
      title: 'Usuarios',
      read: 'Ver detalle de usuario',
      create: 'Crear nuevo usuario',
      update: 'Actualizar usuario',
      delete: 'Borrar usuario',
      save: 'Guardar usuario',
      suggestion_placeholder: 'Buscar usuarios'
    },

    devices: {
      title: 'Dispositivos',
      update: 'Actualizar dispositivo',
      save: 'Guardar dispositivo',
      map_tab: 'Mapa',
      info_tab: 'Información',
      trips_tab: 'Viajes',
      history_tab: 'Historia',
      search: 'Buscar dispositivos',

      trips: 'Últimos viajes',

      detail: 'Ver detalle',
      go_to_detail: 'Ir a detalle',
      status: 'Estado',
      location: 'Ubicación',
      last_report: 'Última ubicación',
      last_speed: 'Velocidad',
      code: 'Código GPS',

      event_stop: 'Detenido',
      event_driving: 'En movimiento',

      zero_devices: 'No hay dispositivos en el sistema',
      add_device: 'Agrega tu primer dispositivo',
    },

    trips: {
      title: 'Viajes',
      create: 'Programar un viaje',
      update: 'Editar viaje programado',
      trip_prefix: 'Viaje',
      search: 'Buscar viajes',

      open: 'Ver',
      start: 'Iniciar viaje',
      finish: 'Terminar viaje',

      empty_list: 'Sin viajes',
      empty_list_sub: 'Este dispositivo todavía no reporta viajes',

      zero_trips: 'No hay viajes en el sistema',
      zero_trips_sub: 'Programa y haz tu primer viaje',

      tracking_title: 'Guía de rastreo',
      started: 'Inicio de viaje',
      finished: 'Fin de viaje',
    },

    reports: {
      title: 'Reportes',
      create: 'Nuevo reporte',
      type: 'Tipo de reporte',
      report_name: 'Nombre del reporte',

      first: 'No tienes ningún reporte',
      description: 'Crea tu primer reporte',

      types: {
        DeviceGeoJSON: 'Historia a GeoJSON'
      }
    },

    forms: {
      date: 'Fecha seleccionada',

      email: 'Correo electrónico',
      email_placeholder: 'alex@email.com',

      password: 'Contraseña',
      password_show: 'Mostrar contraseña',
      password_placeholder: 'Tu contraseña',

      fullname: 'Nombre completo',
      name: 'Nombre(s)',
      lastName: 'Apellidos',

      geofence: 'Nombre de la geocerca',
      geofence_placeholder: 'Geocerca 1',

      device_name: 'Nombre',
      device_description: 'Descripción',

      add_dynamic_field: 'Agregar campo',
      arrival: 'Fecha estimada de llegada',
      departure: 'Fecha de partida',
      dynamiclabel_placeholder: 'Nombre del campo',
      dynamicvalue_placeholder: 'Valor del campo',
      endpoints: '¿De dónde sale? ¿Hacia dónde va?',
      status: 'Estatus',
      dates: 'Fechas',
      hour: 'HH:MM',

      destination: 'Destino',
      destination_placeholder: '¿Hacia dónde va?',

      origin: 'Origen',
      origin_placeholder: '¿De dónde sale?',

      eta: 'ETA en horas',
      eta_placeholder: 'Estimado en horas',

      user: 'Operador',
      user_placeholder: '¿Ya hay alguien asignado a esta misión?',

      device: 'Vehículo',
      device_placeholder: '¿Qué unidad va a llevar la carga?',

      route: 'Ruta',
      route_placeholder: '¿Quieres alertas si el vehículo sale de un camino en específico?',

      notes: 'Notas',
      notes_placeholder: '¿Hay indicaciones especiales para llegar a este lugar? ¿Algún procedimiento que seguir? ¡Este es el lugar!',

      notify: 'Notificar cuando',
      notify_stop_prefix: 'La unidad se detenga mas de',
      notify_route_prefix: 'La unidad salga de la ruta',
    },

    helpers: {
      loading: 'Cargando...',
      delete_warning: 'Esta acción no se podrá deshacer',
      delete_confirm: '¿Estas seguro que quieres borrar este objeto?',
      delete_button: 'Si, borrar',
      deleting_button: 'Borrando...',
      back_button: 'Regresar',

      save: 'Guardar',
      saving: 'Guardando...',

      go_back: 'Regresar'
    },

    classes: {
      device: 'Dispositivos',
      geofence: 'Geocercas',
      user: 'Usuarios',
      fleet: 'Flotas',
      route: 'Rutas'
    },

    links: {
      devices: 'Dispositivos',
      fleets: 'Flotas',
      geofences: 'Geocercas',
      logs: 'Registros',
      map: 'Mapa',
      routes: 'Rutas',
      settings: 'Configuraciones',
      sign_out: 'Cerrar sesión',
      trips: 'Viajes',
      users: 'Usuarios',
      reports: 'Reportes',
    },

    fleets: {
      title: 'Flotas',
      create: 'Crear flota',
      save: 'Guardar flota',
      cancel: 'Cancelar',
      update: 'Actualizar flota',
      delete: 'Borrar flota',

      name: 'Nombre de la flota',
      name_placeholder: 'Ciudad de México',

      abbr: 'Abreviación',
      abbr_placeholder: 'CDMX',

      empty_fleet: 'Todavía no hay dispositivos en esta flota',
      add_device_tutorial: 'Usa el buscador para agregar un dispositivo',
      device_suggestion_placeholder: 'Busca un dispositivo',
      add_device: 'Agregar dispositivo',
    },

    permissions: {
      add_permission: 'Agregar permiso',
      level_placeholder: 'Nivel',
      object_placeholder: 'Objeto',
      empty_user: 'Este usuario todavía no tiene permisos',
      add_permissions_tutorial: 'Usa el selector para agregar permisos'
    },

    access: {
      admin: 'Administrar',
      view: 'Ver'
    },

    history: {
      empty_list: 'No hay puntos para este día',
    },

    errors: {
      something_happened: 'Opps, intenta de nuevo mas tarde',
      go_back: 'Llévame de regreso a donde estaba',

      organization: {
        required: 'Este campo es necesario'
      },

      subdomain: {
        required: 'Este campo es necesario',
        unique: 'Este subdominio ya está en uso',
        invalid: 'Solo letras minúsculas y números',
      },

      name: {
        required: 'Al menos un nombre es necesario',
      },

      last_name: {
        required: 'Al menos un apellido es necesario',
      },

      email: {
        required: 'Este campo es necesario',
        unique: 'Este correo ya está en uso',
        invalid: 'Ingresa una dirección de correo válida',
      },

      password: {
        required: 'Este campo es necesario',
      },

      http: {
        method_not_allowed: 'Por alguna razón, esta petición no puede hacerse (HTTP 405)',
      },

      http404: {
        title: '404 objeto no encontrado',
        description: 'El objecto no se pudo encontrar en nuestros registros',
        button: 'Ir al inicio',
      },

      http401: {
        user: 'Verifica tu correo electrónico',
        password: 'La contraseña es incorrecta',
        disabled: 'El usuario no está activado',
        not_org_member: 'El usuario no pertenece a esta organización',
        invalid: 'La contraseña es incorrecta',
        not_found: 'Este usuario no existe, ¿escribiste bien tu correo?',
      },

      abbr: {
        required: 'La abreviación es requerida',
      },

      generic: {
        title: 'Oops!',
        description: 'Ocurrió un problema al cargar esta página, favor de intentar mas tarde',
        button: 'Ir al inicio',
      },

      perm: {
        invalid: 'Permiso inválido',
      },

      origin: {
        required: 'Este campo es requerido',
      },

      destination: {
        required: 'Este campo es requerido',
      },

      scheduled_departure: {
        invalid: 'Debe ser anterior a la fecha estimada de llegada',
      },

      scheduled_arrival: {
        invalid: 'Debe ser posterior a la fecha de salida',
      },
    },

    guides: {
      new_device: {
        step_1: '¡A continuación vas a registrar tu primer dispositivo!',
        step_2: 'En tu celular, visita la tienda de aplicaciones y busca Traccar Client. ¡Instálalo!',
        step_3: 'Dentro de la aplicación, cambia el identificador del dispositivo por',
        step_4: 'Ahora cambia la URL del servidor por ',
        step_5: 'Finalmente activa el selector con el nombre Estado del servicio. ¡Listo!',

        waiting_for_positions: 'Esperando ubicaciones...',
        next: 'Siguiente',
      },
    },

    logs: {
      title: 'Registros',
    },

    statuses: {
      trip: {
        SCHEDULED: 'Programado',
        ONGOING: 'En curso',
        FINISHED: 'Terminado',
      },

      device: {
        stopped: 'Detenido',
        online: 'Conectado',
        offline: 'Desconectado',
        moving: 'En movimiento',
        alarm: 'Alarma',
      },

      user: {
        active: 'Activo',
        disabled: 'Desactivado',
      },

      report: {
        COMPLETED: 'Completado',
        FAILED: 'Falló',
        IN_PROGRESS: 'En progreso',
      }
    },

    events: {
      names: {
        'alarm': 'Alarma',
        'geofence-enter': 'Entrada a geocerca',
        'geofence-leave': 'Salida de geocerca',
        'trip-started': 'Viaje iniciado',
        'trip-finished': 'Viaje terminado',
        '*': 'Todos los eventos',
      },

      all: 'Todos los eventos',
      meta: 'Plataforma Fleety',

      geofence_create: 'Se ha creado la geocerca',

      fleet_alarm: 'Alarma',
      fleet_alarm_for: 'para el dispositivo',

      the_device: 'El dispositivo',
      has: 'ha',
      entered: 'entrado',
      left: 'salido',
      to_the_geofence: 'a la geocerca',
      from_the_geofence: 'de la geocerca',

      /* Gría de escritura:
      * alarma <text> para el dispositivo <text> */
      alarm: {
        sos: 'SOS',
        powerCut: 'desconexión de corriente',
        fatigueDriving: 'conducción con fatiga',
        tripOffroute: 'dispositivo fuera de ruta',
        tripStop: 'dispositivo detenido durante ruta',
        myAlarm: 'mi alarma',
      },

      fleet: {
        create: 'Se ha creado un viaje para el dispositivo {{device.name}} desde {{trip.origin}} hasta {{trip.destination}}',
        trip_started: 'El dispositivo {{device.name}} comenzó un viaje de {{trip.origin}} a {{trip.destination}}',
        trip_finished: 'El dispositivo {{device.name}} terminó un viaje de {{trip.origin}} a {{trip.destination}}',
        trip_stop: 'El dispositivo {{device.name}} se detuvo durante el viaje de {{trip.origin}} a {{trip.destination}}',
        trip_offroute: 'El dispositivo {{device.name}} salió de su ruta durante el viaje de {{trip.origin}} a {{trip.destination}}',
      },

      user: {
        report_requested: 'Solicitaste un reporte del tipo {{report.builder}}',
        report_finished: 'El reporte tipo {{report.builder}} que solicitaste está listo',
        report_failed: 'El reporte tipo {{report.builder}} que solicitaste falló',
      },
    },

    settings: {
      org_code: 'Código de organización',
      next_device_identifier: 'Siguiente identificador de dispositivo',
    },
  }
};

export default es;
