import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import moment from 'moment';

import { Sidebar } from '../../Partials/Layout';
import Loading from '../../Partials/Loading';
import Errors from '../Errors';
import PageTitle from '../../components/PageTitle';
import ListFilter from '../../components/ListFilter';
import ListItem from '../../Partials/Trips/ListItem';
import subsequence from '../../utils/subsequence';

class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      items: [],
      filters: {},
      search: '',
    };
  }

  componentWillUpdate(newProps, newState) {
    if (
      (this.state.filters._build !== newState.filters._build) ||
      (this.state.items.length !== newState.items.length) ||
      (this.state.search !== newState.search)
    ) {
      this.buildFiltered(newState.filters, newState.search, newState.items);
    }
  }

  componentDidMount() {
    api.get('/v1/:organization/trip')
      .then(body => {
        this.setState({
          loading: false,
          items: body.data,
        })
      })
      .catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        this.setState({
          loading: false,
          errors: errors,
        });
      });
  }

  buildFiltered(filters, search, items) {
    let filtered = items;

    if (filters.status && Object.keys(filters.status).length) {
      filtered = filtered.filter(item => filters.status[item.status]);
    }

    if (search) {
      const textAttrs = [
        'origin',
        'destination'
      ];

      filtered = filtered.filter(item => {
        return textAttrs.reduce((flag, attr) => {
          if (flag) return flag;
          return subsequence(item[attr], search);
        }, false);
      });
    }

    this.filtered = filtered;
  }

  updateTrip(trip) {
    const items = this.state.items.map(item => {
      if (item.id === trip.id) {
        return trip;
      }

      return item;
    });

    const { filters, search } = this.state;
    this.buildFiltered(filters, search, items);

    this.setState({
      items,
    });
  }

  renderItems() {
    const { t } = this.props;

    if (this.state.items.length === 0) {
      return (
        <div className="list-empty">
          <h2>{t('trips.zero_trips')}</h2>
          <div>{t('trips.zero_trips_sub')}</div>
        </div>
      );
    }

    if (this.filtered.length === 0) {
      return (
        <div className="list-empty">
          <h2>{t('commons.no_results')}</h2>
          <div>{t('commons.no_results_feedback')}</div>
        </div>
      );
    }

    return (
      <div className="panel-group">
        {this.filtered.map(trip => (
          <ListItem
            key={trip.id}
            item={trip}
            onChange={this.updateTrip.bind(this)}
          />
        ))}
      </div>
    );
  }

  render() {
    let { t } = this.props;

    if (this.state.loading) {
      return <Loading />
    }

    if (this.state.errors) {
      return <Errors errors={this.state.errors} />
    }

    let createLink = null;
    if (api.permissions.allowedActions(api.organization + ':trip').indexOf('admin') !== -1) {
      createLink = (
        <div className="actions">
          <Link to="/trips/new" className="btn btn-lg btn-mint">{ t('trips.create') }</Link>
        </div>
      );
    }

    return (
      <div>
        <PageTitle title={t("trips.title")} />
        <Sidebar />

        <div className="has-sidebar">
          <div className="container">
            <div className="list-header">
              {createLink}

              <ListFilter
                fields={[{
                  label: 'Estado',
                  name: 'status',
                  type: 'tags',
                  options: [
                    {value: 'SCHEDULED', label: t('statuses.trip.SCHEDULED')},
                    {value: 'ONGOING', label: t('statuses.trip.ONGOING')},
                    {value: 'FINISHED', label: t('statuses.trip.FINISHED')},
                  ]
                }]}

                filters={this.state.filters}
                onChangeFilters={filters => this.setState({ filters })}

                search={this.state.search}
                searchPlaceholder={t('trips.search')}
                onChangeSearch={search => this.setState({ search })}
              />
              <div className="clearfix"></div>
            </div>

            <hr />

            {this.renderItems()}
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(List);
