import React, { Component } from 'react';
import GoogleMapsLoader from 'google-maps';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import settings from '../../settings';
import { Sidebar } from '../../Partials/Layout';
import Loading from '../../Partials/Loading';
import { AjaxButton } from '../../Partials/Helpers';

class Update extends Component {
  constructor(props) {
    super(props);

    this.map = null;
    this.drawingManager = null;
    this.polygon = null;

    const { geofence } = props;

    this.state = {
      saving: false,
      mode: 'edit-polygon',
      name: geofence.name,
    };
  }

  componentDidMount() {
    const { maps } = this.props;

    const map = this.map = new maps.Map(this.refs.map, {
      center: { lat: -24.043, lng: -103.861 },
      zoom: 5,
      options: {
        streetViewControl: true,
        mapTypeControl: true,
        fullscreenControl: false,
        mapTypeControlOptions: {
          style: 2,
          position: 3
        }
      }
    });

    this.resizeMap = this.resizeMap.bind(this);
    maps.event.addDomListener(window, 'resize', this.resizeMap);

    this.drawingManager = new maps.drawing.DrawingManager({
      drawingMode: null,
      drawingControl: false,
      polygonOptions: {
        strokeColor: '#206d5a',
        fillColor: '#37bc9b',
        fillOpacity: 0.5,
        editable: true,
        geodesic: true
      },
      map: map
    });

    this.drawingManager.addListener('overlaycomplete', this.onOverlayComplete.bind(this));

    this.renderPolyline();
  }

  componentDidUpdate() {
    const { maps } = this.props;

    switch (this.state.mode) {
      case 'draw-polygon':
        this.drawingManager.setOptions({
          drawingMode: maps.drawing.OverlayType.POLYGON
        });
        break;
      case 'edit-polygon':
        this.drawingManager.setOptions({
          drawingMode: null
        });
        break;
    }
  }

  componentWillUnmount() {
    const { maps } = this.props;
    maps.event.clearListeners(window, 'resize');
  }

  resizeMap() {
    const { map } = this;
    const { maps } = this.props;

    const center = map.getCenter();
    maps.event.trigger(map, 'resize');
    map.setCenter(center);
  }

  renderPolyline() {
    const { maps, geofence : { polyline } } = this.props;
    const { geometry: { encoding } } = maps;
    const path = encoding.decodePath(polyline);

    // First and last points are the same, so we could remove last point without problems
    path.pop();

    this.polygon = new maps.Polygon({
      strokeColor: '#206d5a',
      fillColor: '#37bc9b',
      fillOpacity: 0.5,
      editable: true,
      geodesic: true,
      path: path
    });

    let bounds = new maps.LatLngBounds();
    path.forEach(pos => bounds.extend(pos));

    this.polygon.setMap(this.map);
    this.map.fitBounds(bounds);
  }

  onOverlayComplete(event) {
    this.polygon = event.overlay;

    this.setState({
      mode: 'edit-polygon'
    });
  }

  clearDraw() {
    this.polygon.setMap(null);
    delete this.polygon;

    this.setState({
      mode: 'draw-polygon'
    });
  }

  onSubmit(event) {
    event.preventDefault();

    const { name } = this.state;
    const { geometry: { encoding } } = this.props.maps;

    if (name === '') {
      return;
    }

    let path = this.polygon.getPath();

    let simplePath = [];
    for (let i=0; i<path.length; i++) {
      simplePath.push(path.getAt(i));
    }
    simplePath.push(path.getAt(0));

    const geofence = {
      name: name,
      polyline: encoding.encodePath(simplePath)
    };

    this.setState({
      saving: true
    });

    const { id } = this.props.geofence;
    api.put(`/v1/:organization/geofence/${id}`, geofence)
      .then(() => {

        this.props.history.push('/geofences');

        this.setState({
          saving: false
        });
      })
      .catch(() => {
        // @TODO display saving error

        this.setState({
          saving: false
        });
      });
  }

  renderMapWidget() {
    const { t } = this.props;

    if (this.state.mode === 'draw-polygon') {
      return (
        <div>
          <Trans i18nKey="geofences.tutorial">
            <p>-</p>
            <ul>
              <li>-</li>
              <li>-</li>
            </ul>
          </Trans>
        </div>
      );
    } else if (this.state.mode === 'edit-polygon') {
      return (
        <form onSubmit={this.onSubmit.bind(this)}>
          <div className="form-group">
            <label htmlFor="inputEmail" className="control-label">
              { t('forms.geofence') }
            </label>
            <input
              id="inputEmail"
              type="text"
              className="form-control with-border"
              placeholder={t('forms.geofence_placeholder')}
              value={this.state.name}
              onChange={event => this.setState({
                name: event.target.value
              })}
            />
          </div>

          <div className="form-group">
            <div className="pull-right">
              <a
                href="javascript:void(0);"
                onClick={this.clearDraw.bind(this)}
              >{t('geofences.delete_draft')}</a>
              &nbsp;
              <AjaxButton
                loading={this.state.saving}
                disabled={this.state.name === ''}
                className="btn btn-mint"
                content={t('geofences.save_draft')}
                loadingClassName="btn-loading"
                loadingContent={t('helpers.saving')}
              />
            </div>
            <div className="clearfix" />
          </div>
        </form>
      );
    } else {
      return null;
    }
  }

  render() {
    return (
      <div id="map-viewport" className="has-sidebar">
        <Sidebar />

        <div id="omnibox">
          <div className="widget-box mint">
            <div className="widget-padding">
              <h4 className="widget-title">{ t('geofences.update') }</h4>
            </div>
          </div>
          <div className="widget-box">
            <div className="widget-padding">
              {this.renderMapWidget()}
            </div>
          </div>
        </div>

        <div className="full-height" ref="map" />
      </div>
    );
  }
}

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      geofence: null,
      maps: null
    };
  }

  componentDidMount() {
    const { id } = this.props.match.params;

    api.get(`/v1/:organization/geofence/${id}`)
      .then(body => {
        this.setState({
          loading: false,
          geofence: body.data
        });
      })
      .catch(err => {
        console.error(err);

        this.setState({
          loading: false,
          error: true
        });
      });

    GoogleMapsLoader.release();
    GoogleMapsLoader.LIBRARIES = ['geometry', 'visualization', 'drawing'];
    GoogleMapsLoader.KEY = settings.google_maps.map_api_key;
    GoogleMapsLoader.load(google => {
      this.setState({
        maps: google.maps
      });
    });
  }

  render() {
    if (this.state.geofence === null || this.state.maps === null) {
      return <Loading />;
    }

    const { props } = this;

    if (this.state.error) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('errors.http404.title') }</h2>
          { t('errors.http404.description') }
          <br />
          <Link to="/" className="btn btn-lg btn-mint">{ t('errors.http404.button') }</Link>
        </div>
      );
    }

    return (
      <Update
        maps={this.state.maps}
        geofence={this.state.geofence}
        {...props}
      />
    );
  }
}

export default withTranslation()(Container);
