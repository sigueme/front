import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import 'react-select/dist/react-select.css';

import React from 'react';
import ReactDOM from 'react-dom';
import './styles/fleety.scss';
import Root from './components/Root';
import reportWebVitals from './reportWebVitals';
import './i18n';

ReactDOM.render(
  <React.StrictMode>
    <Root />
  </React.StrictMode>,
  document.getElementById('app')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
