import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import GoogleMapsLoader from 'google-maps';

import settings from '../settings';
import { Sidebar } from '../Partials/Layout';
import Omnibox from './Omnibox.jsx';

import ClusterEngine from '../utils/ClusterEngine';

class DeviceMap extends Component {
  constructor(props) {
    super(props);

    this.map = null;
    this.listeners = [];
  }

  componentDidMount() {
    const { maps, items, t } = this.props;

    const map = this.map = new maps.Map(this.refs.map, {
      center: { lat: 24.043, lng: -103.861 },
      zoom: 15,
      options: {
        streetViewControl: true,
        mapTypeControl: true,
        fullscreenControl: false,
        mapTypeControlOptions: {
          style: 2,
          position: 3
        }
      }
    });

    const resizeListener = maps.event.addDomListener(
      window,
      'resize',
      this.resizeMap.bind(this)
    );
    this.listeners.push(resizeListener);

    maps.event.addListenerOnce(map, 'idle', () => {
      this.rebuildClusters();

      const zoomListener = maps.event.addDomListener(
        map,
        'zoom_changed',
        this.rebuildClusters.bind(this)
      );
      this.listeners.push(zoomListener);
    });

    // Build clusters
    this.clusters = new ClusterEngine({
      t,
      map,
      maps,
    });

    const OnClustersLoad = this.props.OnClustersLoad;
    if (typeof OnClustersLoad === 'function') {
      OnClustersLoad(this.clusters);
    }

    // Fit points
    let bounds = new maps.LatLngBounds();
    items
      .map(device => device.last_pos)
      .map(pos => new Object({ lat: pos.lat, lng: pos.lon }))
      .forEach(pos => bounds.extend(pos));

    map.fitBounds(bounds);
  }

  componentWillUpdate() {
    // Rebuild clusters
    this.rebuildClusters();
  }

  componentWillUnmount() {
    const { maps } = this.props;

    this.listeners.forEach(listener => maps.event.removeListener(listener));
  }

  rebuildClusters() {
    this.props.items.forEach(device => this.clusters.upsert(device, { render: false }));
    this.clusters.render();
  }

  resizeMap() {
    const { map } = this;
    const { maps } = this.props;

    if (!map) {
      return;
    }

    const center = map.getCenter();
    maps.event.trigger(map, 'resize');
    map.setCenter(center);
  }

  render() {
    return (
      <div id="map-viewport" className="has-sidebar">
        <Sidebar />

        <Omnibox />

        <div className="full-height" ref="map" />
      </div>
    );
  }
}

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      maps: null
    };
  }

  componentDidMount() {
    GoogleMapsLoader.release();
    GoogleMapsLoader.LIBRARIES = [];
    GoogleMapsLoader.KEY = settings.google_maps.map_api_key;
    GoogleMapsLoader.load(google => {
      this.setState({
        maps: google.maps
      });
    });
  }

  render() {
    const { props } = this;

    if (!this.state.maps) {
      return null;
    }

    return (
      <DeviceMap
        maps={this.state.maps}
        {...props}
      />
    );
  }
}

export default withTranslation()(Container);
