import DeviceMarkerBuilder from './DeviceMarker';
import ClusterMarkerBuilder from './ClusterMarker';
import Events from './events';
import moment from 'moment';

class Cluster extends Events {
  constructor(props) {
    super(props);

    this.props = props;

    this._count = 0;
    this._summary = {
      moving: 0,
      stopped: 0,
      offline: 0,
      alarm: 0,
    };

    this._center = {
      lat: 0,
      lon: 0,
    };

    this._devices = new Map();

    this.kdtree_node = null;

    this.markers = {
      cluster: ClusterMarkerBuilder(props.maps),
      device: DeviceMarkerBuilder(props.maps, props.t),
    };

    this._clusterMarker = null;
    this._deviceMarker = null;
  }

  destructor() {
    if (this._clusterMarker) {
      this._clusterMarker.onRemove();
      this._clusterMarker.setMap(null);
      this._clusterMarker = null;
    }

    if (this._deviceMarker !== null) {
      this._deviceMarker.onRemove();
      this._deviceMarker.setMap(null);
      this._deviceMarker = null;
    }

    if (this.kdtree_node) {
      this.props.tree.remove(this.kdtree_node);
      this.kdtree_node = null;
    }
  }

  insert(device) {
    if (this._devices.has(device.id)) {
      // Skip
      return;
    }

    const ref = Object.assign({}, device);
    this._devices.set(device.id, ref);

    // Update cluster's center
    // @TODO: this is math does not really work on the surface of a sphere
    // avg(S U {d}) = (avg(S)*|S| + d) / (|S|+1)
    this._center.lat = (this._center.lat * this._count + ref.last_pos.lat) / (this._count + 1);
    this._center.lon = (this._center.lon * this._count + ref.last_pos.lon) / (this._count + 1);

    if (this._summary[ref.status] !== undefined) {
      this._summary[ref.status] += 1;
    }

    this._count += 1;

    this.syncTree();
  }

  update(device) {
    if (!this._devices.has(device.id)) {
      // Skip
      return;
    }

    let ref = this._devices.get(device.id);
    const nref = Object.assign({}, device);

    const pos_changed =
      (Math.abs(ref.last_pos.lat - nref.last_pos.lat) > 1e-6) ||
      (Math.abs(ref.last_pos.lon - nref.last_pos.lon) > 1e-6);

    if (pos_changed) {
      // Update cluster's center
      // @TODO: this is math does not really work on the surface of a sphere
      // avg(S U {a} - {b}) = (avg(S)*|S| + a - b) / (|S|)
      this._center.lat = (this._center.lat * this._count - ref.last_pos.lat + nref.last_pos.lat) / (this._count);
      this._center.lon = (this._center.lon * this._count - ref.last_pos.lon + nref.last_pos.lon) / (this._count);
    }

    this._devices.set(device.id, nref);

    this._summary[ref.status] -= 1;
    this._summary[nref.status] += 1;
    ref = null;

    if (pos_changed) {
      this.syncTree();
    }
  }

  remove(device) {
    if (!this._devices.has(device.id)) {
      return;
    }

    const ref = this._devices.get(device.id);

    // Update cluster's center
    // @TODO: this is math does not really work on the surface of a sphere
    // avg(S - {d}) = (avg(S)*|S| - d) / (|S|-1)
    this._center.lat = (this._center.lat * this._count - ref.last_pos.lat) / (this._count - 1);
    this._center.lon = (this._center.lon * this._count - ref.last_pos.lon) / (this._count - 1);
    this.syncTree();

    this._summary[ref.status] -= 1;

    this._count -= 1;
    this._devices.delete(device.id);
  }

  syncTree() {
    const { tree } = this.props;

    if (this.kdtree_node !== null) {
      tree.remove(this.kdtree_node);
    }

    this.kdtree_node = Object.assign(
      { id: this.id },
      this._center,
    );
    tree.insert(this.kdtree_node);
  }

  center() {
    return this._center;
  }

  getClusterMarker(visible = true) {
    const { maps, map } = this.props;
    const position = new maps.LatLng(this._center.lat, this._center.lon);

    if (this._clusterMarker) {
      this._clusterMarker.setOptions({
        count: this._count,
        summary: this._summary,
        visibility: (visible ? 'visible' : 'hidden'),
      });

      this._clusterMarker.setPosition(position);
      this._clusterMarker.draw();
    } else {
      this._clusterMarker = new this.markers.cluster({
        map: map,
        position: position,
        count: this._count,
        summary: this._summary,
        visibility: (visible ? 'visible' : 'hidden'),
      });

      this._clusterMarker.addListener('click', () => {
        this.emit('click');
      });
    }

    return this._clusterMarker;
  }

  getDeviceMarker(visible = true) {
    const { maps, map } = this.props;
    const position = new maps.LatLng(this._center.lat, this._center.lon);
    const deviceIter = this._devices.values();
    const device = deviceIter.next().value;

    if (this._deviceMarker !== null) {
      this._deviceMarker.setOptions({
        id: device.id,
        name: device.name || device.code,
        status: device.status,
        course: device.last_course,
        last_speed: device.last_speed,
        last_report: moment(new Date(device.last_update)),
        visibility: (visible ? 'visible' : 'hidden'),
      });

      this._deviceMarker.setPosition(position);
    } else {
      this._deviceMarker = new this.markers.device({
        id: device.id,
        name: device.name || device.code,
        status: device.status,
        course: device.last_course,
        last_speed: device.last_speed,
        last_report: moment(new Date(device.last_update)),
        visibility: (visible ? 'visible' : 'hidden'),

        map: map,
        position: position,
      });
    }

    return this._deviceMarker;
  }

  render() {
    if (this._count > 1) {
      this.getClusterMarker(true);
      this.getDeviceMarker(false);
    } else {
      this.getClusterMarker(false);
      this.getDeviceMarker(true);
    }
  }
}

export default Cluster;
