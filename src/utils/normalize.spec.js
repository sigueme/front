import normalize from './normalize';

describe('Levenshtein', function() {
  it('must convert to lowercase', function() {
    expect(normalize("HOLA")).toEqual("hola");
  });

  it('must remove diacritics', function() {
    expect(normalize("Josué")).toEqual("josue");
  });

  it('must respect spaces', function() {
    expect(normalize("  Og Astorga \t\n Díaz")).toEqual("og astorga diaz");
  });
});
