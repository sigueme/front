export default function builder(maps, t) {
  function DeviceMarker(options) {
    options.course = options.course || 0;

    this.refs = {};
    this.latlng = options.position;
    this.setMap(options.map);
    this.options = options;
  }

  DeviceMarker.prototype = new maps.OverlayView();

  DeviceMarker.prototype.renderStructure = function() {
    const { refs } = this;

    const dot = this.renderDot();
    const prev = this.renderPrev();
    const infowindow = this.renderInfowindow();

    const inner = refs.inner = document.createElement('div');
    inner.className = 'device-marker';
    inner.appendChild(dot);
    inner.appendChild(prev);
    inner.appendChild(infowindow);

    const div = refs.div =  document.createElement('div');
    div.className = 'custom-marker _device';
    div.appendChild(inner);

    return div;
  }

  DeviceMarker.prototype.fillData = function() {
    const { refs, options } = this;

    // Set name
    refs.nameText.textContent = options.name;
    refs.headingTitleText.textContent = options.name;
    refs.headingTitle.title = options.name;

    // Set status
    refs.inner.className = 'device-marker status-' + options.status;

    // Set device course
    if (options.status === 'moving') {
      refs.dot.style.transform = `rotate(${options.course}rad)`;
    } else {
      refs.dot.style.transform = null;
    }

    // Set position
    const projection = this.getProjection();
    const point = projection.fromLatLngToDivPixel(this.latlng);
    if (point) {
      refs.div.style.left = point.x + 'px';
      refs.div.style.top = point.y + 'px';
    }

    // Set visibility
    refs.div.style.visibility = options.visibility;

    // Toggle prev/infowindow
    if (options.open) {
      refs.infowindow.style.display = 'block';
      refs.prev.style.display = 'none';
    } else {
      refs.infowindow.style.display = 'none';
      refs.prev.style.display = 'block';
    }

    // Set infowindow data
    if (options.open) {
      // id
      refs.deviceDetailLink.href = '#/device/' + options.id;

      // status
      refs.deviceStatusText.innerHTML = t('statuses.device.' + options.status);

      // location
      const lat = this.latlng.lat(), lng = this.latlng.lng();
      refs.deviceLocationLink.href = 'https://maps.google.com/maps/place/' + lat + ',' + lng;
      refs.deviceLocationLink.innerHTML = lat.toFixed(4) + ',' + lng.toFixed(4);

      // last_report
      refs.deviceLastReportText.innerHTML = options.last_report.fromNow();

      // last_speed
      let lastSpeedValue;
      if (options.status === 'moving' || options.status === 'alarm') {
        lastSpeedValue = (+options.last_speed).toFixed(2) + ' km/hr';
      } else {
        lastSpeedValue = t('commons.not_set') + ' km/hr';
      }
      refs.deviceLastSpeedText.innerHTML = lastSpeedValue;
    }
  }

  DeviceMarker.prototype.renderInfowindow = function() {
    const { refs } = this;

    const infowindow = refs.infowindow = document.createElement('div');
    const heading = document.createElement('div');
    const windowClose = document.createElement('div');
    const iconClose = document.createElement('i');
    const headingTitle = refs.headingTitle = document.createElement('div');
    const headingTitleText = refs.headingTitleText = document.createTextNode('');
    const infoContent = document.createElement('div');
    const caret = document.createElement('div');

    infowindow.className = 'infowindow';
    heading.className = 'heading';
    windowClose.className = 'window-close';
    iconClose.className = 'material-icons';
    headingTitle.className = 'heading-title';
    infoContent.className = 'info-content';
    caret.className = 'caret';

    iconClose.innerHTML = 'close';

    windowClose.appendChild(iconClose);
    heading.appendChild(windowClose);
    heading.appendChild(headingTitle);
    headingTitle.appendChild(headingTitleText);
    infowindow.appendChild(heading);
    infowindow.appendChild(infoContent);
    infowindow.appendChild(caret);

    /* device detail */
    const deviceDetail = document.createElement('p');
    infoContent.appendChild(deviceDetail);

    const deviceDetailIcon = document.createElement('i');
    deviceDetailIcon.className = 'material-icons';
    deviceDetailIcon.innerHTML = 'arrow_forward';
    deviceDetailIcon.title = t('devices.detail');
    deviceDetail.appendChild(deviceDetailIcon);

    const deviceDetailNbsp = document.createTextNode('\u00A0');
    deviceDetail.appendChild(deviceDetailNbsp);

    const deviceDetailLink = refs.deviceDetailLink = document.createElement('a');
    deviceDetailLink.innerHTML = t('devices.go_to_detail');
    deviceDetail.appendChild(deviceDetailLink);


    /* device status */
    const deviceStatus = document.createElement('p');
    infoContent.appendChild(deviceStatus);

    const deviceStatusIcon = document.createElement('i');
    deviceStatusIcon.className = 'material-icons';
    deviceStatusIcon.innerHTML = 'info';
    deviceStatusIcon.title = t('devices.status');
    deviceStatus.appendChild(deviceStatusIcon);

    const deviceStatusNbsp = document.createTextNode('\u00A0');
    deviceStatus.appendChild(deviceStatusNbsp);

    const deviceStatusText = refs.deviceStatusText = document.createElement('span');
    deviceStatus.appendChild(deviceStatusText);


    /* device location */
    const deviceLocation = document.createElement('p');
    infoContent.appendChild(deviceLocation);

    const deviceLocationIcon = document.createElement('i');
    deviceLocationIcon.className = 'material-icons';
    deviceLocationIcon.innerHTML = 'pin_drop';
    deviceLocationIcon.title = t('devices.location');
    deviceLocation.appendChild(deviceLocationIcon);

    const deviceLocationNbsp = document.createTextNode('\u00A0');
    deviceLocation.appendChild(deviceLocationNbsp);

    const deviceLocationLink = refs.deviceLocationLink = document.createElement('a');
    deviceLocationLink.target = '_blank';
    deviceLocation.appendChild(deviceLocationLink);


    /* device last_report */
    const deviceLastReport = document.createElement('p');
    infoContent.appendChild(deviceLastReport);

    const deviceLastReportIcon = document.createElement('i');
    deviceLastReportIcon.className = 'material-icons';
    deviceLastReportIcon.innerHTML = 'refresh';
    deviceLastReportIcon.title = t('devices.last_report');
    deviceLastReport.appendChild(deviceLastReportIcon);

    const deviceLastReportNbsp = document.createTextNode('\u00A0');
    deviceLastReport.appendChild(deviceLastReportNbsp);

    const deviceLastReportText = refs.deviceLastReportText = document.createElement('span');
    deviceLastReport.appendChild(deviceLastReportText);


    /* device last_speed */
    const deviceLastSpeed = document.createElement('p');
    infoContent.appendChild(deviceLastSpeed);

    const deviceLastSpeedIcon = document.createElement('i');
    deviceLastSpeedIcon.className = 'material-icons';
    deviceLastSpeedIcon.innerHTML = 'network_check';
    deviceLastSpeedIcon.title = t('devices.last_speed');
    deviceLastSpeed.appendChild(deviceLastSpeedIcon);

    const deviceLastSpeedNbsp = document.createTextNode('\u00A0');
    deviceLastSpeed.appendChild(deviceLastSpeedNbsp);

    const deviceLastSpeedText = refs.deviceLastSpeedText = document.createElement('span');
    deviceLastSpeed.appendChild(deviceLastSpeedText);

    maps.event.addDomListener(windowClose, 'click', () => {
      this.options.open = false;
      this.fillData();
    });

    return infowindow;
  }

  DeviceMarker.prototype.renderPrev = function() {
    const { refs } = this;

    const prev = refs.prev = document.createElement('div');
    const name = document.createElement('div');
    const nameText = refs.nameText = document.createTextNode('');
    const caret = document.createElement('div');

    prev.className = 'prev';
    name.className = 'name';
    caret.className = 'caret';

    name.appendChild(nameText);
    prev.appendChild(name);
    prev.appendChild(caret);

    maps.event.addDomListener(name, 'click', () => {
      this.options.open = true;
      this.fillData();
    });

    return prev;
  }

  DeviceMarker.prototype.renderDot = function() {
    const { refs } = this;

    const dot = refs.dot = document.createElement('div');
    dot.className = 'dot';

    return dot;
  }

  DeviceMarker.prototype.draw = function () {
    const map = this.map;
    const panes = this.getPanes();
    const projection = this.getProjection();

    // If there is no map or the map is not ready, skip render
    if (!map || !panes || !projection) {
      return;
    }

    if (!this.refs.div) {
      const div = this.renderStructure();

      // ?
      if (typeof(this.options.marker_id) !== 'undefined') {
        div.dataset.marker_id = this.options.marker_id;
      }

      // ?
      maps.event.addDomListener(div, 'click', () => {
        maps.event.trigger(this, 'click');
      });

      panes.overlayImage.appendChild(div);
    }

    this.fillData();
  };

  DeviceMarker.prototype.onRemove = function() {
    if (this.refs.div) {
      if (this.refs.div.parentNode) {
        this.refs.div.parentNode.removeChild(this.refs.div);
      }

      this.refs.div = null;
    }
  };

  DeviceMarker.prototype.hide = function() {
    if (this.refs.div) {
      this.refs.div.style.visibility = 'hidden';
    }
  };

  DeviceMarker.prototype.show = function() {
    if (this.refs.div) {
      this.refs.div.style.visibility = 'visible';
    }
  };

  DeviceMarker.prototype.setPosition = function(latlng) {
    this.latlng = latlng;
    this.draw();
  };

  DeviceMarker.prototype.setOptions = function(options) {
    for (let attr in options) {
      this.options[attr] = options[attr];
    }
  };

  return DeviceMarker;
}
