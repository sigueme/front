import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import { Sidebar } from '../../Partials/Layout';
import Loading from '../../Partials/Loading';
import Errors from '../Errors';
import Permissions from '../../Partials/Users/Permissions';
import PageTitle from '../../components/PageTitle';

class Read extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      user: null,
    };
  }

  componentDidMount() {
    this.fetchModel();
  }

  fetchModel() {
    const { id } = this.props.match.params;

    this.setState({
      loading: true
    });

    api.get('/v1/:organization/user/' + id)
      .then(body => {
        this.setState({
          loading: false,
          user: body.data
        });
      })
      .catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        errors.map(error => console.error(error));

        this.setState({
          loading: false,
          errors: errors
        });
      })
  }

  renderContent() {
    const { t } = this.props;
    const { user } = this.state;

    return (
      <div>
        <div className="panel-body">
          <div className="media">
            <div className="media-left">
              <img
                alt="64x64"
                className="media-object"
                data-src="holder.js/64x64"
                style={{ width: '64px', height: '64px' }}
                src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWRhZWUwNDEwNyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1ZGFlZTA0MTA3Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi44Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg=="
              />
            </div>
            <div className="media-body">
              <div className="media-heading">
                {user.name + ' ' + user.last_name}
              </div>
              <div>
                <span className="label label-primary">{user.email}</span>
              </div>
            </div>
          </div>
        </div>

        <Permissions user={user} />
      </div>
    );
  }

  render() {
    const { t } = this.props;

    if (this.state.loading) {
      return <Loading />
    }

    if (this.state.errors) {
      return <Errors errors={this.state.errors} />
    }

    return (
      <div>
        <PageTitle title={t("commons.permissions")} />
        <Sidebar />

        <div className="has-sidebar">
          <div className="spacer40"></div>
          <div className="container container-small">
            <div className="panel panel-default">
              {this.renderContent()}
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default withTranslation()(Read);
