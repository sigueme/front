import serialize from './serialize';
import Events from './events';
import PermissionTree from './permissionTree';

import io from 'socket.io-client';

class API extends Events {
  constructor(props = {}) {
    super(props);

    this.version = '0.1';

    // Assign props
    let defaultProps = {};

    if (typeof window !== 'undefined') {
      defaultProps.location = window.location.hostname;
    }

    this.props = Object.assign(defaultProps, props);

    // Decomponse location into [org] and [host]
    this._decompose();

    // build HTTP methods
    this.get = this._fetch.bind(this, 'GET');
    this.put = this._fetch.bind(this, 'PUT');
    this.post = this._fetch.bind(this, 'POST');
    this.delete = this._fetch.bind(this, 'DELETE');

    // seek localStorage for _signedInUser
    this._signedInUser = null;
    this._permissions = new PermissionTree([]);

    if (typeof localStorage !== 'undefined') {
      const userJson = localStorage['signedInUser'];
      const orgJson = localStorage['org'];

      if (typeof userJson === 'string' && userJson.length && typeof orgJson === 'string' && orgJson.length) {
        try {
          const user = JSON.parse(userJson);
          const org = JSON.parse(orgJson);

          this._signedInUser = user;
          this.permissions = user.permissions;
          this.org = org;
        } catch (err) {
          console.error(err);
          this._signedInUser = null;
        }
      }

      // Refresh loaded user to be synced with backend
      this.refreshSignedInUser();
    }

    // events listener store
    this.listeners = new Map();
  }

  _decompose() {
    const { location, hostname } = this.props;

    const match = location.match(/^(([a-z][a-z0-9]+)\.)?([a-z0-9]+\.[a-z0-9]*)$/i);

    this.organization = match[2] ? match[2] : null;
    this.hostname = hostname || match[3];
  }

  _endpoint() {
    const { hostname } = this;

    // if secure flag, use https
    const secure = this.props.secure ? 's' : '';
    const subdomain = this.props.subdomain;

    // compose endpoint
    let endpoint = `http${secure}://${subdomain}.${hostname}`;

    return endpoint;
  }

  _fetch(method, route, params = {}) {
    const endpoint = this._endpoint();

    // Add '/' to route if not already present
    if (route.charAt(0) !== '/') {
      route = '/' + route;
    }

    if (route.match(/:organization/)){
      if (this.organization === null) {
        throw new Error('You can not use :organization param if not in organization subdomain');
      }

      route = route.replace(/:organization/g, this.organization);
    }

    let headers = {};
    let options = {
      method: method
    };

    // Append autorization
    if (this.user && this.user) {
      const { api_key } = this.user;
      let auth = ['api', api_key].join(':');

      if (typeof window !== 'undefined') {
        auth = btoa(auth);
      } else {
        auth  = new Buffer(auth).toString('base64');
      }

      headers['Authorization'] = 'Basic ' + auth;
    }

    let url = endpoint + route;

    // Append params as form urlencoded in body request
    const body = serialize(params);

    if (body.length > 0) {
      if (method === 'GET') {
        url += '?' + body;
      } else {
        headers['charset'] = 'UTF-8';
        headers['Content-Type'] = 'application/x-www-form-urlencoded';

        options.body = body;
      }
    }

    options.headers = headers;

    const self = this;

    return new Promise(function (resolve, reject) {
      let response = undefined;
      fetch(url, options)
        .then(res => {
          response = res;

          // No content to parse
          if (response.status === 204) {
            return Promise.resolve(new Object());
          }

          // Current user is not really signed in
          if (response.status === 401) {
            self.user = null;
            self.org = null;
          }

          return res.json();
        })
        .then(body => {
          if (body.errors) {
            console.error(body.errors);

            throw body.errors;
          }

          body.response = response;

          resolve(body);
        })
        .catch(err => reject(err));
    });
  }

  search(topic, input, callback, _nameCallback) {
    if (input.length === 0) {
      return callback(null);
    }

    let nameCallback = item => item.name;

    if (typeof _nameCallback == 'function') {
      nameCallback = _nameCallback;
    }

    this.get('v1/:organization/search', { q: input, context: topic})
      .then(body => {
        const items = body.data;

        callback(null, {
          options: items.map(item => {
            return {
              id: item.id,
              name: nameCallback(item),
              _model: item,
            };
          }),
          complete: true
        });
      })
      .catch(err => {
        console.error(err);
        return callback(null);
      });
  }

  signup(credentials) {
    return this.post('auth/register', credentials);
  }

  signin(credentials) {
    this.user = null;
    this.org = null;

    if (this.organization && !credentials.org_subdomain) {
      credentials.org_subdomain = this.organization;
    }

    let path = credentials.api_key !== undefined ? 'auth/key_login' : 'auth/login';

    return this.post(path, credentials)
      .then(body => {
        if (body.data && body.data.session) {
          const user = body.data.session;

          this.user = user;
          this.org = body.data.org;
          setTimeout(this.emit.bind(this, 'signin', user), 15);
        }

        return Promise.resolve(body);
      });
  }

  signout() {
    this.user = null;
    this.org = null;
    setTimeout(this.emit.bind(this, 'signout'), 15);
  }

  socket(channels, callback) {
    let socket = io(this._endpoint());

    socket.on('connect', () => {
      socket.emit('authentication', {
        api_key: this.user.api_key,
        org: this.organization,
        channels: channels,
      });

      socket.on('unauthorized', (err) => {
        log.error(`Couldn\'t authenticate socket ${socket.id}`, err.message);
      });

      socket.on('authenticated', () => {
        log.info(`Authenticated socket ${socket.id}`);
        socket.on('message', callback);
      });
    });

    return socket;
  }

  refreshSignedInUser() {
    this.get('auth/ping', {
      org_subdomain: this.organization
    })
    .then(body => {
      const user = body.data.session;
      const org = body.data.org;

      this._signedInUser = user;
      this.org = org;

      // Build permissions
      if (user && user.permissions instanceof Array) {
        this.permissions = user.permissions;
      } else {
        this.permissions = [];
      }

      // Save to localStorage
      if (typeof localStorage !== 'undefined') {
        localStorage.setItem('signedInUser', JSON.stringify(user));
        localStorage.setItem('org', JSON.stringify(org));
      }
    });
  }

  set permissions(list) {
    if (!(list instanceof Array)) {
      throw 'permission list must be an array of strings';
    }

    this._permissions = new PermissionTree(list);

    setTimeout(this.emit.bind(this, 'change_permissions', this._permissions), 15);
  }

  get permissions() {
    return this._permissions;
  }

  fillPermissions(objects) {
    if (!(objects instanceof Array)) {
      objects = [objects];
    }

    return objects.map(object => {
      const key = object._type + ':' + object.id;

      object.permissions = this.permissions.allowedActions(key);

      return object;
    });
  }

  set user(user) {
    this._signedInUser = user;

    // Save to localStorage
    if (typeof localStorage !== 'undefined') {
      localStorage.setItem('signedInUser', JSON.stringify(user));
    }

    // Build permissions
    if (user && user.permissions instanceof Array) {
      this.permissions = user.permissions;
    } else {
      this.permissions = [];
    }
  }

  get user() {
    return this._signedInUser;
  }
}

export default API;
