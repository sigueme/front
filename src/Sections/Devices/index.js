import Read from './Read.jsx';
import List from './List.jsx';

const namespace = new Object({
  Read,
  List,
});

export default namespace;
