import React from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

const Render = (props) => {
  const { log } = props;
  const { data } = log;

  switch (log.event) {
    case 'geofence-enter':
      return (
        <Trans i18nKey="logs.event_geofence_enter">
          Device
          &nbsp;
          <Link to={'/device/' + data.device.id}>{data.device.name || data.device.code}</Link>
          &nbsp;
          has arrived to
          &nbsp;
          <Link to={'/geofence/' + data.geofence.id}>
            { data.geofence.name }
          </Link>
        </Trans>
      );

    case 'geofence-leave':
      return (
        <Trans i18nKey="logs.event_geofence_leave">
          Device
          &nbsp;
          <Link to={'/device/' + data.device.id}>{data.device.name || data.device.code}</Link>
          &nbsp;
          has leaved from
          &nbsp;
          <Link to={'/geofence/' + data.geofence._id}>
            { data.geofence.name }
          </Link>
        </Trans>
      );

    case 'trip-start':
      return (
        <Trans i18nKey="logs.event_trip_start">
          Device
          &nbsp;
          <Link to={'/device/' + data.device.id}>{data.device.name || data.device.code}</Link>
          &nbsp;
          has started a
          &nbsp;
          <Link to={'/trip/' + data.trip.id}>
            trip
          </Link>
        </Trans>
      );

    case 'trip-finish':
      return (
        <Trans i18nKey="logs.event_trip_finish">
          Device
          &nbsp;
          <Link to={'/device/' + data.device.id}>{data.device.name || data.device.code}</Link>
          &nbsp;
          has finished its
          &nbsp;
          <Link to={'/trip/' + data.trip.id}>
            trip
          </Link>
        </Trans>
      );

    default:
      return (
        <div>{log.event}</div>
      );
  }
};

export default withTranslation()(Render);
