import React, { Component } from 'react';
import GoogleMapsLoader from 'google-maps';
import { withTranslation } from 'react-i18next';
import Select from 'react-select';
import moment from 'moment';

import settings from '../../settings';
import { Sidebar } from '../../Partials/Layout';
import { AjaxButton } from '../../Partials/Helpers';
import DateTimeInput from '../../components/DateTimeInput';
import IntegerInput from '../../components/IntegerInput';
import PageTitle from '../../components/PageTitle';

class Create extends Component {
  constructor(props) {
    super(props);

    this.state = {
      departurePickerFocused: false,
      arrivalPickerFocused: false,

      origin: '',
      origin_pos: '',
      destination: '',
      destination_pos: '',
      scheduled_departure: moment().startOf('day'),
      scheduled_arrival: moment().startOf('day'),
      device: null,
      user: null,
      route: null,

      eta: 0,
      notifyStop: null,

      loading: false,
      errors: [],
    };
  }

  componentDidMount() {
    GoogleMapsLoader.release();
    GoogleMapsLoader.LIBRARIES = ['places'];
    GoogleMapsLoader.KEY = settings.google_maps.map_api_key;
    GoogleMapsLoader.load(google => {
      let maps = google.maps;

      this.originInput = new maps.places.Autocomplete(this.refs.origin, {
        types: ['(cities)'],
      });
      this.originInput.addListener('place_changed', () => {
        const data = this.originInput.getPlace();
        const pos = `${data.geometry.location.lng()},${data.geometry.location.lat()}`;

        this.setState({
          origin: data.formatted_address,
          origin_pos: pos,
        });
      });

      this.destinationInput = new maps.places.Autocomplete(this.refs.destination, {
        types: ['(cities)'],
      });
      this.destinationInput.addListener('place_changed', () => {
        const data = this.destinationInput.getPlace();
        const pos = `${data.geometry.location.lng()},${data.geometry.location.lat()}`;

        this.setState({
          destination: data.formatted_address,
          destination_pos: pos,
        });
      });
    });

    this.refs.origin.focus();
  }

  handleSubmit(event) {
    event.preventDefault();

    if (this.state.loading) {
      return;
    }

    const {
      user,
      route,
      device
    } = this.state;

    const params = {
      origin: this.state.origin || '',
      origin_pos: this.state.origin_pos || '',
      destination: this.state.destination || '',
      destination_pos: this.state.destination_pos || '',
      scheduled_departure: this.state.scheduled_departure.toISOString().replace(/\.\d{3}/, ''),
      scheduled_arrival: this.state.scheduled_arrival.toISOString().replace(/\.\d{3}/, ''),
      device_id: device ? device.id : '',
      user_id: user ? user.id : '',
      route_id: route ? route.id : '',
      notes: this.state.notes || '',
      notify_stop: this.state.notifyStop || '',
    };

    this.setState({
      loading: true,
      errors: [],
    });

    api.post('/v1/:organization/trip', params)
      .then(body => {
        if (body.errors !== undefined) {
          const state = body.errors.reduce((errors, error) => {
            errors[error.field + 'Error'] = error;
            return errors;
          }, {});

          state.loading = false;

          return this.setState(state);
        }

        // redirect
        this.props.history.push('/trips');
      }).catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        let state = {
          loading: false,
          errors: []
        };

        errors.forEach(error => {
          if (!error.field) {
            state.errors.push(error);
            return;
          }

          if (state[error.field + 'Errors'] === undefined) {
            state[error.field + 'Errors'] = [];
          }

          state[error.field + 'Errors'].push(error);
        });

        this.setState(state);
      });
  }

  suggestDevice(input, callback) {
    api.search('device', input, callback);
  }

  suggestUser(input, callback) {
    api.search('user', input, callback);
  }

  suggestRoute(input, callback) {
    api.search('route', input, callback);
  }

  departureIsOutsideRange() {
    return false;
  }

  arrivalIsOutsideRange(date) {
    return date.isBefore(this.state.departureDate, 'day');
  }

  handleDepartureChange(dt) {
    let scheduled_departure = dt;
    let scheduled_arrival = scheduled_departure.clone();
    let eta = this.state.eta;

    scheduled_arrival.add(eta, 'hours');

    this.setState({
      scheduled_departure,
      scheduled_arrival,
      scheduled_departureErrors: [],
      scheduled_arrivalErrors: [],
    });
  }

  handleArrivalChange(dt) {
    let scheduled_arrival = dt;
    let scheduled_departure = this.state.scheduled_departure;
    let eta = scheduled_arrival.diff(scheduled_departure, 'hours');

    this.setState({
      eta,
      scheduled_arrival,
      scheduled_departureErrors: [],
      scheduled_arrivalErrors: [],
    });
  }

  handleEtaChange(eta) {
    let scheduled_arrival = this.state.scheduled_departure.clone();

    scheduled_arrival.add(eta, 'hours');

    this.setState({
      eta,
      scheduled_arrival,
      scheduled_departureErrors: [],
      scheduled_arrivalErrors: [],
    });
  }

  renderEndpoints() {
    const { t } = this.props;

    let originClassName = 'form-group';
    let destinationClassName = 'form-group';
    let originHelpBlock = '';
    let destinationHelpBlock = '';

    if (this.state.originErrors !== undefined && this.state.originErrors.length>0) {
      originClassName += ' has-error';
      originHelpBlock = t(this.state.originErrors[0].i18n);
    }

    if (this.state.destinationErrors !== undefined && this.state.destinationErrors.length>0) {
      destinationClassName += ' has-error';
      destinationHelpBlock = t(this.state.destinationErrors[0].i18n);
    }

    return (
      <div className="row">
        <div className="col-xs-6">
          <div className={originClassName}>
            <label htmlFor="inputOrigin" className="control-label">
              { t('forms.origin') }
            </label>
            <input
              id="inputOrigin"
              type="text"
              ref="origin"
              className="form-control"
              placeholder={t('forms.origin_placeholder')}
              value={this.state.origin}
              onChange={event => this.setState({
                origin: event.target.value,
                originErrors: [],
              })}
              autoFocus
            />
            <p className="help-block">{originHelpBlock}</p>
          </div>
        </div>

        <div className="col-xs-6">
          <div className={destinationClassName}>
            <label htmlFor="inputOrigin" className="control-label">
              { t('forms.destination') }
            </label>
            <input
              type="text"
              ref="destination"
              className="form-control"
              placeholder={t('forms.destination_placeholder')}
              value={this.state.destination}
              onChange={event => this.setState({
                destination: event.target.value,
                destinationErrors: [],
              })}
            />
            <p className="help-block">{destinationHelpBlock}</p>
          </div>
        </div>
      </div>
    );
  }

  renderDates() {
    const { t } = this.props;

    return (
      <div className="row">
        <div className="col-xs-4">
          <DateTimeInput
            value={this.state.scheduled_departure}
            onChange={this.handleDepartureChange.bind(this)}
            errors={this.state.scheduled_departureErrors}
            label="forms.departure"
          />
        </div>

        <div className="col-xs-4">
          <label htmlFor="inputDeparture" className="control-label">
            { t('forms.eta') }
          </label>
          <IntegerInput
            value={this.state.eta}
            onChange={this.handleEtaChange.bind(this)}
            placeholder={t('forms.eta_placeholder')}
          />
        </div>

        <div className="col-xs-4">
          <DateTimeInput
            value={this.state.scheduled_arrival}
            onChange={this.handleArrivalChange.bind(this)}
            errors={this.state.scheduled_arrivalErrors}
            label="forms.arrival"
          />
        </div>
      </div>
    );
  }

  renderUser() {
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (this.state.userError) {
      className += ' has-error';
      helpBlock = t(this.state.user_idError.i18n);
    }

    return (
      <div className={className}>
        <label htmlFor="inputUser" className="control-label">
          { t('forms.user') }
        </label>
        <Select.Async
          placeholder={t('forms.user_placeholder')}
          loadOptions={this.suggestUser.bind(this)}
          value={this.state.user}
          onChange={option => this.setState({ user: option })}
          labelKey="name"
          valueKey="id"
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  renderDevice() {
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (this.state.deviceError) {
      className += ' has-error';
      helpBlock = t(this.state.device_idError.i18n);
    }

    return (
      <div className={className}>
        <label htmlFor="inputDevice" className="control-label">
          { t('forms.device') }
        </label>
        <Select.Async
          placeholder={t('forms.device_placeholder')}
          loadOptions={this.suggestDevice.bind(this)}
          value={this.state.device}
          onChange={option => this.setState({ device: option })}
          labelKey="name"
          valueKey="id"
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  renderNotes() {
    const { t } = this.props;

    return (
      <div className="form-group">
        <label htmlFor="inputNotes" className="control-label">
          { t('forms.notes') }
        </label>
        <textarea
          id="inputNotes"
          className="form-control"
          placeholder={t('forms.notes_placeholder')}
          value={this.state.notes}
          onChange={event => this.setState({
            notes: event.target.value,
          })}
        />
      </div>
    );
  }

  renderNotifications() {
    const { t } = this.props;

    return (
      <div className="form-group">
        <label htmlFor="inputNotes" className="control-label">
          { t('forms.notify') }
        </label>

        <div className="form-horizontal">
          <div className="form-group">
            <div className="control-label col-xs-6">
              { t('forms.notify_stop_prefix') }
            </div>
            <div className="col-xs-6">
              <Select
                placeholder="-"
                name="form-time"
                options={[
                  { value: 1, label: t('commons.1m') },
                  { value: 10, label: t('commons.10m') },
                  { value: 30, label: t('commons.30m') },
                  { value: 60, label: t('commons.60m') },
                  { value: 120, label: t('commons.120m') },
                ]}
                value={this.state.notifyStop}
                onChange={selected => {
                  let notifyStop = null;
                  if (selected) {
                    notifyStop = selected.value;
                  }

                  this.setState({ notifyStop });
                }}
              />
            </div>
          </div>

          <div className="form-group">
            <div className="control-label col-xs-6">
              { t('forms.notify_route_prefix') }
            </div>
            <div className="col-xs-6">
              <Select.Async
                labelKey="name"
                valueKey="id"
                placeholder={t('routes.search')}
                loadOptions={this.suggestRoute.bind(this)}
                value={this.state.route}
                onChange={route => {
                  this.setState({ route });
                }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { t } = this.props;

    return (
      <div>
        <PageTitle title={t("trips.create")} />
        <Sidebar />

        <div className="has-sidebar">
          <div className="container container-small">
            <h3>{ t('trips.create') }</h3>
            <hr />
            {this.state.errors.map(error => (
              <div key={error.i18n} className="alert alert-danger">
                <i className="material-icons">info</i>
                &nbsp;
                { t(error.i18n) }
              </div>
            ))}
            <form onSubmit={this.handleSubmit.bind(this)}>

              {this.renderEndpoints()}
              {this.renderDates()}
              {this.renderDevice()}
              {this.renderUser()}
              {this.renderNotes()}
              {this.renderNotifications()}

              <div className="pull-right">
                <a href="javascript:void(0);" onClick={() => this.props.history.goBack()}>
                  {t('commons.cancel')}
                </a>
                &nbsp;
                <AjaxButton
                  type="submit"
                  loading={this.state.loading}
                  className="btn btn-lg btn-primary"
                  content={t('commons.save')}
                  loadingClassName="btn-loading"
                  loadingContent={t('helpers.saving')}
                />
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(Create);
