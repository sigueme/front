import merge from './merge';

describe('Merge', function() {
  it('must merge recursively', function() {
    const o1 = {
      a: 1,
      b: 1,
      c: {
        a: 1,
        b: 1,
        c: 1,
        d: 1,
      }
    };

    const o2 = {
      a: 2,
      c: {
        d: 2,
        e: 2,
        f: 2,
      },
    };

    const o3 = merge(o1, o2);
    expect(o3).toMatchObject({
      a: 2,
      b: 1,
      c: {
        a: 1,
        b: 1,
        c: 1,
        d: 2,
        e: 2,
        f: 2,
      },
    });
  });
});
