import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import { Sidebar } from '../../Partials/Layout';
import Loading from '../../Partials/Loading';
import Errors from '../Errors';
import PageTitle from '../../components/PageTitle';
import ListFilter from '../../components/ListFilter';
import ListItem from '../../Partials/Devices/ListItem';
import subsequence from '../../utils/subsequence';

class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      items: [],
      filters: {},
      search: '',
    };
    this.filtered = [];
  }

  componentWillUpdate(newProps, newState) {
    if (
      (this.state.filters._build !== newState.filters._build) ||
      (this.state.items.length !== newState.items.length) ||
      (this.state.search !== newState.search)
    ) {
      this.buildFiltered(newState);
    }
  }

  componentDidMount() {
    api.get('/v1/:organization/device?embed=fleet.name')
      .then(body => {
        this.setState({
          loading: false,
          items: body.data,
        });
      })
      .catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        this.setState({
          loading: false,
          errors: errors
        });
      });
  }

  buildFiltered(state) {
    const { filters, search, items } = state;
    let filtered = items;

    if (filters.status && Object.keys(filters.status).length) {
      filtered = filtered.filter(item => filters.status[item.status]);
    }

    if (search) {
      const textAttrs = [
        'name',
        'code'
      ];

      filtered = filtered.filter(item => {
        return textAttrs.reduce((flag, attr) => {
          if (flag) return flag;
          return subsequence(item[attr], search);
        }, false);
      });
    }

    this.filtered = filtered;
  }

  renderItems() {
    const { t } = this.props;

    if (this.state.items.length === 0) {
      return (
        <div className="list-empty">
          <h2>{t('devices.zero_devices')}</h2>
          <div>{t('devices.add_device')}</div>
        </div>
      );
    }

    if (this.filtered.length === 0) {
      return (
        <div className="list-empty">
          <h2>{t('commons.no_results')}</h2>
          <div>{t('commons.no_results_feedback')}</div>
        </div>
      );
    }

    return (
      <div className="panel-group">
        {this.filtered.map(item => (
          <ListItem
            key={item.id}
            item={item}
          />
        ))}
      </div>
    );
  }

  render() {
    if (this.state.loading) {
      return <Loading />
    }

    if (this.state.errors) {
      return <Errors errors={this.state.errors} />
    }

    const { t } = this.props;
    const { users } = this.state;

    return (
      <div>
        <PageTitle title={t("devices.title")} />
        <Sidebar />

        <div className="has-sidebar">
          <div className="container">
            <div className="list-header">
              <ListFilter
                fields={[{
                  label: 'Estado',
                  name: 'status',
                  type: 'tags',
                  options: [
                    {value: 'stopped', label: t('statuses.device.stopped')},
                    {value: 'offline', label: t('statuses.device.offline')},
                    {value: 'moving', label: t('statuses.device.moving')},
                    {value: 'alarm', label: t('statuses.device.alarm')},
                  ]
                }]}

                filters={this.state.filters}
                onChangeFilters={filters => this.setState({ filters })}

                search={this.state.search}
                searchPlaceholder={t('devices.search')}
                onChangeSearch={search => this.setState({ search })}
              />
            </div>

            <br/>

            {this.renderItems()}
          </div>
        </div>
      </div>
    )
  }
}

export default withTranslation()(List);
