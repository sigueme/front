import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import { Sidebar } from './Layout';

class Loading extends Component {
  render() {
    const { t } = this.props;

    return (
      <div>
        <Sidebar />

        <div className="has-sidebar">
          <div className="list-empty">
            <br />
            <h2>{ t('helpers.loading') }</h2>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(Loading);
