import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

class SignOut extends Component {
  componentDidMount() {
    api.signout()
    this.props.history.push('/');
  }

  render() {
    const { t } = this.props;

    return (
      <div className="container">
        <br />
        <h4>
          { t('sign_out.signing_out') }
        </h4>
      </div>
    );
  }
};

export default withTranslation()(SignOut);
