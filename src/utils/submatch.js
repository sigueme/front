import normalize from './normalize';

const submatch = function(str, query) {
  str = normalize(str);
  query = normalize(query);

  const query_words = query.split(' ');
  return query_words
    .map(word => {
      let pnt = 0;
      for (let i=0; i<str.length; ++i) {
        if (word.charAt(pnt) == str.charAt(i)) {
          pnt++;

          if (pnt === word.length) {
            return true;
          }
        }

        if (str.charAt(i) === ' ') {
          pnt = 0;
        }
      }

      return false;
    })
    .reduce((a,b) => a&&b, true);
};

export default submatch;
