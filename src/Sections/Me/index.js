import Subscriptions from './Subscriptions.jsx';

const namespace = new Object({
  Subscriptions,
});

export default namespace;
