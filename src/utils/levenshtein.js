const levenshtein = function(a, b) {
  const N = a.length + 1, M = b.length + 1;
  const matrix = new Int32Array(N*M);

  for (let i=0; i<N; ++i) {
    matrix[i*M + 0] = i;
  }

  for (let j=0; j<M; ++j) {
    matrix[0 + j] = j;
  }

  for (let i=1; i<N; ++i) {
    for (let j=1; j<M; ++j) {
      let cost = +(a.charAt(i-1) !== b.charAt(j-1));

      matrix[(i)*M + j] = Math.min(
        matrix[(i-1)*M + j] + 1,
        matrix[(i)*M + j-1] + 1,
        matrix[(i-1)*M + j-1] + cost,
      );
    }
  }

  return matrix[N*M-1];
};

export default levenshtein;
