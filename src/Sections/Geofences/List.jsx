import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import settings from '../../settings';
import { Sidebar } from '../../Partials/Layout';
import Loading from '../../Partials/Loading';
import PageTitle from '../../components/PageTitle';

class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      geofences: []
    };
  }

  componentDidMount() {
    api.get('/v1/:organization/geofence')
      .then(body => {
        const geofences = api.fillPermissions(body.data);
        const visibleGeofences = geofences.filter(geofence => geofence.permissions.length);

        this.setState({
          loading: false,
          geofences: visibleGeofences
        })
      })
      .catch(err => {
        console.error(err);

        this.setState({
          loading: false,
          error: true
        });
      })
  }

  renderGeofence(geofence) {
    const { t } = this.props;

    let thumbnail = 'https://maps.googleapis.com/maps/api/staticmap?size=128x128&sensor=true&path=weight:3|color:0x206D5AAA|fillcolor:0x37BC9B55|enc:';

    thumbnail += encodeURIComponent(geofence.polyline);
    thumbnail += '&key=' + encodeURIComponent(settings.google_maps.staticmap_api_key);

    let title = geofence.name;
    let actions = [];
    if (geofence.permissions.indexOf('admin') !== -1) {
      title = (
        <Link to={`/geofence/${geofence.id}/update`}>
          {geofence.name}
        </Link>
      );

      actions.push((
        <Link to={`/geofence/${geofence.id}/update`} className="action">
          <i className="material-icons">mode_edit</i>
          &nbsp;
          {t('commons.update')}
        </Link>
      ));

      actions.push((
        <Link to={`/geofence/${geofence.id}/delete`} className="action">
          <i className="material-icons">delete</i>
          &nbsp;
          {t('commons.delete')}
        </Link>
      ));
    }

    return (
      <div className="panel panel-default">
        <div className="panel-body">
          <div className="media">
            <div className="media-left">
              <img width="128" height="128" src={thumbnail} />
            </div>
            <div className="media-body">
              <h4 className="media-heading">{title}</h4>
            </div>
          </div>
        </div>
        <div className="panel-footer">
          <ul className="footer-actions">
            {actions}
          </ul>
        </div>
      </div>
    );
  }

  renderGeofences() {
    const { t } = this.props;

    if (this.state.error) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('errors.generic.title') }</h2>
          { t('errors.generic.description') }
          <br />
          <Link to="/" className="btn btn-lg btn-mint">{ t('errors.generic.button') }</Link>
        </div>
      );
    }

    const { geofences } = this.state;

    if (geofences.length === 0) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('geofences.first') }</h2>
          { t('geofences.description') }
          <br />
          <Link to="/geofences/new" className="btn btn-lg btn-mint">{ t('geofences.create') }</Link>
        </div>
      );
    }

    let createGeofence = null;
    if (api.permissions.allowedActions(api.organization+':geofence').indexOf('admin') !== -1) {
      createGeofence = (
        <div className="actions">
          <Link to="/geofences/new" className="btn btn-lg btn-mint">{ t('geofences.create') }</Link>
        </div>
      );
    }

    return (
      <div className="container">
        <div className="list-header">
          <div className="title">{ t('geofences.title') }</div>
          {createGeofence}
          <div className="clearfix"></div>
          <hr />
        </div>

        <div className="panel-group">
          {geofences.map(this.renderGeofence.bind(this))}
        </div>
      </div>
    )
  }

  render() {
    const { t } = this.props;

    if (this.state.loading) {
      return <Loading />
    }

    return (
      <div>
        <PageTitle title={t("geofences.title")} />
        <Sidebar />

        <div className="has-sidebar">
          {this.renderGeofences()}
        </div>
      </div>
    );
  }
}

export default withTranslation()(List);
