import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import { Sidebar } from '../Partials/Layout';
import PageTitle from '../components/PageTitle';

class Settings extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,

      next_device_identifier: '',
    };
  }

  componentDidMount() {
    api.get('/v1/:organization/device/freecode')
      .then(body => {
        this.setState({
          next_device_identifier: body.data,
        });
      });
  }

  render() {
    const { t } = this.props;

    return (
      <div>
        <PageTitle title={t("commons.settings")} />
        <Sidebar />

        <div className="has-sidebar">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="list-header">
                  <div className="title">{ t('commons.settings') }</div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-md-12">
                <div className="setting-list">
                  <div className="setting">
                    <div className="setting-name">{t('settings.org_code')} <i className="material-icons">help</i></div>
                    <div className="setting-control">
                      <span className="setting-readonly">{api.org.code}</span>
                    </div>
                  </div>

                  <div className="setting">
                    <div className="setting-name">{t('settings.next_device_identifier')} <i className="material-icons">help</i></div>
                    <div className="setting-control">
                      <span className="setting-readonly">{this.state.next_device_identifier}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(Settings);
