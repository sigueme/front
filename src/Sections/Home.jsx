import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next'

import { Header } from '../Partials/Layout';

class Home extends Component {
  render() {
    const { t } = this.props;
    return (
      <div>
        <Header />

        <section className="home-hero jumbotron">
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <p>{ t('home.title') }</p>
                <h1>{ t('home.app_name') }</h1>
                <h3>{ t('home.description') }</h3>
                <br />
                <p><Link className="btn btn-lg btn-block btn-default" to="/signup">{ t('home.signup') }</Link></p>
              </div>
              <div className="col-md-6">
                <div>
                  <div className="home-phone">
                    <div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default withTranslation()(Home);
