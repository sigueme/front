import NewDevice from './NewDevice';

const namespace = new Object({
  NewDevice,
});

export default namespace;
