import React, { Component } from 'react';
import GoogleMapsLoader from 'google-maps';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import settings from '../../settings';
import Loading from '../../Partials/Loading';
import { Sidebar } from '../../Partials/Layout';
import { AjaxButton } from '../../Partials/Helpers';
import PageTitle from '../../components/PageTitle';

class Create extends Component {
  constructor(props) {
    super(props);

    this.map = null;
    this.maps = null;
    this.polygon = null;

    this.state = {
      fetching: false,
      saving: false,
      mode: 'draw-polygon',
      name: '',

      origin: '',
      destination: ''
    };
  }

  componentDidMount() {
    const { maps } = this.props;

    const map = this.map = new maps.Map(this.refs.map, {
      center: { lat: -34.397, lng: 150.644 },
      zoom: 8
    });

    this.resizeMap = this.resizeMap.bind(this);
    maps.event.addDomListener(window, 'resize', this.resizeMap);

    this.originInput = new maps.places.Autocomplete(this.refs.origin, {
      types: ['(cities)'],
    });

    this.destinationInput = new maps.places.Autocomplete(this.refs.destination, {
      types: ['(cities)'],
    });

    this.originInput.addListener('place_changed', this.referenceChange.bind(this));
    this.destinationInput.addListener('place_changed', this.referenceChange.bind(this));
  }

  displayRoute(origin, destination) {
    if (!this._directionsService) {
      const { maps } = this.props;

      this._directionsService = new maps.DirectionsService;
      this._directionsDisplay = new maps.DirectionsRenderer({
        draggable: true,
        map: this.map
      });
    }

    this.setState({
      fetching: true
    });

    this._directionsService.route({
      origin,
      destination,
      travelMode: 'DRIVING',
    }, (response, status) => {
      if (status === 'OK') {
        this._directionsDisplay.setDirections(response);

        this.setState({
          name: origin.split(',')[0] + ' => ' + destination.split(',')[0],
          fetching: false
        });
      } else {
        log.error('Could not connect the dots');

        this.setState({
          fetching: false
        });
      }
    });
  }

  referenceChange() {
    const origin = this.originInput.getPlace();
    const destination = this.destinationInput.getPlace();

    if (origin && destination) {
      this.displayRoute(origin.formatted_address, destination.formatted_address);
    }
  }

  resizeMap() {
    const { map } = this;
    const { maps } = this.props;

    const center = map.getCenter();
    maps.event.trigger(map, 'resize');
    map.setCenter(center);
  }

  onSubmit(event) {
    event.preventDefault();

    const { name } = this.state;

    if (name === '') {
      return;
    }

    const directions = this._directionsDisplay.getDirections();
    const polyline = directions.routes[0].overview_polyline;

    const route = {
      name,
      polyline
    };

    this.setState({
      saving: true
    });

    api.post('/v1/:organization/route', route)
      .then(body => {

        // redirect
        this.props.history.push('/routes');

        this.setState({
          saving: false
        })
      })
      .catch(errors => {
        // @TODO display saving error

        this.setState({
          saving: false
        })
      })
  }

  renderMapWidget() {
    const { t } = this.props;

    if (this.state.name) {
      return (
        <form onSubmit={this.onSubmit.bind(this)}>
          <div className="form-group">
            <label className="control-label">
              <label>{t('routes.name')}</label>
            </label>
            <input
              type="text"
              className="form-control with-border"
              value={this.state.name}
              onChange={event => this.setState({
                name: event.target.value
              })}
            />
          </div>

          <div className="form-group">
            <div className="pull-right">
              <AjaxButton
                loading={this.state.saving}
                className="btn btn-mint"
                content={t('helpers.save')}
                loadingClassName="btn-loading"
                loadingContent={t('helpers.saving')}
              />
            </div>
            <div className="clearfix"></div>
          </div>
        </form>
      );
    }

    return (
      <div>
        <div className="form-group">
          <label>{t('routes.origin')}</label>
          <input
            type="text"
            className="form-control with-border"
            ref="origin"
          />
        </div>

        <div className="form-group">
          <label>{t('routes.destination')}</label>
          <input
            type="text"
            className="form-control with-border"
            ref="destination"
          />
        </div>

        <div>{this.state.fetching ? 'Cargando ruta...' : ''}</div>
      </div>
    );
  }

  render() {
    const { t } = this.props;

    return (
      <div id="map-viewport" className="has-sidebar">
        <PageTitle title={t("routes.create")} />
        <Sidebar />

        <div id="omnibox">
          <div className="widget-box mint">
            <div className="widget-padding">
              <h4 className="widget-title">{ t('routes.create') }</h4>
            </div>
          </div>
          <div className="widget-box">
            <div className="widget-padding">
              {this.renderMapWidget()}
            </div>
          </div>
        </div>

        <div className="full-height" ref="map" />
      </div>
    );
  }
};

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      maps: null
    }
  }

  componentDidMount() {
    GoogleMapsLoader.release();
    GoogleMapsLoader.LIBRARIES = ['drawing', 'places'];
    GoogleMapsLoader.KEY = settings.google_maps.map_api_key;
    GoogleMapsLoader.load(google => {
      this.setState({
        maps: google.maps
      });
    });
  }

  render() {
    if (this.state.maps === null) {
      return <Loading />;
    }

    const { props } = this;
    const { t } = this.props;

    if (this.state.error) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('errors.http404.title') }</h2>
          { t('errors.http404.description') }
          <br />
          <Link to="/" className="btn btn-lg btn-mint">
            { t('errors.http404.button') }
          </Link>
        </div>
      );
    }

    return (
      <Create
        maps={this.state.maps}
        {...props}
      />
    )
  }
};

export default withTranslation()(Container);
