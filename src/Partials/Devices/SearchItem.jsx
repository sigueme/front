import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class SearchItem extends Component {
  render() {
    const { model } = this.props;

    return (
      <Link to={`/device/${model.id}`} className="search-item">
        <span className="material-icons">directions_car</span>
        &nbsp;
        {model.name || model.code}
      </Link>
    );
  }
}

export default SearchItem;
