import List from './List.jsx';

const namespace = new Object({
  List,
});

export default namespace;
