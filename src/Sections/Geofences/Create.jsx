import React, { Component } from 'react';
import GoogleMapsLoader from 'google-maps';
import { withTranslation } from 'react-i18next';

import settings from '../../settings';
import { Sidebar } from '../../Partials/Layout';
import Loading from '../../Partials/Loading';
import { AjaxButton } from '../../Partials/Helpers';
import PageTitle from '../../components/PageTitle';

class Create extends Component {
  constructor(props) {
    super(props);

    this.map = null;
    this.drawingManager = null;
    this.polygon = null;

    this.state = {
      saving: false,
      mode: 'draw-polygon',
      name: '',
    };
  }

  componentDidMount() {
    const { maps } = this.props;

    const map = this.map = new maps.Map(this.refs.map, {
      center: { lat: 24.043, lng: -103.861 },
      zoom: 5,
      options: {
        streetViewControl: true,
        mapTypeControl: true,
        fullscreenControl: false,
        mapTypeControlOptions: {
          style: 2,
          position: 3
        }
      }
    });

    this.resizeMap = this.resizeMap.bind(this);
    maps.event.addDomListener(window, 'resize', this.resizeMap);
    // @TODO: unmount

    this.drawingManager = new maps.drawing.DrawingManager({
      drawingMode: 'polygon',
      drawingControl: false,
      polygonOptions: {
        strokeColor: '#206d5a',
        fillColor: '#37bc9b',
        fillOpacity: 0.5,
        editable: true,
        geodesic: true
      },
      circleOptions: {
        fillColor: '#ffff00',
        fillOpacity: 1,
        strokeWeight: 5,
        clickable: false,
        editable: true,
        zIndex: 1
      },
      map: map
    });

    this.drawingManager.addListener('overlaycomplete', this.onOverlayComplete.bind(this));
  }

  componentWillUpdate(nextProps, nextState) {
    const { maps } = this.props;

    switch (nextState.mode) {
      case 'draw-polygon':
        this.drawingManager.setOptions({
          drawingMode: maps.drawing.OverlayType.POLYGON
        });
        break;
      case 'edit-polygon':
        this.drawingManager.setOptions({
          drawingMode: null
        });
        break;
    }
  }

  componentWillUnmount() {
    const { maps } = this.props;
    maps.event.clearListeners(window, 'resize');
  }

  resizeMap() {
    const { map } = this;
    const { maps } = this.props;

    const center = map.getCenter();
    maps.event.trigger(map, 'resize');
    map.setCenter(center);
  }

  onOverlayComplete(event) {
    this.polygon = event.overlay;

    this.setState({
      mode: 'edit-polygon'
    });
  }

  clearDraw() {
    // Unset polygon
    this.polygon.setMap(null);
    delete this.polygon;

    this.setState({
      mode: 'draw-polygon'
    });
  }

  onSubmit(event) {
    event.preventDefault();

    const { name } = this.state;
    const { geometry: { encoding } } = this.props.maps;

    if (name === '') {
      return;
    }

    let path = this.polygon.getPath();

    let simplePath = [];
    for (let i=0; i<path.length; i++) {
      simplePath.push(path.getAt(i));
    }
    simplePath.push(path.getAt(0));

    const geofence = {
      name: name,
      polyline: encoding.encodePath(simplePath)
    };

    this.setState({
      saving: true
    });

    api.post('/v1/:organization/geofence', geofence)
      .then(() => {

        // redirect
        this.props.history.push('/geofences');

        this.setState({
          saving: false
        });
      })
      .catch(() => {
        // @TODO display saving error

        this.setState({
          saving: false
        });
      });
  }

  renderMapWidget() {
    const { t } = this.props;

    if (this.state.mode === 'draw-polygon') {
      return (
        <div>
          <Trans i18nKey="geofences.tutorial">
            <p>-</p>
            <ul>
              <li>-</li>
              <li>-</li>
            </ul>
          </Trans>
        </div>
      );
    } else if (this.state.mode === 'edit-polygon') {
      return (
        <form onSubmit={this.onSubmit.bind(this)}>
          <div className="form-group">
            <label htmlFor="inputEmail" className="control-label">
              { t('forms.geofence') }
            </label>
            <input
              id="inputEmail"
              type="text"
              className="form-control with-border"
              placeholder={t('forms.geofence_placeholder')}
              value={this.state.name}
              onChange={event => this.setState({
                name: event.target.value
              })}
            />
          </div>

          <div className="form-group">
            <div className="pull-right">
              <a
                href="javascript:void(0);"
                onClick={this.clearDraw.bind(this)}
              >{ t('geofences.delete_draft') }</a>
              &nbsp;
              <AjaxButton
                loading={this.state.saving}
                disabled={this.state.name === ''}
                className="btn btn-mint"
                content={t('geofences.save_draft')}
                loadingClassName="btn-loading"
                loadingContent={t('helpers.saving')}
              />
            </div>
            <div className="clearfix" />
          </div>
        </form>
      );
    } else {
      return null;
    }
  }

  render() {
    const { t } = this.props;

    return (
      <div id="map-viewport" className="has-sidebar">
        <PageTitle title={t("geofences.create")} />
        <Sidebar />

        <div id="omnibox">
          <div className="widget-box mint">
            <div className="widget-padding">
              <h4 className="widget-title">{ t('geofences.create') }</h4>
            </div>
          </div>
          <div className="widget-box">
            <div className="widget-padding">
              {this.renderMapWidget()}
            </div>
          </div>
        </div>

        <div className="full-height" ref="map" />
      </div>
    );
  }
}

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      maps: null
    };
  }

  componentDidMount() {
    GoogleMapsLoader.release();
    GoogleMapsLoader.LIBRARIES = ['geometry', 'visualization', 'drawing'];
    GoogleMapsLoader.KEY = settings.google_maps.map_api_key;
    GoogleMapsLoader.load(google => {
      this.setState({
        loading: false,
        maps: google.maps
      });
    });
  }

  render() {
    const { maps, loading } = this.state;

    if (loading === true) {
      return <Loading />;
    }

    const { props } = this;

    return (
      <Create
        maps={maps}
        {...props}
      />
    );
  }
}

export default withTranslation()(Container);
