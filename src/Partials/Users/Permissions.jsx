import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Select from 'react-select';

import Loading from '../Loading';

class Permissions extends Component {
  constructor(props) {
    super(props);

    const { t } = props;

    this.controlLevels = [{
      value: 'view',
      label: t('access.view')
    }, {
      value: '',
      label: t('access.admin')
    }];

    this.state = {
      loading: true,
      objectOptions: [{
        value: '',
        label: api.organization
      }, {
        value: 'fleet',
        label: t('links.fleets')
      }, {
        value: 'geofence',
        label: t('links.geofences')
      }, {
        value: 'user',
        label: t('links.users')
      }, {
        value: 'route',
        label: t('links.routes')
      }],
      selectedObject: null,
      selectedLevel: null
    };
  }

  componentDidMount() {
    this.listPermissions();
    this.loadFleets();
  }

  loadFleets() {
    api.get('/v1/:organization/fleet')
      .then(body => {
        const fleetOptions = body.data.map(fleet => {
          return {
            value: 'fleet:' + fleet.id,
            label: `Flota: ${fleet.abbr} - ${fleet.name}`
          }
        });

        const mergedOptions = [
          ...this.state.objectOptions,
          ...fleetOptions
        ];

        this.setState({
          objectOptions: mergedOptions
        });
      })
      .catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        errors.map(error => console.error(error));
      });
  }

  createPermission() {
    const { id } = this.props.user;
    const { selectedObject, selectedLevel } = this.state;

    if (selectedObject === null || selectedLevel === null) {
      return;
    }

    const { organization } = api;
    const objectKey = [organization, selectedObject.value].filter(x => x).join(':');
    const levelKey = selectedLevel.value;

    let perm = objectKey;

    if (levelKey !== '') {
      perm += '/' + levelKey;
    }

    // Clean permission selects
    this.setState({
      selectedObject: null,
      selectedLevel: null
    });

    const promise = api.post(`/v1/:organization/user/${id}/perms?embed=1`, { perm })

    this.processPermsPromise(promise);
  }

  updatePermission(objectKey, selectedLevel) {
    const { id } = this.props.user;

    let perm = objectKey;
    if (selectedLevel.value !== '') {
      perm += '/' + selectedLevel.value;
    }

    const promise = api.put(`/v1/:organization/user/${id}/perms?embed=1`, { perm })

    this.processPermsPromise(promise);
  }

  deletePermission(perm) {
    const { id } = this.props.user;
    const promise = api.delete(`/v1/:organization/user/${id}/perms?embed=1`, { perm });

    this.processPermsPromise(promise);
  }

  listPermissions() {
    const { id } = this.props.user;

    this.setState({
      loading: true
    });

    const promise = api.get(`/v1/:organization/user/${id}/perms?embed=1`);
    this.processPermsPromise(promise);
  }

  processPermsPromise(promise) {
    promise.then(body => {
        this.setState({
          loading: false,
          permissions: body.data.perms,
          errors: []
        })
      })
      .catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        errors.map(error => console.error(error));

        this.setState({
          loading: false,
          errors: errors
        });
      });
  }

  renderPermissionName(permission) {
    const { t } = this.props;
    const object = permission.obj;

    if (object._type === 'class') {
      return (
        <td colSpan="2">{t(object.name)}</td>
      );
    }

    if (object._type.match(/:fleet$/)) {
      return (
        <td colSpan="2">
          <Link to={`/fleet/${object.id}`}>
            <span className="label label-primary">{object.abbr}</span>
            &nbsp;
            {object.name}
          </Link>
        </td>
      );
    }

    return (
      <td colSpan="2">{object.id}</td>
    );
  }

  renderPermissions(permissions) {
    if (permissions.length === 0) {
      return [
        <tr key="empty">
          <td colSpan="4">
            <div style={{ paddingBottom: '40px', textAlign: 'center' }}>
              <h3>{ t('permissions.empty_user') }</h3>
              { t('permissions.add_permissions_tutorial') }
            </div>
          </td>
        </tr>
      ];
    }

    return permissions.map(permission => {
      let pieces = permission.perm.split('/');
      let parts = pieces[0].split(':');
      let level = '';

      if (pieces.length == 2) {
        level = pieces[1];
      }

      permission.key = pieces[0];
      permission.level = level;

      return permission;
    }).map(permission => (
      <tr key={permission.key}>
        <td>
          <Select
            value={permission.level}
            options={this.controlLevels}
            onChange={this.updatePermission.bind(this, permission.key)}
            autosize={false}
            clearable={false}
          />
        </td>
        {this.renderPermissionName(permission)}
        <td className="minimal">
          <div
            className="delete-icon"
            onClick={this.deletePermission.bind(this, permission.perm)}
          >
            <i className="material-icons">delete</i>
          </div>
        </td>
      </tr>
    ));
  }

  render() {
    if (this.state.loading) {
      return <Loading />
    }

    const { t } = this.props;
    const { permissions } = this.state;

    let errors = [];
    if (this.state.errors) {
      errors = this.state.errors.map(error => (
        <tr key={error.i18n}>
          <td colSpan="4">
            <div className="alert alert-danger" style={{ marginBottom: 0 }}>
              { t(error.i18n) }
            </div>
          </td>
        </tr>
      ));
    }

    return (
      <table className="items-list">
        <thead>
          <tr className="form-row">
            <td style={{ width: '150px' }}>
              <Select
                placeholder={t('permissions.level_placeholder')}
                options={this.controlLevels}
                value={this.state.selectedLevel}
                onChange={option => this.setState({ selectedLevel: option })}
                autosize={false}
                clearable={false}
              />
            </td>

            <td>
              <Select
                placeholder={t('permissions.object_placeholder')}
                options={this.state.objectOptions}
                value={this.state.selectedObject}
                onChange={option => this.setState({ selectedObject: option })}
                autosize={false}
                clearable={false}
              />
            </td>

            <td className="shrink" colSpan="2">
              <button
                disabled={this.state.selectedLevel === null || this.state.selectedObject === null}
                onClick={this.createPermission.bind(this)}
                className="btn btn-default btn-block"
              >
                { t('permissions.add_permission') }
              </button>
            </td>
          </tr>
        </thead>
        <tbody>
          {errors}
          {this.renderPermissions(permissions)}
        </tbody>
      </table>
    );
  }
};

export default withTranslation()(Permissions);
