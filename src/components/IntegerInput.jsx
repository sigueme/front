import React, { Component } from 'react';

class IntegerInput extends Component {
  constructor(props) {
    super(props);
  }

  handleChange(event) {
    let val = event.target.value;

    if (!val) {
      return this.props.onChange(0);
    }

    val = val.replace(new RegExp(/[^0-9]/g, ''));

    this.props.onChange(parseInt(val));
  }

  render() {
    return (
      <input
        type="text"
        className="form-control"
        placeholder={this.props.placeholder}
        value={this.props.value}
        onChange={this.handleChange.bind(this)}
      />
    );
  }
}

export default IntegerInput;
