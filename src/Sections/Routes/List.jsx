import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import settings from '../../settings';
import { Sidebar } from '../../Partials/Layout';
import Loading from '../../Partials/Loading';
import PageTitle from '../../components/PageTitle';

class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      routes: []
    };
  }

  componentDidMount() {
    api.get('/v1/:organization/route')
      .then(body => {
        const routes = api.fillPermissions(body.data);
        const visibleRoutes = routes.filter(route => route.permissions.length);

        this.setState({
          loading: false,
          routes: visibleRoutes
        })
      })
      .catch(err => {
        console.error(err);

        this.setState({
          loading: false,
          error: true
        });
      })
  }

  renderRoute(route) {
    const { t } = this.props;

    let thumbnail = 'https://maps.googleapis.com/maps/api/staticmap?size=128x128&sensor=true&path=weight:5|color:0x206D5AAA|enc:';

    thumbnail += encodeURIComponent(route.polyline);
    thumbnail += '&key=' + encodeURIComponent(settings.google_maps.staticmap_api_key);

    let title = route.name;
    let actions = [];
    if (route.permissions.indexOf('admin') !== -1) {
      actions.push((
        <Link to={`/route/${route.id}/update`} className="action">
          <i className="material-icons">mode_edit</i>
          &nbsp;
          {t('commons.update')}
        </Link>
      ));

      actions.push((
        <Link to={`/route/${route.id}/delete`} className="action">
          <i className="material-icons">delete</i>
          &nbsp;
          {t('commons.delete')}
        </Link>
      ));

      title = (
        <Link to={`/route/${route.id}/update`}>
          {route.name}
        </Link>
      );
    }

    return (
      <div className="panel panel-default">
        <div className="panel-body">
          <div className="media">
            <div className="media-left">
              <img width="128" height="128" src={thumbnail} />
            </div>
            <div className="media-body">
              <h4 className="media-heading">{title}</h4>
            </div>
          </div>
        </div>
        <div className="panel-footer">
          <ul className="footer-actions">
            {actions}
          </ul>
        </div>
      </div>
    );
  }

  renderRoutes() {
    const { t } = this.props;
    if (this.state.error) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('errors.generic.title') }</h2>
          { t('errors.generic.description') }
          <br />
          <Link to="/" className="btn btn-lg btn-mint">{ t('errors.generic.button') }</Link>
        </div>
      );
    }

    const { routes } = this.state;

    if (routes.length === 0) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('routes.first') }</h2>
          { t('routes.description') }
          <br />
          <Link to="/routes/new" className="btn btn-lg btn-mint">{ t('routes.create') }</Link>
        </div>
      );
    }

    let createRoute = null;
    if (api.permissions.allowedActions(api.organization+':route').indexOf('admin') !== -1) {
      createRoute = (
        <div className="actions">
          <Link to="/routes/new" className="btn btn-lg btn-mint">{ t('routes.create') }</Link>
        </div>
      );
    }

    return (
      <div className="container">
        <div className="list-header">
          <div className="title">{ t('routes.title') }</div>
          {createRoute}
          <div className="clearfix"></div>
          <hr />
        </div>

        <div className="panel-group">
          {routes.map(this.renderRoute.bind(this))}
        </div>
      </div>
    )
  }

  render() {
    const { t } = this.props;

    if (this.state.loading) {
      return <Loading />
    }

    return (
      <div>
        <PageTitle title={t("routes.title")} />
        <Sidebar />

        <div className="has-sidebar">
          {this.renderRoutes()}
        </div>
      </div>
    );
  }
}

export default withTranslation()(List);
