import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import moment from 'moment';

import settings from '../../settings';
import Loading from '../../Partials/Loading';
import Errors from '../Errors';

class Tracking extends Component {
  getEvents() {
    const { trip, t } = this.props;
    const events = [];

    events.push({
      pointClassName: 'point point-mint',
      description: t('trips.started'),
      time: moment(trip.departure),
    });

    if (trip.route_sample) {
      trip.route_sample.forEach(sample => {
        events.push({
          pointClassName: 'point point-darkgray',
          description: sample.address,
          time: moment(sample.time),
        });
      });
    }

    if (trip.status === 'FINISHED') {
      events.push({
        pointClassName: 'point point-mint',
        description: t('trips.finished'),
        time: moment(trip.arrival),
      });
    }

    return events;
  }

  render() {
    const { trip, t } = this.props;

    const scheduled_arrival = moment(trip.scheduled_arrival);
    const events = this.getEvents();

    return (
      <div>
        <br />
        <div className="container container-micro trip-tracking">
          <div className="heading">
            <div className="heading_title">
              {t('trips.tracking_title')}
            </div>

            <p><b>Unidad</b> {trip.device_name}</p>

            <div className="row">
              <div className="col-xs-6 ">
                <div className="attr-label">ETA</div>
                <div className="attr-value">
                  <span>{scheduled_arrival.format('hh:mm')}</span>
                  <small>{scheduled_arrival.format('a')}</small>
                </div>
              </div>
              <div className="col-xs-6">
                <div className="attr-label">{t('forms.destination')}</div>
                <div className="attr-value">
                  <span>{trip.destination}</span>
                </div>
              </div>
            </div>
          </div>

          <div className="body events">
            {events.map((event, ix) => (
              <div className="event" key={ix}>
                <div className="time">
                  {event.time.format('hh:mm')}
                  <small>{event.time.format('a')}</small>
                </div>
                <div className="name">
                  <div className={event.pointClassName} />
                  {event.description}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    this.fetchModel()
  }

  fetchModel() {
    const { hash } = this.props.match.params;

    api.get(`open/track/${hash}`)
      .then(body => {
        this.setState({
          loading: false,
          trip: body.data,
        });
      })
      .catch(error => {
        console.error(error);

        this.setState({
          loading: false,
          error: true
        });
      });
  }

  render() {
    if (this.state.loading) {
      return <Loading />
    }

    const { props } = this;
    const { t } = this.props;

    if (this.state.error) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('errors.http404.title') }</h2>
          { t('errors.http404.description') }
          <br />
          <Link to="/" className="btn btn-lg btn-mint">{ t('errors.http404.button') }</Link>
        </div>
      );
    }

    return (
      <Tracking
        trip={this.state.trip}
        {...props}
      />
    );
  }
}

export default withTranslation()(Container);
