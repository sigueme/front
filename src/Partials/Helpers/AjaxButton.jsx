import React from 'react';

const AjaxButton = (props) => {
  const {
    loading,
    content,
    className,
    loadingClassName,
    loadingContent,
    ...meta
  } = props;

  let button;

  if (props.loading) {
    button = <button
      className={props.className + ' ' + props.loadingClassName}
      disabled="disabled"
      {...meta}
    >{props.loadingContent}</button>
  } else {
    button = <button
      className={props.className}
      {...meta}
    >{props.content}</button>
  }

  return button;
};

export default AjaxButton;
