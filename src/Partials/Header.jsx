import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { translate, Trans } from 'react-i18next';

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      signedInUser: api.user
    };

    this.onSignIn = this.onSignIn.bind(this);
    this.onSignOut = this.onSignOut.bind(this);
  }

  onSignIn(user) {
    this.setState({
      signedInUser: user
    });
  }

  onSignOut() {
    this.setState({
      signedInUser: null
    });
  }

  componentDidMount() {
    api.addEventListener('signin', this.onSignIn);
    api.addEventListener('signout', this.onSignOut);
  }

  componentWillUnmount() {
    api.removeEventListener('signin', this.onSignIn);
    api.removeEventListener('signout', this.onSignOut);
  }

  renderPublicNavbar() {
    const { organization } = api;

    if (organization) {
      return (
        <ul className="nav navbar-nav navbar-right">
          <li><Link to="/signin"><Trans>header.signin</Trans></Link></li>
        </ul>
      );
    } else {
      return (
        <ul className="nav navbar-nav navbar-right">
          <li><Link to="/signin"><Trans>header.signin</Trans></Link></li>
          <li><Link to="/signup"><Trans>header.signup</Trans></Link></li>
        </ul>
      );
    }
  }

  renderPrivateNavbar() {
    let links = [
      {
        key: 'fleet',
        route: '/',
        label: 'links.devices',
      }, {
        key: 'user',
        route: '/users',
        label: 'links.users',
      }, {
        key: 'fleet',
        route: '/fleets',
        label: 'links.fleets',
      }, {
        key: 'geofence',
        route: '/geofences',
        label: 'links.geofences',
      }
    ];

    links = links.filter(link => {
      const perm = api.organization + ':' + link.key;
      const hasChilds = api.permissions.hasChilds(perm);
      const allowedActions = api.permissions.allowedActions(perm);

      return hasChilds || allowedActions.length;
    });

    return (
      <ul className="nav navbar-nav navbar-right">
        {links.map((link, ix) => (
          <li key={ix + '-' + link.key}><Link to={link.route}><Trans>{link.label}</Trans></Link></li>
        ))}
        <li><Link to="/signout"><Trans>links.sign_out</Trans></Link></li>
      </ul>
    );
  }

  render() {
    let navbarLinks;
    if (this.state.signedInUser) {
      navbarLinks = this.renderPrivateNavbar();
    } else {
      navbarLinks = this.renderPublicNavbar();
    }

    const { hostname } = api;

    return (
      <div className='navbar navbar-default'>
        <div className="container">
          <div className="navbar-header">
            <a className="navbar-brand" href={ `//${ hostname }` }>Fleety</a>
            <button className="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
          </div>
          <div className="navbar-collapse collapse" id="navbar-main">
            {navbarLinks}
          </div>
        </div>
      </div>
    );
  }
}

export default translate()(Header);
