import submatch from './submatch';

describe('Submatch', function() {
  it('must match substrings', function() {
    expect(submatch("pedro fernandez", "fndz ped")).toEqual(true);
    expect(submatch("Torton T17", "17 torton")).toEqual(true);
    expect(submatch("Torton T17", "19 torton")).toEqual(false);
    expect(submatch("Camión", "ion")).toEqual(true);
  });
});
