import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import { Sidebar } from '../Partials/Layout';
import { AjaxButton } from '../Partials/Helpers';

class Delete extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
    }
  }

  doDelete() {
    const { id } = this.props.match.params;
    const object = this.props.match.url.split("/").filter(e => e)[0];

    this.setState({
      loading: true
    });

    api.delete(`v1/:organization/${object}/${id}`)
      .then(body => {
        this.setState({
          loading: false
        });

        if (body.response.status === 204) {
          this.props.history.goBack();
        }
      })
      .catch(errors => {
        this.setState({
          loading: false
        });
      });
  }

  renderBody() {
    const { t } = this.props;

    if (this.state.error) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('errors.http404.title') }</h2>
          { t('errors.http404.description') }
          <br />
          <button
            onClick={event => this.props.history.goBack()}
            className="btn btn-lg btn-mint"
          >{ t('helpers.back_button') }</button>
        </div>
      );
    }

    return (
      <div className="container container-small list-empty">
        <h2>{ t('helpers.delete_warning') }</h2>
        { t('helpers.delete_confirm') }
        <br />
        <AjaxButton
          loading={this.state.loading}
          onClick={this.doDelete.bind(this)}
          className="btn btn-lg btn-grapefruit"
          content={t('helpers.delete_button')}
          loadingClassName="btn-loading"
          loadingContent={t('helpers.deleting_button')}
        />
      </div>
    )
  }

  render() {
    return (
      <div>
        <Sidebar />

        <div className="has-sidebar">
          {this.renderBody()}
        </div>
      </div>
    );
  }
};

export default withTranslation()(Delete);
