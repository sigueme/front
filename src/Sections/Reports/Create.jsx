import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import moment from 'moment';

import DateTimeInput from '../../components/DateTimeInput';
import PageTitle from '../../components/PageTitle';
import { AjaxButton } from '../../Partials/Helpers';
import { Sidebar } from '../../Partials/Layout';

class Create extends Component {
  constructor(props) {
    super(props);

    this.state = {
      device: null,
      export_from: moment().startOf('day'),
      export_to: moment().startOf('day'),
      name: '',
    };
  }

  handleSubmit(event) {
    event.preventDefault();

    const {
      name,
      export_from,
      export_to,
      device
    } = this.state;

    // validate form data
    if (export_from >= export_to) {
      alert('daterange error');
    }

    if (device === null) {
      alert('device error');
    }

    const params = {
      builder: 'DeviceGeoJSON',
      name,
      param_export_from: export_from.toISOString().replace(/\.\d{3}/, ''),
      param_export_to: export_to.toISOString().replace(/\.\d{3}/, ''),
      param_device_code: api.org.code + device._model.code
    };

    // create report
    api.post('/v1/:organization/report', params)
      .then(body => {
        if (body.errors !== undefined) {
          const state = body.errors.reduce((errors, error) => {
            errors[error.field + 'Error'] = error;
            return errors;
          }, {});

          state.loading = false;

          return this.setState(state);
        }

        // redirect
        this.props.history.push('/reports');
      }).catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        let state = {
          loading: false,
          errors: []
        };

        errors.forEach(error => {
          if (!error.field) {
            state.errors.push(error);
            return;
          }

          state[error.field + 'Error'] = error;
        });

        this.setState(state);
      });
  }

  isFuture(date) {
    const now = moment(new Date());

    return date > now;
  }

  renderDateRange() {
    const { state } = this;
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = [];

    if (state.nameError || state.last_nameError) {
      className += ' has-error';
    }

    if (state.nameError) {
      helpBlock.push(t(state.nameError.i18n));
    }

    if (state.last_nameError) {
      helpBlock.push(t(state.last_nameError.i18n));
    }

    helpBlock = helpBlock.join(', ');

    return (
      <div className={className}>
        <div className="row">
          <div className="col-xs-6">
            <DateTimeInput
              value={this.state.export_from}
              onChange={date => this.setState({ export_from: date })}
              label={t('commons.startdate')}
              dateIsOutsideRange={this.isFuture.bind(this)}
            />
          </div>
          <div className="col-xs-6">
            <DateTimeInput
              value={this.state.export_to}
              onChange={date => this.setState({ export_to: date })}
              label={t('commons.enddate')}
              dateIsOutsideRange={this.isFuture.bind(this)}
            />
          </div>
        </div>
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  suggest(input, callback) {
    api.search('device', input, callback, (device) => {
      return [device.code, device.name, device.description].filter(x => x).join(' - ');
    });
  }

  renderDevice() {
    const { state } = this;
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (state.deviceError) {
      className += ' has-error';
      helpBlock = t(state.deviceError.i18n);
    }

    return (
      <div className={className}>
        <label htmlFor="inputDevice" className="control-label">
          { t('forms.device') }
        </label>
        <Select.Async
          id="inputDevice"
          placeholder={t('fleets.device_suggestion_placeholder')}
          loadOptions={this.suggest.bind(this)}
          value={this.state.device}
          onChange={device => this.setState({ device })}
          labelKey="name"
          valueKey="id"
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  renderName() {
    const { state } = this;
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (state.nameError) {
      className += ' has-error';
      helpBlock = t(state.nameError.i18n);
    }

    return (
      <div className={className}>
        <label htmlFor="inputName" className="control-label">
          { t('reports.report_name') }
        </label>
        <input
          id="inputName"
          type="text"
          className="form-control"
          placeholder={ t('reports.report_name') }
          value={state.name}
          onChange={event => this.setState({
            name: event.target.value,
            nameError: null
          })}
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  render() {
    const { t } = this.props;

    return (
      <div>
        <PageTitle title={t("reports.create")} />
        <Sidebar />

        <div className="has-sidebar">
          <div className="container">
            <div className="row">
              <div className="col-xs-3">
                <h4>{ t('reports.type') }</h4>
                <div className="list-group">
                  <Link
                    to="/reports/new/device-csv-builder"
                    className="list-group-item active"
                  >{t('reports.types.DeviceGeoJSON')}</Link>
                </div>
              </div>
              <div className="col-xs-9">
                <h3>{ t('reports.types.DeviceGeoJSON') }</h3>
                <hr />

                <form onSubmit={this.handleSubmit.bind(this)}>
                  {this.renderDevice()}
                  {this.renderDateRange()}
                  {this.renderName()}

                  <div className="pull-right">
                    <a href="javascript:void(0);" onClick={() => this.props.history.goBack()}>
                      {t('commons.cancel')}
                    </a>
                    &nbsp;
                    <AjaxButton
                      type="submit"
                      loading={this.state.loading}
                      className="btn btn-lg btn-primary"
                      content={t('commons.save')}
                      loadingClassName="btn-loading"
                      loadingContent={t('helpers.saving')}
                    />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(Create);
