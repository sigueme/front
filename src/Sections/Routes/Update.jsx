import React, { Component } from 'react';
import GoogleMapsLoader from 'google-maps';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import settings from '../../settings';
import Loading from '../../Partials/Loading';
import { Sidebar } from '../../Partials/Layout';
import { AjaxButton } from '../../Partials/Helpers';

class Update extends Component {
  constructor(props) {
    super(props);

    this.map = null;

    const { route } = props;

    this.state = {
      saving: false,
      name: route.name,
    };
  }

  componentDidMount() {
    const { maps } = this.props;

    const map = this.map = new maps.Map(this.refs.map, {
      center: { lat: -34.397, lng: 150.644 },
      zoom: 8
    });

    this.resizeMap = this.resizeMap.bind(this);
    maps.event.addDomListener(window, 'resize', this.resizeMap);

    this.renderPolyline();
  }

  resizeMap() {
    const { map } = this;
    const { maps } = this.props;

    const center = map.getCenter();
    maps.event.trigger(map, 'resize');
    map.setCenter(center);
  }

  renderPolyline() {
    const { maps } = this.props;
    const { route } = this.props;
    const { geometry: { encoding } } = google.maps;
    const path = encoding.decodePath(route.polyline)

    const polyline = new maps.Polyline({
      strokeColor: '#206d5a',
      fillColor: '#37bc9b',
      fillOpacity: 0.5,
      geodesic: true,
      path: path,
      map: this.map
    });

    const origin = new maps.Marker({
      position: path[0],
      label: 'A',
      map: this.map
    });

    const destination = new maps.Marker({
      position: path[path.length - 1],
      label: 'B',
      map: this.map
    });

    const bounds = new maps.LatLngBounds();
    path.forEach(pos => bounds.extend(pos));

    this.map.fitBounds(bounds);
  }

  onSubmit(event) {
    event.preventDefault();

    const { name } = this.state;

    const route = {
      name: name,
    };

    this.setState({
      saving: true
    })

    const { id } = this.props.route;
    api.put(`/v1/:organization/route/${id}`, route)
      .then(body => {

        this.props.history.push('/routes');

        this.setState({
          saving: false
        })
      })
      .catch(errors => {
        // @TODO display saving error

        this.setState({
          saving: false
        })
      })
  }

  renderMapWidget() {
    const { t } = this.props;

    return (
      <form onSubmit={this.onSubmit.bind(this)}>
        <div className="form-group">
          <label htmlFor="inputEmail" className="control-label">
            { t('routes.name') }
          </label>
          <input
            id="inputEmail"
            type="text"
            className="form-control with-border"
            placeholder={ t('routes.name_placeholder') }
            value={this.state.name}
            onChange={event => this.setState({
              name: event.target.value
            })}
          />
        </div>

        <div className="form-group">
          <div className="pull-right">
            <Link
              to="/routes"
            >{ t('helpers.go_back') }</Link>
            &nbsp;
            <AjaxButton
              loading={this.state.saving}
              disabled={this.state.name === ''}
              className="btn btn-mint"
              content={t('helpers.save')}
              loadingClassName="btn-loading"
              loadingContent={t('helpers.saving')}
            />
          </div>
          <div className="clearfix"></div>
        </div>
      </form>
    );
  }

  render() {
    const { t } = this.props;

    return (
      <div id="map-viewport" className="has-sidebar">
        <Sidebar />

        <div id="omnibox">
          <div className="widget-box mint">
            <div className="widget-padding">
              <h4 className="widget-title">{ t('routes.update') }</h4>
            </div>
          </div>
          <div className="widget-box">
            <div className="widget-padding">
              {this.renderMapWidget()}
            </div>
          </div>
        </div>

        <div className="full-height" ref="map" />
      </div>
    );
  }
};

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      route: null,
      maps: null
    }
  }

  componentDidMount() {
    const { id } = this.props.match.params;

    api.get(`/v1/:organization/route/${id}`)
      .then(body => {
        this.setState({
          loading: false,
          route: body.data
        });
      })
      .catch(err => {
        console.error(err)

        this.setState({
          loading: false,
          error: true
        });
      });

    GoogleMapsLoader.release();
    GoogleMapsLoader.LIBRARIES = ['geometry'];
    GoogleMapsLoader.KEY = settings.google_maps.map_api_key;
    GoogleMapsLoader.load(google => {
      this.setState({
        maps: google.maps
      });
    });
  }

  render() {
    if (this.state.route === null || this.state.maps === null) {
      return <Loading />;
    }

    const { props } = this;
    const { t } = this.props;

    if (this.state.error) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('errors.http404.title') }</h2>
          { t('errors.http404.description') }
          <br />
          <Link to="/" className="btn btn-lg btn-mint">{ t('errors.http404.button') }</Link>
        </div>
      );
    }

    return (
      <Update
        maps={this.state.maps}
        route={this.state.route}
        {...props}
      />
    )
  }
};

export default withTranslation()(Container);
