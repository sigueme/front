import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

class ListItem extends Component {

  renderAbbr() {
    const { item } = this.props;

    return (
      <div className="pull-right">
        <span className="label label-success">{item.abbr}</span>
      </div>
    );
  }

  renderName() {
    const { item } = this.props;

    return (
      <h5>{item.name}</h5>
    );
  }

  renderDescription() {
    const { item } = this.props;

    return (<p>{item.description}</p>);
  }

  renderFooter() {
    const { item, t } = this.props;

    let actions = [];

    const perm = `${api.organization}:fleet:${item.id}`;
    const allowedActions = api.permissions.allowedActions(perm);

    if (allowedActions.indexOf('admin') !== -1) {
      actions.push(
        <Link key="fleet-update" to={`/fleet/${item.id}/update`} className="action">
          <i className="material-icons">mode_edit</i>
          &nbsp;
          {t('commons.update')}
        </Link>
      );
      actions.push(
        <Link key="fleet-delete" to={`/fleet/${item.id}/delete`} className="action">
          <i className="material-icons">delete</i>
          &nbsp;
          {t('commons.delete')}
        </Link>
      );
    }

    actions.push(
      <Link key="fleet-read" to={`/fleet/${item.id}`} className="action">
        <i className="material-icons">gps_fixed</i>
        &nbsp;
        {t('commons.view_in_map')}
      </Link>
    );

    return (
      <ul className="footer-actions">{actions}</ul>
    );
  }

  render() {
    const { item } = this.props;

    return (
      <div className="panel panel-default">
        <div className="panel-body">
          <div className="media">
            <div className="media-body">
              {this.renderAbbr()}

              <Link key="fleet-read" to={`/fleet/${item.id}`} className="action">
              {this.renderName()}
              </Link>

            </div>
          </div>
        </div>
        <div className="panel-footer">
          {this.renderFooter()}
        </div>
      </div>
    );
  }
}

export default withTranslation()(ListItem);
