import React from 'react';
import { ToastContainer } from 'react-toastify';

const ToastProvider = (props) => {
  return (
    <div className="full-height">
      <ToastContainer
        className="toasts"
        position="top-right"
        autoClose={30 * 60000}
      />

      {props.children}
    </div>
  );
};

export default ToastProvider;
