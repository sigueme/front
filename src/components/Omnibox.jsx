import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import submatch from '../utils/submatch';
import SearchItem from './SearchItem';

class Omnibox extends Component {
  constructor(props) {
    super(props);

    this.db = new Map();
    this.searched = new Set();
    this.state = {
      search: '',
      focus: false,
      mouseover: false,
    };
  }

  loadOptions(search) {
    api.get('/v1/:organization/search', {
      q: search,
      context: 'device,user,fleet',
    }).then(body => {
      const items = body.data;

      items.forEach(item => this.db.set(item.id, item));
      this.searched.add(search);
      this.forceUpdate();
    });
  }

  onSearchChange(event) {
    event.preventDefault();

    const search = event.target.value;
    this.setState({ search });

    if (!this.searched.has(search)) {
      this.loadOptions(search);
    }
  }

  renderResults() {
    const results = Array.from(this.db.values())
      .filter(model => submatch(model._searchable, this.state.search));

    const { focus, mouseover } = this.state;

    if (results.length === 0 || !(focus || mouseover)) {
      return null;
    }

    return (
      <div
        className="search-items"
        onMouseEnter={() => this.setState({ mouseover: true })}
        onMouseLeave={() => this.setState({ mouseover: false })}
      >
        {results.map(model => (
          <SearchItem key={model.id} model={model} />
        ))}
      </div>
    );
  }

  render() {
    const { t } = this.props;

    return (
      <div id="omnibox">
        <div className="widget-box">
          <div className="widget-padding">
            <form action="" className="search-box">
              <input
                type="text"
                placeholder={t('map.search_box')}
                value={this.state.search}
                onChange={this.onSearchChange.bind(this)}
                onFocus={() => this.setState({ focus: true })}
                onBlur={() => this.setState({ focus: false })}
              />
            </form>
          </div>
        </div>
        {this.renderResults()}
      </div>
    );
  }
}

export default withTranslation()(Omnibox);
