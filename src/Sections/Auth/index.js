import ForgotPassword from './ForgotPassword.jsx';
import SignInWithApikey from './SignInWithApikey.jsx';
import SignIn from './SignIn.jsx';
import SignOut from './SignOut.jsx';
import SignUp from './SignUp.jsx';

const namespace = new Object({
  ForgotPassword,
  SignInWithApikey,
  SignIn,
  SignOut,
  SignUp,
});

export default namespace;
