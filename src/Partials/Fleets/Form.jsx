import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { AjaxButton } from '../Helpers';

class Form extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.eventPropagator.bind(this, 'onSubmit');
    this.onCancel = this.eventPropagator.bind(this, 'onCancel');
  }

  eventPropagator(eventName, ...args) {
    const { props } = this;

    if (typeof props[eventName] === 'function') {
      props[eventName].apply(this, args)
    }
  }

  onFieldChange(field, nextValue) {
    const { props } = this;
    const modelClone = Object.assign({}, props.model);
    modelClone[field] = nextValue;

    if (typeof props.onChange == 'function') {
      props.onChange(modelClone);
    }
  }

  renderTextField(field) {
    // const { state } = this;
    const { t, model, errors } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (errors.get(field) !== undefined) {
      className += ' has-error';
      helpBlock = t(errors.get(field).i18n);
    }

    return (
      <div className={className}>
        <label htmlFor={`${field}-input`}>
          { t('fleets.' + field) }
        </label>
        <input
          id={`${field}-input`}
          type="text"
          className="form-control"
          placeholder={t('fleets.' + field + '_placeholder')}
          value={model[field]}
          onChange={event => this.onFieldChange(field, event.target.value)}
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  render() {
    const { t } = this.props;

    return (
      <form onSubmit={this.onSubmit.bind(this)}>

        {this.renderTextField('name')}
        {this.renderTextField('abbr')}

        <br />
        <div className="pull-right">
          <a href="javascript:void(0);" onClick={event => this.onCancel()}>
            {t('fleets.cancel')}
          </a>
          &nbsp;
          <AjaxButton
            type="submit"
            loading={this.props.saving}
            className="btn btn-lg btn-primary"
            content={t('fleets.save')}
            loadingClassName="btn-loading"
            loadingContent={t('fleets.saving')}
          />
        </div>
      </form>
    );
  }
}

export default withTranslation()(Form);
