import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import Select from 'react-select';
import moment from 'moment';

import { Sidebar } from '../../Partials/Layout';
import { AjaxButton } from '../../Partials/Helpers';
import Loading from '../../Partials/Loading';
import DateTimeInput from '../../components/DateTimeInput';
import IntegerInput from '../../components/IntegerInput';

class Update extends Component {
  constructor(props) {
    super(props);

    const { item } = this.props;
    let scheduled_departure = moment(item.scheduled_departure);
    let scheduled_arrival = moment(item.scheduled_arrival);

    this.state = {
      origin: item.origin,
      destination: item.destination,
      scheduled_departure: scheduled_departure,
      scheduled_arrival: scheduled_arrival,

      device: item.device,
      user: item.user,
      route: item.route,

      eta: scheduled_arrival.diff(scheduled_departure, 'hours'),
      notes: item.notes,

      notifyStop: item.notify_stop,

      loading: false,
      errors: [],
    };
  }

  handleDepartureChange(dt) {
    let scheduled_departure = dt;
    let scheduled_arrival = scheduled_departure.clone();
    let eta = this.state.eta;

    scheduled_arrival.add(eta, 'hours');

    this.setState({
      scheduled_departure,
      scheduled_arrival,
      scheduled_departureErrors: [],
      scheduled_arrivalErrors: [],
    });
  }

  handleArrivalChange(dt) {
    let scheduled_arrival = dt;
    let scheduled_departure = this.state.scheduled_departure;
    let eta = scheduled_arrival.diff(scheduled_departure, 'hours');

    this.setState({
      eta,
      scheduled_arrival,
      scheduled_departureErrors: [],
      scheduled_arrivalErrors: [],
    });
  }

  handleEtaChange(eta) {
    let scheduled_arrival = this.state.scheduled_departure.clone();

    scheduled_arrival.add(eta, 'hours');

    this.setState({
      eta,
      scheduled_arrival,
      scheduled_departureErrors: [],
      scheduled_arrivalErrors: [],
    });
  }


  handleSubmit(event) {
    event.preventDefault();

    if (this.state.loading) {
      return;
    }

    const {
      user,
      route,
      device
    } = this.state;

    const params = {
      origin: this.state.origin || '',
      destination: this.state.destination || '',
      scheduled_departure: this.state.scheduled_departure.toISOString().replace(/\.\d{3}/, ''),
      scheduled_arrival: this.state.scheduled_arrival.toISOString().replace(/\.\d{3}/, ''),

      device_id: device ? device.id : '',
      user_id: user ? user.id : '',
      route_id: route ? route.id : '',

      notes: this.state.notes || '',
      notify_stop: this.state.notifyStop || -1,
    };

    this.setState({
      loading: true,
      errors: []
    });

    const { item } = this.props;

    api.put(`/v1/:organization/trip/${item.id}`, params)
      .then(() => {
        this.props.history.push('/trips');
      }).catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        let state = {
          loading: false,
          errors: []
        };

        errors.forEach(error => {
          if (!error.field) {
            state.errors.push(error);
            return;
          }

          if (state[error.field + 'Errors'] === undefined) {
            state[error.field + 'Errors'] = [];
          }

          state[error.field + 'Errors'].push(error);
        });

        this.setState(state);
      });
  }

  suggestDevice(input, callback) {
    api.search('device', input, callback);
  }

  suggestUser(input, callback) {
    api.search('user', input, callback);
  }

  suggestRoute(input, callback) {
    api.search('route', input, callback);
  }

  renderEndpoints() {
    const { t } = this.props;

    let originClassName = 'form-group';
    let destinationClassName = 'form-group';
    let originHelpBlock = '';
    let destinationHelpBlock = '';

    if (this.state.originErrors !== undefined && this.state.originErrors.length>0) {
      originClassName += ' has-error';
      originHelpBlock = t(this.state.originErrors[0].i18n);
    }

    if (this.state.destinationErrors !== undefined && this.state.destinationErrors.length>0) {
      destinationClassName += ' has-error';
      destinationHelpBlock = t(this.state.destinationErrors[0].i18n);
    }

    return (
      <div className="row">
        <div className="col-xs-6">
          <div className={originClassName}>
            <label htmlFor="inputOrigin" className="control-label">
              { t('forms.origin') }
            </label>
            <input
              id="inputOrigin"
              type="text"
              ref="origin"
              className="form-control"
              placeholder={t('forms.origin_placeholder')}
              value={this.state.origin}
              onChange={event => this.setState({
                origin: event.target.value,
                originErrors: [],
              })}
              autoFocus
            />
            <p className="help-block">{originHelpBlock}</p>
          </div>
        </div>

        <div className="col-xs-6">
          <div className={destinationClassName}>
            <label htmlFor="inputOrigin" className="control-label">
              { t('forms.destination') }
            </label>
            <input
              type="text"
              ref="destination"
              className="form-control"
              placeholder={t('forms.destination_placeholder')}
              value={this.state.destination}
              onChange={event => this.setState({
                destination: event.target.value,
                destinationErrors: [],
              })}
            />
            <p className="help-block">{destinationHelpBlock}</p>
          </div>
        </div>
      </div>
    );
  }

  renderDates() {
    const { t } = this.props;

    return (
      <div className="row">
        <div className="col-xs-4">
          <DateTimeInput
            value={this.state.scheduled_departure}
            onChange={this.handleDepartureChange.bind(this)}
            errors={this.state.scheduled_departureErrors}
            label="forms.departure"
          />
        </div>

        <div className="col-xs-4">
          <label htmlFor="inputDeparture" className="control-label">
            { t('forms.eta') }
          </label>
          <IntegerInput
            value={this.state.eta}
            onChange={this.handleEtaChange.bind(this)}
            placeholder={t('forms.eta_placeholder')}
          />
        </div>

        <div className="col-xs-4">
          <DateTimeInput
            value={this.state.scheduled_arrival}
            onChange={this.handleArrivalChange.bind(this)}
            errors={this.state.scheduled_arrivalErrors}
            label="forms.arrival"
          />
        </div>
      </div>
    );
  }

  renderUser() {
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (this.state.userError) {
      className += ' has-error';
      helpBlock = t(this.state.user_idError.i18n);
    }

    return (
      <div className={className}>
        <label htmlFor="inputUser" className="control-label">
          { t('forms.user') }
        </label>
        <Select.Async
          placeholder={t('forms.user_placeholder')}
          loadOptions={this.suggestUser.bind(this)}
          value={this.state.user}
          onChange={option => this.setState({ user: option })}
          labelKey="name"
          valueKey="id"
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  renderDevice() {
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (this.state.deviceError) {
      className += ' has-error';
      helpBlock = t(this.state.device_idError.i18n);
    }

    return (
      <div className={className}>
        <label htmlFor="inputDevice" className="control-label">
          { t('forms.device') }
        </label>
        <Select.Async
          placeholder={t('forms.device_placeholder')}
          loadOptions={this.suggestDevice.bind(this)}
          value={this.state.device}
          onChange={option => this.setState({ device: option })}
          labelKey="name"
          valueKey="id"
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  renderNotes() {
    const { t } = this.props;

    return (
      <div className="form-group">
        <label htmlFor="inputNotes" className="control-label">
          { t('forms.notes') }
        </label>
        <textarea
          id="inputNotes"
          className="form-control"
          placeholder={t('forms.notes_placeholder')}
          value={this.state.notes}
          onChange={event => this.setState({
            notes: event.target.value,
          })}
        />
      </div>
    );
  }

  renderNotifications() {
    const { t } = this.props;

    return (
      <div className="form-group">
        <label htmlFor="inputNotes" className="control-label">
          { t('forms.notify') }
        </label>

        <div className="form-horizontal">
          <div className="form-group">
            <div className="control-label col-xs-6">
              { t('forms.notify_stop_prefix') }
            </div>
            <div className="col-xs-6">
              <Select
                placeholder="-"
                name="form-time"
                options={[
                  { value: 10, label: t('commons.10m') },
                  { value: 30, label: t('commons.30m') },
                  { value: 60, label: t('commons.60m') },
                  { value: 120, label: t('commons.120m') },
                ]}
                value={this.state.notifyStop}
                onChange={selected => {
                  let notifyStop = null;
                  if (selected) {
                    notifyStop = selected.value;
                  }

                  this.setState({ notifyStop });
                }}
              />
            </div>
          </div>

          <div className="form-group">
            <div className="control-label col-xs-6">
              { t('forms.notify_route_prefix') }
            </div>
            <div className="col-xs-6">
              <Select.Async
                labelKey="name"
                valueKey="id"
                placeholder={t('route.search')}
                loadOptions={this.suggestRoute.bind(this)}
                value={this.state.route}
                onChange={route => this.setState({ route })}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { t } = this.props;

    return (
      <div>
        <Sidebar />

        <div className="has-sidebar">
          <div className="container container-small">
            <h3>{ t('trips.update') }</h3>
            <hr />
            {this.state.errors.map(error => (
              <div key={error.i18n} className="alert alert-danger">
                <i className="material-icons">info</i>
                &nbsp;
                { t(error.i18n) }
              </div>
            ))}
            <form onSubmit={this.handleSubmit.bind(this)}>

              {this.renderEndpoints()}
              {this.renderDates()}
              {this.renderDevice()}
              {this.renderUser()}
              {this.renderNotes()}
              {this.renderNotifications()}

              <div className="pull-right">
                <a href="javascript:void(0);" onClick={() => this.props.history.goBack()}>
                  {t('commons.cancel')}
                </a>
                &nbsp;
                <AjaxButton
                  type="submit"
                  loading={this.state.loading}
                  className="btn btn-lg btn-primary"
                  content={t('commons.save')}
                  loadingClassName="btn-loading"
                  loadingContent={t('helpers.saving')}
                />
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true
    };
  }

  componentDidMount() {
    const { id } = this.props.match.params;

    api.get(`/v1/:organization/trip/${id}`)
      .then(body => {
        this.setState({
          loading: false,
          item: body.data
        });
      })
      .catch(err => {
        console.error(err);

        this.setState({
          loading: false,
          error: true
        });
      });
  }

  render() {
    if (this.state.loading) {
      return <Loading />;
    }

    const { props } = this;
    const { t } = this.props;

    if (this.state.error) {
      return (
        <div>
          <Sidebar />

          <div className="has-sidebar">
            <div className="container container-small list-empty">
              <h2>{ t('errors.http404.title') }</h2>
              { t('errors.http404.description') }
              <br />
              <button
                onClick={() => this.props.history.goBack()}
                className="btn btn-lg btn-mint"
              >{ t('helpers.back_button') }</button>
            </div>
          </div>
        </div>
      );
    }

    return (
      <Update
        item={this.state.item}
        {...props}
      />
    );
  }
}

export default withTranslation()(Container);
