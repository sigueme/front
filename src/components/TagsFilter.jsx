import React, { Component } from 'react';
import Select from 'react-select';
import { withTranslation } from 'react-i18next';

class TagsFilter extends Component {
  handleSelectChange(value) {
    const { onChange } = this.props;

    const selected = value
      .split(',')
      .filter(x => x);

    const state = selected
      .reduce((obj, option) => {
        obj[option] = true;
        return obj;
      }, new Object());

    if (typeof onChange === 'function') {
      onChange(state);
    }
  }

  render() {
    const { t, label, options, value } = this.props;

    const selected=[];
    for (let key in value) {
      if (value[key]) {
        selected.push(key);
      }
    }

    return (
      <div className="row">
        <div className="col-xs-3"><h4>{label}</h4></div>
        <div className="col-xs-9">
          <Select
            multi
            onChange={this.handleSelectChange.bind(this)}
            options={options}
            simpleValue
            value={selected.join(',')}

            placeholder={t('commons.search')}
            clearAllText={t('commons.clear_field')}
            clearValueText={t('commons.clear_option')}
            noResultsText={t('commons.no_results')}
            searchPromptText={t('commons.type_to_search')}
          />
        </div>
      </div>
    );
  }
}

export default withTranslation()(TagsFilter);
