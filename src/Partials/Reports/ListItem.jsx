import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import moment from 'moment';

import StatusBadge from './StatusBadge.jsx';

class ListItem extends Component {
  renderStatus() {
    const { report } = this.props;

    return (
      <div className="pull-right">
        <StatusBadge report={report} />
      </div>
    );
  }

  renderTitle() {
    const { report } = this.props;

    return (
      <h4 className="media-heading">{report.name}</h4>
    )
  }

  renderRequestedAt() {
    const { report, t } = this.props;
    const requested_at = moment(report.requested_at).format('lll');

    return (
      <span>
        <i className="material-icons">query_builder</i>
        &nbsp;
        {t('commons.requested_at')} {requested_at}
      </span>
    );
  }

  renderCompletedAt() {
    const { report, t } = this.props;

    if (!report.completed_at) {
      return null;
    }

    const completed_at = moment(report.completed_at).format('lll');

    return (
      <span>
        <i className="material-icons">query_builder</i>
        &nbsp;
        {t('commons.completed_at')} {completed_at}
      </span>
    );
  }

  render() {
    const { t, report } = this.props;

    console.log(report);

    let actions = [];

    if (report.status === 'COMPLETED') {
      actions.push((
        <a key={`${report.type}:${report.id}:download`} className="action" href={report.url} target="_blank">
          <i className="material-icons">file_download</i>
          &nbsp;
          {t('commons.download')}
        </a>
      ));
    }

    return (
      <div className="panel panel-default">
        <div className="panel-body">
          <div className="media">
            <div className="media-body">
              {this.renderStatus()}
              {this.renderTitle()}
              {this.renderRequestedAt()}
              <br />
              {this.renderCompletedAt()}
            </div>
          </div>
        </div>
        <div className="panel-footer">
          <ul className="footer-actions">
            {actions}
          </ul>
        </div>
      </div>
    );
  }
}

export default withTranslation()(ListItem);
