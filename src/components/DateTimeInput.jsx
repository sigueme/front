import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { SingleDatePicker } from 'react-dates';

import TimeInput from './TimeInput';

class DateTimeInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pickerFocused: false,
    };
  }

  handleDateChage(date) {
    let dt = date.clone();

    dt.hours(this.props.value.hours());
    dt.minutes(this.props.value.minutes());

    this.props.onChange(dt);
  }

  handleTimeChange(time) {
    let dt = this.props.value.clone();

    if (!time) {
      return this.props.onChange(dt);
    }

    let pieces = time.split(':').filter(x => x);

    dt.hours(parseInt(pieces[0]));

    if (pieces.length > 1) {
      dt.minutes(parseInt(pieces[1]));
    }

    this.props.onChange(dt);
  }

  render() {
    const { errors } = this.props;
    let errorItems = [];
    let className = 'form-group';

    if (errors !== undefined && errors.length>0) {
      errorItems = errors;
      className += ' has-error';
    }

    return (
      <div className={className}>
        <label htmlFor="inputDeparture" className="control-label">
          <Trans>{this.props.label}</Trans>
        </label>
        <div className="row">
          <div className="col-xs-6">
            <SingleDatePicker
              date={this.props.value}
              onDateChange={this.handleDateChage.bind(this)}
              focused={this.state.pickerFocused}
              onFocusChange={({ focused }) => this.setState({ pickerFocused: focused })}
              numberOfMonths={1}
              isOutsideRange={this.props.dateIsOutsideRange}
              block={true}
              small={true}
            />
          </div>
          <div className="col-xs-6">
            <TimeInput
              value={this.props.value.format('H:mm')}
              onChange={this.handleTimeChange.bind(this)}
            />
          </div>
        </div>
        <p className="help-block">{errorItems.map(err => (
          <Trans key={err.i18n}>{err.i18n}</Trans>
        ))}</p>
      </div>
    );
  }
}

export default withTranslation()(DateTimeInput);
