import React, { Component } from 'react';
import { translate, Trans } from 'react-i18next';
import { Link } from 'react-router-dom';
import Select from 'react-select';

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: null
    }
  }

  suggest(input, callback) {
    api.search('user', input, callback, (user) => {
      return [user.name, user.last_name, user.email].filter(x => x).join(' - ');
    });
  }

  addUser(event) {
    event.preventDefault();

    if (!this.state.selected) {
      return;
    }

    if (typeof this.props.onUserAdd === 'function') {
      this.props.onUserAdd({
        id: this.state.selected,
      });
    }

    this.setState({
      selected: null
    });
  }

  removeUser(user) {
    if (typeof this.props.onUserRemove === 'function') {
      this.props.onUserRemove(user);
    }
  }

  renderUsers(users) {
    if (users.length === 0) {
      return null;
    }

    return users.map(user => (
      <tr key={user.id}>
        <td colSpan="2">
          <div className="pull-right">
            <a href="javascript:void(0)" onClick={this.removeUser.bind(this, user)}>
              <i className="material-icons">delete</i>
            </a>
          </div>

          <Link to={`/user/${user.id}`}>
            <span className="name">{user.name} {user.last_name}</span>
          </Link>
        </td>
      </tr>
    ));
  }

  renderContent() {
    const { t, users } = this.props;

    return (
      <table className="items-list">
        <tbody>
          {this.renderUsers(users)}
        </tbody>
        <tfoot>
          <tr className="form-row">
            <td>
              <Select.Async
                placeholder={t('users.suggestion_placeholder')}
                loadOptions={this.suggest.bind(this)}
                value={this.state.selected}
                onChange={option => this.setState({ selected: option.id })}
                valueKey="id"
                labelKey="name"
              />
            </td>
            <td className="shrink">
              <button
                onClick={this.addUser.bind(this)}
                className="btn btn-default btn-block"
              >
                <i className="material-icons">add</i>
              </button>
            </td>
          </tr>
        </tfoot>
      </table>
    );
  }

  render() {
    return this.renderContent();
  }
}

export default translate()(Container)
