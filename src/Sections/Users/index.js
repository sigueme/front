import List from './List.jsx';
import Create from './Create.jsx';
import Read from './Read.jsx';
import Update from './Update.jsx';

const namespace = new Object({
  List,
  Create,
  Read,
  Update,
});

export default namespace;
