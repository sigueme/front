import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

class StatusBadge extends Component {
  render() {
    const { report, t } = this.props;

    const statusLabels = {
      COMPLETED: 'label label-mint',
      FAILED: 'label label-grapefruit',
      IN_PROGRESS: 'label label-mediumgray',
    };

    let content = t('statuses.report.' + report.status);

    return (
      <span className={statusLabels[report.status]}>{content}</span>
    );
  }
}

export default withTranslation()(StatusBadge);
