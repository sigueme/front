import i18next from 'i18next';
import { initReactI18next } from "react-i18next";
import moment from 'moment';
import LanguageDetector from 'i18next-browser-languagedetector';

// import languages
import en from './en';
import es from './es';

// note: moment en is loaded by default
import 'moment/locale/es.js';

// Configure and use languages
i18next
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources: {
      en: en,
      es: es
    },
    fallbackLng: 'es',

    react: {
      wait: true
    }
  }, (err, t) => {
    moment.locale(i18next.language);
  });

export default i18next;
