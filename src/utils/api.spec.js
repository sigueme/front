import PermissionTree from './permissionTree';
import API from './api';

global.fetch = require('node-fetch');

import 'babel-polyfill';


describe('API handler', function() {
  it('should exist', function() {
    const handler = new API({ location: 'getfleety.dev' });

    expect(typeof handler.version).toEqual('string');
  });

  it('should decompose location into [org] and [hostname]', function () {
    const handler1 = new API({ location: 'foo.getfleety.dev' });
    const handler2 = new API({ location: 'getfleety.com' });

    expect(handler1.organization).toEqual('foo');
    expect(handler1.hostname).toEqual('getfleety.dev');

    expect(handler2.organization).toEqual(null);
    expect(handler2.hostname).toEqual('getfleety.com');
  });

  it('should point to api.$hostname from $hostname/', function() {
    const handler = new API({ location: 'getfleety.dev' });
    const endpoint = handler._endpoint();

    expect(endpoint).toEqual('http://api.getfleety.dev');
  });

  it('should point to api.$hostname from *.$hostname', function() {
    const handler = new API({ location: 'test.getfleety.dev' });
    const endpoint = handler._endpoint();

    expect(endpoint).toEqual('http://api.getfleety.dev');
  });

  it('shoud allow secure API endpoints', function() {
    const handler = new API({ location: 'foo.getfleety.dev', secure: true });
    const endpoint = handler._endpoint();

    expect(endpoint).toEqual('https://api.getfleety.dev');
  });

  it('should support get, put, post and delete methods', function() {
    const handler = new API({ location: 'getfleety.dev' });

    expect(handler).toHaveProperty('get');
    expect(handler).toHaveProperty('post');
    expect(handler).toHaveProperty('put');
    expect(handler).toHaveProperty('delete');
  });

  it('should sign in', function(done) {
    const handler = new API({ location: 'getfleety.dev' });

    handler.signin({
      email: 'admin@local.com',
      password: '123456',
      org_subdomain: 'local'
    }).then(body => {
        return expect(body.data.session).toHaveProperty('id');
      })
      .then(() => done())
      .catch(err => done(err));
  });

  it('should emit signin and signout events', function(done) {
    const handler = new API({ location: 'getfleety.dev' });

    // Add event listeners
    handler.addEventListener('signin', function(user) {
      expect(handler.user.id).toEqual(user.id);
      expect(handler.user).to.have.property('type').that.equal('user');

      // Sign out
      handler.signout();
    });

    handler.addEventListener('signout', function () {
      expect(handler.user).toEqual(null);

      done();
    });

    expect(handler.user).toEqual(null);

    // Sign in
    handler.signin({
      email: 'admin@local.com',
      password: '123456',
      org_subdomain: 'local'
    }).then(body => expect(body).to.not.have.property('errors'))
      .catch(err => done(err));
  });

  it('should emit change_permissions event', function(done) {
    const handler = new API({ location: 'getfleety.dev', verbose: true });
    handler.verbose = true;

    // Add event listeners
    handler.addEventListener('change_permissions', function(permissions) {
      expect(permissions instanceof PermissionTree).to.be.true;

      done();
    });

    // Sign in
    handler.signin({
      email: 'admin@local.com',
      password: '123456',
      org_subdomain: 'local'
    }).catch(err => done(err));
  });

  it('event listeners should be removable', function(done) {
    const handler = new API({ location: 'getfleety.dev' });

    const fc = function() {
      done(new Error('this function should not have been called'));
    };

    handler.addEventListener('signin', fc);
    handler.addEventListener('signin', () => done());

    handler.removeEventListener('signin', fc);

    // Sign in
    handler.signin({
      email: 'admin@local.com',
      password: '123456',
      org_subdomain: 'local'
    });
  });

  it('should allow easy use of organization when fetching api endpoints', function(done) {
    const handler = new API({ location: 'local.getfleety.dev' });

    handler.signin({
      email: 'admin@local.com',
      password: '123456',
      org_subdomain: 'local'
    }).then(() => handler.get('/v1/:organization/user'))
      .then(body => {
        expect(body.response.url).toEqual('http://api.getfleety.dev/v1/local/user');
      })
      .then(() => done())
      .catch(err => done(err));
  });

  it('should send all api requests with signedInUser\'s api_key', function(done) {
    const handler = new API({ location: 'getfleety.dev' });

    handler.signin({
      email: 'admin@local.com',
      password: '123456',
      org_subdomain: 'local'
    }).then(() => handler.get('/auth/ping?org_subdomain=local'))
      .then(body => expect(body.data.session).toHaveProperty('id'))
      .then(() => done())
      .catch(err => done(err));
  });
});
