import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import GoogleMapsLoader from 'google-maps';
import moment from 'moment';

import settings from '../../settings';
import DeviceMarkerBuilder from '../../utils/DeviceMarker';

class MapMobile extends Component {
  constructor(props) {
    super(props);

    this.map = null;
  }

  componentDidMount() {
    const { maps, device, t } = this.props;
    const DeviceMarker = DeviceMarkerBuilder(maps, t);

    const last_pos = new maps.LatLng(device.last_pos.lat, device.last_pos.lon);

    const map = this.map = new maps.Map(this.refs.map, {
      center: last_pos,
      zoom: 15,
      options: {
        streetViewControl: true,
        mapTypeControl: true,
        fullscreenControl: false,
        mapTypeControlOptions: {
          style: 2,
          position: 3
        }
      }
    });

    this.resizeMap = this.resizeMap.bind(this);
    maps.event.addDomListener(window, 'resize', this.resizeMap);

    this.marker = new DeviceMarker({
      id: device.id,
      name: device.name || device.code,
      status: device.status,
      course: device.last_course,
      last_speed: device.last_speed,
      last_report: moment(new Date(device.last_update)),
      visibility: 'visible',

      map: map,
      position: last_pos,
    });
  }

  componentDidUpdate() {
    const { device, maps } = this.props;

    const last_pos = new maps.LatLng(device.last_pos.lat, device.last_pos.lon);
    this.map.setCenter(last_pos);

    this.marker.setOptions({
      id: device.id,
      name: device.name || device.code,
      status: device.status,
      course: device.last_course,
      last_speed: device.last_speed,
      last_report: moment(new Date(device.last_update)),
      visibility: 'visible',
    });
    this.marker.setPosition(last_pos);
  }

  componentWillUnmount() {
    const { maps } = this.props;
    maps.event.clearListeners(window, 'resize');
  }

  resizeMap() {
    const { map } = this;
    const { maps } = this.props;

    const center = map.getCenter();
    maps.event.trigger(map, 'resize');
    map.setCenter(center);
  }

  render() {
    return (
      <div className="full-height" ref="map" />
    );
  }
}

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      maps: null
    };
  }

  componentDidMount() {
    GoogleMapsLoader.release();
    GoogleMapsLoader.LIBRARIES = [];
    GoogleMapsLoader.KEY = settings.google_maps.map_api_key;
    GoogleMapsLoader.load(google => {
      this.setState({
        maps: google.maps
      });
    });
  }

  render() {
    const { props } = this;

    if (!this.state.maps) {
      return null;
    }

    return (
      <MapMobile
        maps={this.state.maps}
        {...props}
      />
    );
  }
}

export default withTranslation()(Container);
