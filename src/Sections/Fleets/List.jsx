import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

import { Sidebar } from '../../Partials/Layout';
import PageTitle from '../../components/PageTitle';

import ListItem from '../../Partials/Fleets/ListItem';

class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: []
    };
  }

  componentDidMount() {
    api.get('/v1/:organization/fleet')
      .then(body => {
        const items = api.fillPermissions(body.data);
        const visibleItems = items.filter(fleet => fleet.permissions.length);

        this.setState({
          items: visibleItems
        });
      })
  }

  render() {
    const { t } = this.props;
    const { items } = this.state;

    let createLink = null;
    if (api.permissions.allowedActions(api.organization+':fleet').indexOf('admin') !== -1) {
      createLink = (
        <div className="actions">
          <Link to="/fleets/new" className="btn btn-lg btn-mint">{ t('fleets.create') }</Link>
        </div>
      );
    }

    return (
      <div>
        <PageTitle title={t("fleets.title")} />
        <Sidebar />

        <div className="has-sidebar">
          <div className="container">
            <div className="list-header">
              <div className="title">{ t('fleets.title') }</div>
              {createLink}
              <div className="clearfix"></div>
            </div>

            <br />

            <div className="panel-group">
              {this.state.items.map(item => <ListItem
                key={item.id}
                item={item}
              />)}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withTranslation()(List);
