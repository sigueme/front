import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

class NewDevice extends Component {
  constructor(props) {
    super(props);

    this.state = {
      code: '00001',
    };
  }

  componentDidMount() {
    api.get('/v1/:organization/device/freecode')
      .then(body => {
        this.setState({
          code: body.data,
        });
      });

    setTimeout(this.subscribe.bind(this), 1000);
  }

  subscribe() {
    // Create socket
    const organization = api.organization;
    const fleetChannel = `${organization}:fleet:*`;

    this.socket = api.socket([fleetChannel], (eventData) => {
      if (eventData.event == 'device-update') {
        let fleet_id = eventData.channel.split(':')[2]

        // this prevents the app to returning to the guide, if you find a
        // better way to do it please feel free to change the code
        let session = api.user;
        session.empty_account = false;
        api.user = session;

        this.props.history.push('/fleet/' + fleet_id);
      }
    });
  }

  componentWillUnmount() {
    if (this.socket && typeof this.socket.disconnect == 'function') {
      this.socket.disconnect();
    }
  }

  render() {
    const { props } = this;
    const { t } = this.props;

    return (
      <div className="container" id="new-device">
        <div className="row">
          <div className="col-sm-12">
            <div className="guide-item">
              <div className="guide-number">1</div>
              <div className="guide-content">{ t('guides.new_device.step_1') }</div>
            </div>

            <div className="guide-item">
              <div className="guide-number">2</div>
              <div className="guide-content">{ t('guides.new_device.step_2') }</div>
            </div>

            <div className="guide-item">
              <div className="guide-number">3</div>
              <div className="guide-content">
                <div style={{textAlign: 'center' }}>{ t('guides.new_device.step_3') }<code>{api.org.code + this.state.code}</code></div>
              </div>
            </div>

            <div className="guide-item">
              <div className="guide-number">4</div>
              <div className="guide-content">
                <div style={{textAlign: 'center'}}>{ t('guides.new_device.step_4') }<code>gps.getfleety.com</code></div>
              </div>
            </div>

            <div className="guide-item">
              <div className="guide-number">5</div>
              <div className="guide-content">{ t('guides.new_device.step_5') }</div>
            </div>

            <center>
              <button className="btn btn-lg btn-mint btn-next btn-loading">{ t('guides.new_device.waiting_for_positions') }</button>
            </center>
          </div>
        </div>
      </div>
    );
  }
};

export default withTranslation()(NewDevice);
