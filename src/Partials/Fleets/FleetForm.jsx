import React, { Component } from 'react';
import { translate, Trans } from 'react-i18next';

class FleetForm extends Component {
  onFieldChange(field, nextValue) {
    const { props } = this;
    const modelClone = Object.assign({}, props.model);
    modelClone[field] = nextValue;

    if (typeof props.onChange == 'function') {
      props.onChange(modelClone);
    }
  }

  renderTextField(field) {
    const { state } = this;
    const { t, model } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (state[`${field}Error`]) {
      className += 'has-error';
      helpBlock = t(state[`${field}Error`].message);
    }

    return (
      <div className={className}>
        <label>
          field
        </label>
        <input
          id="inputSitename"
          type="text"
          className="form-control"
          placeholder={ t('forms.name_placeholder') }
          value={model[field]}
          onChange={event => this.onFieldChange(field, event.target.value)}
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit.bind(this)}>

        {this.renderTextField('name')}
        {this.renderTextField('abbr')}

        <br />
        <div className="pull-right">
          <a href="javascript:void(0);" onClick={event => this.props.history.goBack()}>
            {t('fleets.cancel')}
          </a>
          &nbsp;
          <button
            type="submit"
            disabled={this.state.loading}
            className="btn btn-lg btn-primary"
          >
            <Trans>fleets.save</Trans>
          </button>
        </div>
      </form>
    );
  }
}

export default FleetForm;
