import normalize from './normalize';

const subsequence = function(str, subseq) {
  if (typeof subseq !== 'string') {
    return false;
  }

  if (typeof str !== 'string') {
    return false;
  }

  const nstr = normalize(str);
  const nsub = normalize(subseq);

  let pnt=0;
  for (let i=0; i<nstr.length; ++i) {
    if (nsub.charAt(pnt) == nstr.charAt(i)) {
      pnt++;

      if (pnt === nsub.length) {
        return true;
      }
    }
  }

  return false;
};

export default subsequence;
