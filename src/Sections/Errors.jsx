import React, { Component } from 'react';
import { Trans } from 'react-i18next';
import { withRouter } from 'react-router-dom';

import { Sidebar } from '../Partials/Layout';

function Errors(props) {
  const errors = props.errors || [];

  if (errors.length === 0) {
    errors.push({ i18n: 'errors.generic.description' })
  }

  // The following code allows error propagation to the console for debugging
  // otherwise hidden by the .catch() method of a promise
  for (let err of errors) {
    if (err instanceof Error) {
      throw err;
    }
  }

  return (
    <div>
      <Sidebar />

      <div className="has-sidebar">
        <div className="container container-small list-empty">
          <h2><Trans>errors.generic.title</Trans></h2>
          {errors.map((error, ix) => (
            <Trans key={`error-${ix}`}>{error.i18n}</Trans>
          ))}
          <br />
          <button
            onClick={event => props.history.goBack()}
            className="btn btn-lg btn-mint"
          >
            <Trans>errors.go_back</Trans>
          </button>
        </div>
      </div>
    </div>
  );
}

export default withRouter(Errors);
