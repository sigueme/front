import React, { Component } from 'react';

import DeviceSearchItem from '../Partials/Devices/SearchItem';
import UserSearchItem from '../Partials/Users/SearchItem';
import FleetSearchItem from '../Partials/Fleets/SearchItem';

const SearchItem = function(props) {
  let model = props.model._type;

  if (model != 'user') {
    model = model.split(':')[1];
  }

  switch (model) {
    case 'device': return <DeviceSearchItem {...props} />;
    case 'user':   return <UserSearchItem {...props} />;
    case 'fleet':   return <FleetSearchItem {...props} />;
    default:
      return null;
      break;
  }
};

export default SearchItem;
