import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { Sidebar } from '../../Partials/Layout';
import Loading from '../../Partials/Loading';
import PageTitle from '../../components/PageTitle';
import ListItem from '../../Partials/Reports/ListItem';

class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      reports: []
    };
  }

  componentDidMount() {
    api.get('/v1/:organization/report')
      .then(body => {
        const reports = api.fillPermissions(body.data);
        const visibleReports = reports.filter(report => report.permissions.length);

        this.setState({
          loading: false,
          reports: visibleReports
        })
      })
      .catch(err => {
        console.error(err);

        this.setState({
          loading: false,
          error: true
        });
      })
  }

  renderReport(report) {
    const { t } = this.props;

    return (
      <ListItem t={t} report={report} />
    );

    let title = report.name;
    let actions = [];
    if (report.permissions.indexOf('admin') !== -1) {
      title = (
        <Link to={`/report/${report.id}/update`}>
          <div className="pull-right"></div>
          {report.name}
          {report.requested_at}
          {report.completed_at}
          {report.status}
        </Link>
      );

      actions.push((
        <Link to={`/report/${report.id}/update`} className="action">
          <i className="material-icons">mode_edit</i>
          &nbsp;
          {t('commons.update')}
        </Link>
      ));

      actions.push((
        <Link to={`/report/${report.id}/delete`} className="action">
          <i className="material-icons">delete</i>
          &nbsp;
          {t('commons.delete')}
        </Link>
      ));
    }

    return (
      <div className="panel panel-default">
        <div className="panel-body">
          <div className="media">
            <div className="media-body">
              <h4 className="media-heading">{title}</h4>
            </div>
          </div>
        </div>
        <div className="panel-footer">
          <ul className="footer-actions">
            {actions}
          </ul>
        </div>
      </div>
    );
  }

  renderReports() {
    const { t } = this.props;

    if (this.state.error) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('errors.generic.title') }</h2>
          { t('errors.generic.description') }
          <br />
          <Link to="/" className="btn btn-lg btn-mint">{ t('errors.generic.button') }</Link>
        </div>
      );
    }

    const { reports } = this.state;

    if (reports.length === 0) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('reports.first') }</h2>
          { t('reports.description') }
          <br />
          <Link to="/reports/new" className="btn btn-lg btn-mint">{ t('reports.create') }</Link>
        </div>
      );
    }

    let createReport = null;
    if (api.permissions.allowedActions(api.organization+':report').indexOf('admin') !== -1) {
      createReport = (
        <div className="actions">
          <Link to="/reports/new" className="btn btn-lg btn-mint">{ t('reports.create') }</Link>
        </div>
      );
    }

    return (
      <div className="container">
        <div className="list-header">
          <div className="title">{ t('reports.title') }</div>
          {createReport}
          <div className="clearfix"></div>
          <hr />
        </div>

        <div className="panel-group">
          {reports.map(report => (
            <ListItem
              key={report.type + ':' + report.id}
              report={report}
            />
          ))}
        </div>
      </div>
    )
  }

  render() {
    const { t } = this.props;

    if (this.state.loading) {
      return <Loading />
    }

    return (
      <div>
        <PageTitle title={t("reports.title")} />
        <Sidebar />

        <div className="has-sidebar">
          {this.renderReports()}
        </div>
      </div>
    );
  }
}

export default withTranslation()(List);
