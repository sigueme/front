import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import moment from 'moment';

import DeviceStatusBadge from '../../components/DeviceStatusBadge';

class ListItem extends Component {

  renderStatus() {
    const { item } = this.props;

    return (
      <div className="pull-right">
        <DeviceStatusBadge device={item} />
      </div>
    );
  }

  renderName() {
    const { item } = this.props;

    return (
      <h5>{item.name} <small>({api.org.code}{item.code})</small></h5>
    );
  }

  renderDescription() {
    const { item } = this.props;

    return (<p>{item.description}</p>);
  }

  renderFooter() {
    const { item, t } = this.props;

    let actions = [];

    const perm = `${api.organization}:device:${item.id}`;
    const allowedActions = api.permissions.allowedActions(perm);

    if (allowedActions.indexOf('admin') !== -1) {
      actions.push(
        <Link key="device-update" to={`/device/${item.id}/update`} className="action">
          <i className="material-icons">mode_edit</i>
          &nbsp;
          {t('commons.update')}
        </Link>
      );
      actions.push(
        <Link key="device-delete" to={`/device/${item.id}/delete`} className="action">
          <i className="material-icons">delete</i>
          &nbsp;
          {t('commons.delete')}
        </Link>
      );
    }

    actions.push(
      <Link key="device-read" to={`/device/${item.id}`} className="action">
        <i className="material-icons">gps_fixed</i>
        &nbsp;
        {t('commons.view_in_map')}
      </Link>
    );

    return (
      <ul className="footer-actions">{actions}</ul>
    );
  }

  renderCreatedAt() {
    const { item, t } = this.props;
    const created_at = moment(item.created_at).format('lll');

    return (
      <span>
        <i className="material-icons">query_builder</i>
        {t('commons.created_at')} {created_at}
      </span>
    );
  }

  renderFleet() {
    const { item } = this.props;

    return (
      <span style={{marginRight: '10px'}}>
        <i className="material-icons">group_work</i>
        {item.fleet.name}
      </span>
    );
  }

  render() {
    return (
      <div className="panel panel-default">
        <div className="panel-body">
          <div className="media">
            <div className="media-body">
              {this.renderStatus()}
              {this.renderName()}
              {this.renderDescription()}
              {this.renderFleet()}
              {this.renderCreatedAt()}
            </div>
          </div>
        </div>
        <div className="panel-footer">
          {this.renderFooter()}
        </div>
      </div>
    );
  }
}

export default withTranslation()(ListItem);
