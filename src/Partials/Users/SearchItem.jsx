import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class SearchItem extends Component {
  render() {
    const { model } = this.props;

    return (
      <Link to={`/user/${model.id}`} className="search-item">
        <span className="material-icons">person</span>
        &nbsp;
        {model.name} {model.last_name}
      </Link>
    );
  }
}

export default SearchItem;
