import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import moment from 'moment';

import { Sidebar } from '../../Partials/Layout';
import Loading from '../../Partials/Loading';
import Errors from '../Errors';
import Render from '../../Partials/Logs/Render';
import PageTitle from '../../components/PageTitle';

import ListItem from '../../Partials/Logs/ListItem';

class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      items: []
    };
  }

  componentDidMount() {
    this.subscribe();
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  fetchModel() {
    api.get('/v1/:organization/log')
      .then(body => {
        const items = body.data.map(log => {
          log.permissions = api.permissions.allowedActions(log.channel);

          return log;
        });

        this.setState({
          loading: false,
          items: items.filter(log => log.permissions.length)
        });
      })
      .catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        errors.map(error => console.error(error));

        this.setState({
          loading: false,
          errors: errors
        });
      });
  }

  subscribe() {
    this.fetchModel();

    this.subscribeInterval = setInterval(() => {
      this.fetchModel();
    }, 300000);

    this.socket = api.socket([`${api.organization}:*:*`], (event) => {
      if (event.event == 'device-update') {
        return;
      }

      let items = this.state.items;

      event.created_at = moment();

      items.unshift(event);

      if (items.length > 50) {
        items = items.slice(0, 50);
      }

      this.setState({
        items: items,
      });
    });
  }

  unsubscribe() {
    clearInterval(this.subscribeInterval);

    if (this.socket && typeof this.socket.disconnect == 'function') {
      this.socket.disconnect();
    }
  }

  render() {
    if (this.state.loading) {
      return <Loading />
    }

    if (this.state.errors) {
      return <Errors errors={this.state.errors} />
    }

    const { t } = this.props;

    return (
      <div>
        <PageTitle title={t("logs.title")} />
        <Sidebar />

        <div className="has-sidebar">
          <div className="container">
            <div className="list-header">
              <div className="title">{ t('logs.title') }</div>
              <div className="actions">
                <Link to="/me/subscriptions" className="btn btn-lg btn-mint">{ t('me.subscriptions') }</Link>
              </div>
              <div className="clearfix"></div>
            </div>

            <hr />

            <div className="panel-group">
              {this.state.items.map(item => <ListItem
                key={item.id}
                item={item}
              />)}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withTranslation()(List);
