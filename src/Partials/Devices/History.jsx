import React, { Component } from 'react';
import moment from 'moment';
import { withTranslation } from 'react-i18next';

import haversine from '../../utils/haversine';
import msToHumanReadable from '../../utils/msToHumanReadable';
import { SingleDatePicker } from 'react-dates';

class History extends Component {
  constructor(props) {
    super(props);

    const now = new Date();
    const today = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate()
    );

    this.markers = [];

    this.state = {
      loading: true,
      date: moment(today),
      focused: false,
      duration: 86400,
      history: [],
      selectedEvent: undefined
    };
  }

  componentDidMount() {
    this.loadHistory();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.date.diff(this.state.date) !== 0) {
      this.loadHistory();
    }
  }

  componentWillUnmount() {
    this.polyline.setMap(null);
    for (let marker of this.markers.values()) {
      marker.setMap(null);
    }

    this.keepAnimation = false;
  }

  setRange(index) {
    if (this.state.selectedEvent === index) {
      index = undefined;
    }

    this.displayPolyline(
      this.state.clusters,
      this.state.events,
      index
    );

    this.setState({
      selectedEvent: index
    });
  }

  get polyline() {
    if (!this._polyline) {
      // Generate polyline icons
      const polylineIcons = [{
        icon: {
          path: 'M 0,0 0,2',
          strokeOpacity: 0.75,
          scale: 5
        },
        offset: '0',
        repeat: '20px'
      }];

      // Generate poyline
      this._polyline = new google.maps.Polyline({
        strokeColor: '#37bc9b',
        strokeOpacity: 0,
        icons: polylineIcons,
      });

      // Animate polyline
      this.keepAnimation = true;
      let stepAnimation = () => {
        let now = new Date();
        polylineIcons[0].offset = (now.getMilliseconds() / 1000) * 20 + 'px';
        this._polyline.set('icons', polylineIcons);

        if (this.keepAnimation) {
          window.requestAnimationFrame(stepAnimation);
        }
      };
      stepAnimation();

      // Set map to polyline
      this._polyline.setMap(this.props.map);
    }

    return this._polyline;
  }

  displayPolyline(clusters, events, selectedEvent, adjustMap = false) {
    const { maps, map } = this.props;

    if (!maps) {
      return;
    }

    if (clusters.length === 0) {
      if (this._polyline) {
        this._polyline.setPath([]);
      }

      return;
    }

    let path;
    if (selectedEvent !== undefined) {
      const event = events[selectedEvent];

      if (event.type === 'driving') {
        for (let marker of this.markers.values()) {
          marker.setMap(null);
        }

        path = clusters
          .filter((c, index) => event.head <= index && index <= event.tail)
          .map(pos => {
            const marker = this.markers.get(pos.id);
            if (marker) {
              marker.setMap(map);
            }

            return {
              lat: pos.lat,
              lng: pos.lon
            }
          });
      } else if (event.type === 'stop') {
        const cluster = clusters[event.ref];

        path = [{
          lat: cluster.lat,
          lng: cluster.lon,
        }];

        for (let [clusterId, marker] of this.markers) {
          if (clusterId === cluster.id) {
            marker.setMap(map);
          } else {
            marker.setMap(null);
          }
        }
      }
    } else {
      path = clusters
        .map(pos => ({ lat: pos.lat, lng: pos.lon }));

      for (let marker of this.markers.values()) {
        marker.setMap(map);
      }
    }

    this.polyline.setPath(path);

    if (adjustMap) {
      let bounds = new maps.LatLngBounds();
      path.forEach(pos => bounds.extend(pos));
      map.fitBounds(bounds);
    }
  }

  loadHistory() {
    const { map, maps } = this.props;
    const { id } = this.props.device;
    const { date, duration } = this.state;

    this.setState({
      loading: true
    });

    if (this.markers instanceof Map) {
      for (let marker of this.markers.values()) {
        marker.setMap(null);
      }
    }

    const UTCUnix = date.unix();

    api.get(`/v1/:organization/device/${id}/location_history`, {
      devicetime: [UTCUnix, duration].join('+'),
    })
    .then(body => {
      const points = body.data.map(point => {
        point.device_time = new Date(point.device_time);

        return point;
      });

      let clusters, events;
      if (points.length) {
        clusters = this.clusterize(points);
        events = this.eventize(clusters);
      } else {
        clusters = [];
        events = [];
      }

      this.markers = events
        .filter(event => event.type === 'stop')
        .map(event => clusters[event.ref])
        .filter(cluster => cluster.id)
        .reduce((map_ds, cluster) => {
          const { lat, lon } = cluster;

          const marker = new maps.Marker({
            position: new maps.LatLng(lat, lon),
            map
          });

          return map_ds.set(cluster.id, marker);
        }, new Map());

      this.displayPolyline(clusters, events, this.state.selectedEvent);

      this.setState({
        loading: false,
        points,
        clusters,
        events,
        selectedEvent: undefined
      });
    })
    .catch(errors => {
      if (!(errors instanceof Array)) {
        errors = [errors];
      }

      // @TODO notify user
    });
  }

  clusterize(points) {
    let clusters = [{
      head: 0,
      tail: 0
    }];

    let last_cluster = clusters[0];
    for (let i=1, l=points.length; i<l; i++) {
      const ref = points[last_cluster.head];
      const point = points[i];

      const distance = haversine(
        ref.lon,
        ref.lat,
        point.lon,
        point.lat
      );

      if (distance < 150) {
        last_cluster.tail = i;
      } else {
        if (last_cluster.head !== last_cluster.tail) {
          let head_time = points[last_cluster.head].device_time;
          let tail_time = points[last_cluster.tail].device_time;

          last_cluster.nodes = last_cluster.tail - last_cluster.head + 1;
          last_cluster.idletime = tail_time - head_time;
        } else {
          last_cluster.nodes = 1;
          last_cluster.idletime = 0;
        }

        last_cluster = {
          head: i,
          tail: i
        };

        clusters.push(last_cluster);
      }
    }

    if (last_cluster.head !== last_cluster.tail) {
      let head_time = points[last_cluster.head].device_time;
      let tail_time = points[last_cluster.tail].device_time;

      last_cluster.nodes = last_cluster.tail - last_cluster.head + 1;
      last_cluster.idletime = tail_time - head_time;
    } else {
      last_cluster.nodes = 1;
      last_cluster.idletime = 0;
    }

    return clusters.map(cluster => {
      let head = Object.assign({}, points[cluster.head]);

      head.idletime = cluster.idletime;
      head.nodes = cluster.nodes;

      return head;
    });
  }

  eventize(clusters) {
    let events = [{
      type: 'stop',
      ref: 0,
      start: clusters[0].device_time,
      duration: clusters[0].idletime
    }];

    for (let i=1, l=clusters.length; i<l; i++) {
      const cluster = clusters[i];

      if (cluster.idletime > 300000 || i === l-1) {
        const prevEvent = events[events.length - 1];
        const prevCluster = clusters[prevEvent.ref];
        const prevFinish = new Date(prevCluster.device_time.getTime() + prevCluster.idletime);

        events.push({
          type: 'driving',
          head: events[events.length - 1].ref,
          tail: i,
          start: prevFinish,
          duration: cluster.device_time - prevFinish
        });

        events.push({
          type: 'stop',
          ref: i,
          start: cluster.device_time,
          duration: cluster.idletime || 0,
        });
      }
    }

    return events;
  }

  renderEvents() {
    const { t } = this.props;

    if (this.state.loading) {
      return (
        <tbody>
          <tr key="empty">
            <td colSpan="4">
              <div style={{ paddingBottom: '40px', textAlign: 'center' }}>
                <h4>{ t('helpers.loading') }</h4>
              </div>
            </td>
          </tr>
        </tbody>
      );
    }

    if (this.state.points.length === 0) {
      return (
        <tbody>
          <tr key="empty">
            <td colSpan="4">
              <div style={{ paddingBottom: '40px', textAlign: 'center' }}>
                <h4>{ t('history.empty_list') }</h4>
              </div>
            </td>
          </tr>
        </tbody>
      );
    }

    const { events, selectedEvent } = this.state;

    const clusterList = events.map((event, index) => {
      let className = '';
      if (index === selectedEvent) {
        className = 'active';
      }

      return {
        id: `cluster-${index}`,
        index: index,
        className,
        time: event.start,
        duration: event.duration,
        description: this.props.t('devices.event_' + event.type)
      };
    });

    return (
      <tbody>
        {clusterList.map(row => (
          <tr
            key={row.id}
            className={row.className}
            onClick={() => this.setRange(row.index)}
          >
            <td>{moment(row.time).format('YYYY-MM-DD HH:mm')}</td>
            <td title={msToHumanReadable(row.duration)}>{moment.duration(row.duration).humanize()}</td>
            <td title={row.description}>{row.description}</td>
          </tr>
        ))}
      </tbody>
    );
  }

  render() {
    const { t } = this.props;

    return (
      <div>
        <div style={{ padding: '12px' }}>
          <div className="pull-right">
            <SingleDatePicker
              style={{ float: 'right' }}
              date={this.state.date}
              onDateChange={date => {
                this.setState({
                  date: date.hours(0)
                });
              }}
              focused={this.state.focused}
              onFocusChange={({ focused }) => this.setState({ focused })}
              numberOfMonths={1}
              isOutsideRange={day => {
                return day.diff(new Date()) > 0;
              }}
            />
          </div>

          <div style={{ lineHeight: '48px', fontSize: '18px' }}>
            { t('forms.date') }
          </div>
          <div className="clearfix" />
        </div>

        <div className="location-events">
          <table className="table table-striped">
            {this.renderEvents()}
          </table>
        </div>
      </div>
    );
  }
}

export default withTranslation()(History);
