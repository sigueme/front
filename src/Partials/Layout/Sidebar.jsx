import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

class Sidebar extends Component {
  constructor(props) {
    super(props);

    let state = {
      alarmCount: 0
    };

    if (props.open !== undefined) {
      state.slave = true;
    } else {
      state.slave = false;
      state.open = false;
    }

    this.state = state;
  }

  setOpenState(status) {
    const { props, state } = this;

    if (typeof props.onChange === 'function') {
      props.onChange(status);
    }

    if (state.slave == false) {
      this.setState({
        open: status
      });
    }
  }

  render() {
    const { t } = this.props;
    let opened;

    if (this.state.slave) {
      opened = this.props.open;
    } else {
      opened = this.state.open;
    }

    let linkgroups = [
      [{
        key: 'fleet',
        route: '/map',
        label: 'links.map',
        icon: 'map',
        exact: true
      }],
      [{
        key: 'user',
        route: '/users',
        label: 'links.users',
        icon: 'people',
      }, {
        key: 'device',
        route: '/devices',
        label: 'links.devices',
        icon: 'directions_car',
      }, {
        key: 'fleet',
        route: '/fleets',
        label: 'links.fleets',
        icon: 'group_work',
      }, {
        key: 'trip',
        route: '/trips',
        label: 'links.trips',
        icon: 'flight_takeoff',
      }, {
        key: 'geofence',
        route: '/geofences',
        label: 'links.geofences',
        icon: 'blur_circular',
      }, {
        key: 'route',
        route: '/routes',
        label: 'links.routes',
        icon: 'timeline',
      }, {
        key: 'report',
        route: '/reports',
        label: 'links.reports',
        icon: 'pie_chart',
      }, {
        key: api.organization,
        route: '/logs',
        label: 'links.logs',
        icon: 'notifications',
        iconCount: this.state.alarmCount,
      }, {
        key: 'settings',
        route: '/settings',
        label: 'links.settings',
        icon: 'settings',
      }],
    ];

    linkgroups = linkgroups.map(linkgroup =>
      linkgroup.filter(link => {
        const perm = api.organization + ':' + link.key;
        const node = api.permissions.getNode(perm);
        const allowedActions = api.permissions.allowedActions(perm);

        return node || allowedActions.length;
      })
    );

    linkgroups = linkgroups.filter(linkgroup => linkgroup.length);

    return (
      <div className={ 'modal-sidebar' + (opened ? ' active' : '') }>
        <div className="sidebar">
          <div className="sidebar-menu sidebar-header">
            <a href="javascript:void(0);" onClick={event => this.setOpenState(!opened)}>
              <div className="menuitem-icon">
                <i className="material-icons">menu</i>
              </div>

              Fleety / {api.organization}
            </a>
          </div>
          {linkgroups.map((links, ix) => (
            <div key={`linkgroup-` + ix} className="sidebar-menu">
              {links.map(link => (
                <NavLink key={link.key} exact={link.exact || false} to={link.route}>
                  <div className="menuitem-icon">
                    {!link.iconCount ? null : (
                      <span className="icon-count">{link.iconCount}</span>
                    )}
                    <i className="material-icons">{link.icon}</i>
                  </div>

                  { t(link.label) }
                </NavLink>
              ))}
            </div>
          ))}
          <div className="sidebar-menu">
            <NavLink to="/signout">
              <div className="menuitem-icon">
                <i className="material-icons">power_settings_new</i>
              </div>

              { t('links.sign_out') }
            </NavLink>
          </div>
        </div>
        <div
          className="background"
          onClick={event => this.setOpenState(false)}
        />
      </div>
    );
  }
}

export default withTranslation()(Sidebar);
