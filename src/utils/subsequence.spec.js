import subsequence from './subsequence';

describe('Levenshtein', function() {
  it('must find distance', function() {
    expect(subsequence("holas", "ola")).toEqual(true);
    expect(subsequence("T0012", "T12")).toEqual(true);
    expect(subsequence("T0012", "T13")).toEqual(false);
  });

  it('must be case insensitive', function() {
    expect(subsequence("HOLA", "ola")).toEqual(true);
    expect(subsequence("T045wxl", "TWL")).toEqual(true);
    expect(subsequence("T045wxl", "TWLL")).toEqual(false);
  });

  it('must be diacritics insensitive', function() {
    expect(subsequence("Josué", "jose")).toEqual(true);
    expect(subsequence("Árbol", "abó")).toEqual(true);
    expect(subsequence("Árbol", "aaron")).toEqual(false);
  });
});
