import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import { Header } from '../../Partials/Layout';
import { AjaxButton } from '../../Partials/Helpers';

class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      subdomain: '',
      name: '',
      lastName: '',
      email: '',
      password: '',
      showPassword: false,
      loading: false,
      errors: []
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    if (this.state.loading) {
      return;
    }

    const params = {
      organization: this.state.subdomain,
      subdomain: this.state.subdomain,
      name: this.state.name,
      last_name: this.state.lastName,
      email: this.state.email,
      password: this.state.password
    };

    this.setState({
      loading: true,
      errors: []
    });

    api.signup(params)
      .then(body => {
        const { session } = body.data;
        window.location = `http://${params.subdomain}.${api.hostname}#/signin/${session.api_key}`
      }).catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        let state = {
          loading: false,
          errors: []
        };

        errors.forEach(error => {
          if (!error.field) {
            state.errors.push(error);
            return;
          }

          state[error.field + 'Error'] = error;
        });

        this.setState(state);
      });
  }

  prepareSubdomain(pretended) {
    let r = pretended.toLowerCase().replace(' ', '');

    r = r.replace(new RegExp(/\s/g),"");
    r = r.replace(new RegExp(/[àáâãäå]/g),"a");
    r = r.replace(new RegExp(/æ/g),"ae");
    r = r.replace(new RegExp(/ç/g),"c");
    r = r.replace(new RegExp(/[èéêë]/g),"e");
    r = r.replace(new RegExp(/[ìíîï]/g),"i");
    r = r.replace(new RegExp(/ñ/g),"n");
    r = r.replace(new RegExp(/[òóôõö]/g),"o");
    r = r.replace(new RegExp(/œ/g),"oe");
    r = r.replace(new RegExp(/[ùúûü]/g),"u");
    r = r.replace(new RegExp(/[ýÿ]/g),"y");
    r = r.replace(new RegExp(/\W/g),"");

    return r;
  }

  renderSubdomain() {
    const { state } = this;
    const { t } = this.props;
    const { hostname } = window.location;

    let className = 'form-group';
    let helpBlock = '';

    if (state.subdomainError) {
      className += ' has-error';
      helpBlock = t(state.subdomainError.i18n);
    }

    return (
      <div className={className}>
        <label htmlFor="inputSubdomain" className="control-label">
          { t('sign_up.claim_subdomain') }
        </label>
        <div className="input-group">
          <input
            id="inputSubdomain"
            type="text"
            className="form-control"
            placeholder={ t('sign_up.claim_subdomain_placeholder') }
            value={state.subdomain}
            onChange={event => this.setState({
              subdomain: this.prepareSubdomain(event.target.value),
              subdomainError: null
            })}
          />
          <span className="input-group-addon">.{ hostname }</span>
        </div>
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  renderName() {
    const { state } = this;
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = [];

    if (state.nameError || state.last_nameError) {
      className += ' has-error';
    }

    if (state.nameError) {
      helpBlock.push(t(state.nameError.i18n));
    }

    if (state.last_nameError) {
      helpBlock.push(t(state.last_nameError.i18n));
    }

    helpBlock = helpBlock.join(', ');

    return (
      <div className={className}>
        <label htmlFor="inputName" className="control-label">
          { t('forms.fullname') }
        </label>
        <div className="row">
          <div className="col-xs-6">
            <input
              id="inputName"
              type="text"
              className="form-control"
              placeholder={ t('forms.name') }
              value={state.name}
              onChange={event => this.setState({
                name: event.target.value,
                nameError: null
              })}
            />
          </div>
          <div className="col-xs-6">
            <input
              id="inputLastName"
              type="text"
              className="form-control"
              placeholder={ t('forms.lastName') }
              value={state.lastName}
              onChange={event => this.setState({
                lastName: event.target.value,
                last_nameError: null
              })}
            />
          </div>
        </div>
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  renderEmail() {
    const { state } = this;
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (state.emailError) {
      className += ' has-error';
      helpBlock = t(state.emailError.i18n);
    }

    return (
      <div className={className}>
        <label htmlFor="inputSitename" className="control-label">
          { t('forms.email') }
        </label>
        <input
          id="inputSitename"
          type="text"
          className="form-control"
          placeholder={ t('forms.email_placeholder') }
          value={state.email}
          onChange={event => this.setState({
            email: event.target.value,
            emailError: null
          })}
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  renderPassword() {
    const { state } = this;
    const { t } = this.props;

    let className = 'form-group';
    let helpBlock = '';

    if (state.passwordError) {
      className += ' has-error';
      helpBlock = t(state.passwordError.i18n);
    }

    return (
      <div className={className}>
        <small className="pull-right">
          <div className="checkbox" style={{ margin: '0' }}>
            <label>
              <input
                type="checkbox"
                tabIndex="-1"
                checked={state.showPassword}
                onChange={event => this.setState({ showPassword: event.target.checked })}
              /> { t('forms.password_show') }
            </label>
          </div>
        </small>
        <label htmlFor="inputSitename" className="control-label">
          { t('forms.password') }
        </label>
        <input
          id="inputSitename"
          type={state.showPassword ? "text" : "password"}
          className="form-control"
          placeholder={ t('sign_up.password_hint') }
          value={state.password}
          onChange={event => this.setState({
            password: event.target.value,
            passwordError: null
          })}
        />
        <p className="help-block">{helpBlock}</p>
      </div>
    );
  }

  render() {
    const { t } = this.props;

    return (
      <div>
        <Header />

        <div className="container">
          <h3>{ t('sign_up.title') }</h3>
          <hr />
          <div className="row">
            <div className="col-sm-6 hidden-xs">
              <div>
                <div className="home-phone full">
                  <div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              {this.state.errors.map(error => (
                <div key={error.i18n} className="alert alert-danger">
                  <i className="material-icons">info</i>
                  &nbsp;
                  { t(error.i18n) }
                </div>
              ))}
              <form onSubmit={this.handleSubmit.bind(this)}>

                {this.renderSubdomain()}
                {this.renderName()}
                {this.renderEmail()}
                {this.renderPassword()}

                <br />
                <Trans i18nKey="sign_up.terms_and_conditions">
                  you accept our <a tabIndex="-1" href="javascript:void(0);">terms and conditions</a>
                </Trans>

                <br />
                <AjaxButton
                  type="submit"
                  loading={this.state.loading}
                  className="btn btn-lg btn-primary"
                  content={t('sign_up.submit')}
                  loadingClassName="btn-loading"
                  loadingContent={t('sign_up.registering')}
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(SignUp);
