import React, { Component } from 'react';

class MobileHeader extends Component {
  renderBar(side) {
    let callback;
    let className = 'navbar-toggle';

    if (side === 'left') {
      callback = this.props.onLeftBarClick;
      className += ' toggle-left';
    } else if (side === 'right') {
      callback = this.props.onRightBarClick;
    } else {
      return null;
    }

    if (typeof callback !== 'function') {
      return null;
    }

    return (
      <div className="ghost-button" style={{ float: side, padding: '15px 5px 15px 10px' }} onClick={callback} >
        <i className="material-icons">menu</i>
      </div>
    );
  }

  render() {
    return (
      <div className="navbar navbar-default navbar-static-top centered">
        {this.renderBar('left')}
        {this.renderBar('right')}

        {this.props.children}
      </div>
    )
  }
}

export default MobileHeader;
