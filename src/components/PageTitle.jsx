import { Component } from 'react';

class PageTitle extends Component {

  constructor(props) {
    super(props);

    this.prevTitle = '';
  }

  componentDidMount() {
    this.prevTitle = document.title;
    document.title = this.props.title + ' - Fleety';
  }

  componentWillUnmount() {
    document.title = this.prevTitle;
  }

  render () { return null; }

}

export default PageTitle;
