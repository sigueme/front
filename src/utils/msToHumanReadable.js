function msToHumanReadable(ms) {
  const notation = [
    { suffix: 'ms', base: 1000 },
    { suffix: 's', base: 60 },
    { suffix: 'm', base: 60 },
    { suffix: 'h', base: 24 },
    { suffix: 'd', base: 365 },
    { suffix: 'y' },
  ];

  const vect = notation.map(el => {
    el.base = el.base || 1e9;

    let units = ms % el.base;
    ms = Math.floor(ms / el.base);
    return units;
  });

  let ix = notation.length;
  while (vect[--ix] == 0 && ix !== 0);

  let string = [];
  string.push(vect[ix] + notation[ix].suffix);

  if (ix > 0) {
    string.push(vect[ix - 1] + notation[ix - 1].suffix);
  }

  return string.join(' ');
}

export default msToHumanReadable;
