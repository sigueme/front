class Events {
  constructor() {
    this.listeners = new Map();
  }

  addEventListener(type, fc) {
    if (this.listeners[type] === undefined) {
      this.listeners[type] = [fc];
    } else {
      this.listeners[type].push(fc);
    }
  }

  removeEventListener(type, fc) {
    if (this.listeners[type] === undefined) {
      return -1;
    }

    const index = this.listeners[type].indexOf(fc);

    if (index != -1) {
      this.listeners[type].splice(index, 1);
    }
  }

  emit(type, data = undefined) {
    if (this.listeners[type] instanceof Array) {
      this.listeners[type].forEach(fc => fc(data));
    }
  }
}

export default Events;
