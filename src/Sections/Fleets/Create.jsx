import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { Sidebar } from '../../Partials/Layout';
import { Form } from '../../Partials/Fleets';
import PageTitle from '../../components/PageTitle';

class Create extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fleet: {
        abbr: '',
        name: ''
      },
      errors: new Map(),
      saving: false
    };
  }

  handleSubmit(event) {
    event.preventDefault();

    if (this.state.saving) {
      return;
    }

    const { fleet } = this.state;

    this.setState({
      saving: true,
      errors: new Map()
    });

    api.post('/v1/:organization/fleet', fleet)
      .then(body => {
        this.props.history.push('/fleets');
      }).catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        let generalErrors = [];

        let state = {
          saving: false,
          errors: new Map()
        };

        errors.forEach(error => {
          if (!error.field) {
            generalerrors.push(error);
            return;
          }

          state.errors.set(error.field, error);
        });

        state.errors.set('general', generalErrors);

        this.setState(state);
      });
  }

  onChange(nextFleet) {
    const { fleet } = this.state;

    if (nextFleet.name !== fleet.name) {
      nextFleet.abbr = nextFleet.name.substr(0, 3).toUpperCase();
    }

    if (nextFleet.abbr !== fleet.abbr) {
      nextFleet.abbr = nextFleet.abbr.toUpperCase();
    }

    this.setState({
      fleet: nextFleet
    });
  }

  render() {
    const { t } = this.props;
    const { fleet, errors } = this.state;

    let alerts = null;
    if (errors.general instanceof Array) {
      alerts = errors.general.map(error => (
        <div key={error.i18n} className="alert alert-danger">
          <i className="material-icons">info</i>
          &nbsp;
          { t(error.i18n) }
        </div>
      ));
    }

    return (
      <div>
        <PageTitle title={t("fleets.create")} />
        <Sidebar />

        <div className="has-sidebar">
          <div className="container container-small">
            <h3>{ t('fleets.create') }</h3>
            <hr />
            {alerts}

            <Form
              model={fleet}
              errors={errors}
              onChange={this.onChange.bind(this)}
              onSubmit={this.handleSubmit.bind(this)}
              onCancel={event => this.props.history.goBack()}
              saving={this.state.saving}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(Create);
