import React, { Component } from 'react';
import GoogleMapsLoader from 'google-maps';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import moment from 'moment';

import settings from '../../settings';
import msToHumanReadable from '../../utils/msToHumanReadable';
import { Sidebar } from '../../Partials/Layout';

class Read extends Component {
  constructor(props) {
    super(props);

    this.polyline = null;
    this.markers = [];

    this.state = {
      selectedEvent: undefined
    };
  }

  componentDidMount() {
    const { maps } = this.props;

    const map = this.map = new maps.Map(this.refs.map, {
      center: { lat: 24.043, lng: -103.861 },
      zoom: 15,
      options: {
        streetViewControl: true,
        mapTypeControl: true,
        fullscreenControl: false,
        mapTypeControlOptions: {
          style: 2,
          position: 3
        }
      }
    });

    this.resizeMap = this.resizeMap.bind(this);
    maps.event.addDomListener(window, 'resize', this.resizeMap);

    // Generate polyline icons
    const polylineIcons = [{
      icon: {
        path: 'M 0,0 0,2',
        strokeOpacity: 0.75,
        scale: 5
      },
      offset: '0',
      repeat: '20px'
    }];

    // Generate polyline
    this.polyline = new maps.Polyline({
      strokeColor: '#37bc9b',
      strokeOpacity: 0,
      icons: polylineIcons,
      map: map,
    });

    // Animate polyline
    this.keepAnimation = true;
    let stepAnimation = () => {
      let now = new Date();
      polylineIcons[0].offset = (now.getMilliseconds() / 1000) * 20 + 'px';
      this.polyline.set('icons', polylineIcons);

      if (this.keepAnimation) {
        window.requestAnimationFrame(stepAnimation);
      }
    };
    stepAnimation();

    this.displayPolyline(true);
    this.displayMarkers();
  }

  componentDidUpdate() {
    this.displayPolyline(true);
    this.displayMarkers();
  }

  componentWillUnmount() {
    this.keepAnimation = false;
  }

  resizeMap() {
    const { map } = this;
    const { maps } = this.props;

    const center = map.getCenter();
    maps.event.trigger(map, 'resize');
    map.setCenter(center);
  }

  setRange(index) {
    if (this.state.selectedEvent === index) {
      index = undefined;
    }

    this.setState({
      selectedEvent: index
    });
  }

  displayPolyline(adjustMap = false) {
    const { map } = this;
    const { maps } = this.props;
    const { clusters, events } = this.props.trip.virtuals;

    let path;
    if (this.state.selectedEvent !== undefined) {
      const event = events[this.state.selectedEvent];

      if (event.type === 'driving') {
        path = clusters
          .filter((c, index) => event.head <= index && index <= event.tail)
          .map(pos => new Object({ lat: pos.lat, lng: pos.lon }));
      } else if (event.type === 'stop') {
        const cluster = clusters[event.ref];

        path = [{
          lat: cluster.lat,
          lng: cluster.lon,
        }];
      }
    } else {
      path = clusters
        .filter(cluster => cluster.lat && cluster.lon)
        .map(cluster => new Object({ lat: cluster.lat, lng: cluster.lon }));
    }

    this.polyline.setPath(path);

    if (adjustMap) {
      let bounds = new maps.LatLngBounds();
      path.forEach(pos => bounds.extend(pos));
      map.fitBounds(bounds);
    }
  }

  displayMarkers() {
    const { map } = this;
    const { trip, maps } = this.props;
    const { clusters } = trip.virtuals;

    // Filter rendered clusters
    let { events } = trip.virtuals;
    if (this.state.selectedEvent !== undefined) {
      const { selectedEvent } = this.state;
      const event = events[selectedEvent];

      if (event.type === 'stop') {
        events = [event];
      } else if (event.type === 'driving') {
        events = [
          events[selectedEvent - 1],
          events[selectedEvent + 1],
        ];
      }
    }

    this.markers.forEach(marker => {
      marker.setMap(null);
      marker = null;
    });

    this.markers = events
      .filter(event => event.type === 'stop')
      .map(event => {
        const pos = clusters[event.ref]

        return {
          type: event.type,
          duration: event.duration,
          lat: pos.lat,
          lon: pos.lon,
        };
      })
      .map(position => {
        const marker = new maps.Marker({
          position: {
            lat: position.lat,
            lng: position.lon,
          },
          title: position.type,
          map: map,
        });

        return marker;
      });
  }

  renderEvents() {
    const { trip } = this.props;
    const { selectedEvent } = this.state;
    const { events } = trip.virtuals;

    const clusterList = events.map((event, index) => {

      let className = '';
      if (index === selectedEvent) {
        className = 'active';
      }

      return new Object({
        id: `cluster-${index}`,
        index: index,
        className,
        time: event.start,
        duration: event.duration,
        description: this.props.t('devices.event_' + event.type)
      });
    });

    return (
      <div className="location-events">
        <table className="table table-striped">
          <tbody>
            {clusterList.map(row => (
              <tr
                key={row.id}
                className={row.className}
                onClick={() => this.setRange(row.index)}
              >
                <td>{moment(row.time).format('YYYY-MM-DD HH:mm')}</td>
                <td title={msToHumanReadable(row.duration)}>{moment.duration(row.duration).humanize()}</td>
                <td title={row.description}>{row.description}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }

  renderMap() {
    return (
      <div className="full-height" ref="map" />
    );
  }

  render() {
    const {
      trip,
      t
    } = this.props;
    const { device } = trip;

    return (
      <div id="map-viewport" className="has-sidebar">
        <Sidebar />

        <div id="omnibox">
          <div>
            <div className="widget-box mint">
              <div className="widget-padding">
                <h4 className="widget-title">
                  {device.name || device.code}
                  &nbsp;
                  <Link
                    title={t('commons.share')}
                    to={`/trip/${trip.hashid}/track`}
                    style={{ color: 'white' }}
                  >
                    <i className="material-icons">share</i>
                  </Link>
                </h4>
              </div>
            </div>

            <div className="widget-box">
              <div className="widget-padding">
                <small>Eventos</small>
              </div>

              {this.renderEvents()}
            </div>
          </div>
        </div>

        {this.renderMap()}
      </div>
    );
  }
}

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      maps: null
    };
  }

  componentDidMount() {
    GoogleMapsLoader.release();
    GoogleMapsLoader.LIBRARIES = [];
    GoogleMapsLoader.KEY = settings.google_maps.map_api_key;
    GoogleMapsLoader.load(google => {
      this.setState({
        maps: google.maps
      });
    });
  }

  render() {
    const { props } = this;

    if (!this.state.maps) {
      return null;
    }

    return (
      <Read
        maps={this.state.maps}
        {...props}
      />
    );
  }
}

export default withTranslation()(Container);
