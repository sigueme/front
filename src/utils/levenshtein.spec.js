import levenshtein from './levenshtein';

describe('Levenshtein', function() {
  it('must find distance', function() {
    expect(levenshtein("holas", "ola")).toEqual(2);
    expect(levenshtein("T12", "T0012")).toEqual(2);
    expect(levenshtein("", "1234")).toEqual(4);
    expect(levenshtein("", "")).toEqual(0);
  });
});
