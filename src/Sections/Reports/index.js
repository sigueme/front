import List from './List.jsx';
import Create from './Create.jsx';

const namespace = new Object({
  List,
  Create,
});

export default namespace;
