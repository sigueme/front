import React, { Component } from 'react';
import { Sidebar } from '../../Partials/Layout';
import { withTranslation } from 'react-i18next';
import {
  Link,
  Switch,
  Route
} from 'react-router-dom';

import MobileHeader from '../../Partials/Layout/MobileHeader';

// Device partials
import Map from '../../Partials/Devices/Map.mobile';
import Update from '../../Partials/Devices/Update';
import Info from '../../Partials/Devices/Info';
import Trips from '../../Partials/Devices/Trips';

class ReadMobile extends Component {
  constructor(props) {
    super(props);

    this.tabs = [{
      key: 'map',
      label: props.t('devices.map_tab'),
    }, {
      key: 'trips',
      label: props.t('devices.trips_tab'),
    }, {
      key: 'info',
      label: props.t('devices.info_tab'),
    }];
  }

  render() {
    const { device, match, location } = this.props;

    let activeTab = location.pathname.split('/').pop();
    if (match.isExact) {
      activeTab = 'map';
    }

    return (
      <div className="has-sidebar" style={{
        height: '100%',
        background: 'white',
        overflow: 'hidden'
      }}>
        <Sidebar />

        <MobileHeader>
          <p className="navbar-text">
            {device.name}
            &nbsp;
            <small className="font-monospace">{device.code}</small>
          </p>
        </MobileHeader>

        <ul className="nav nav-tabs no-navbar-margin">
          {this.tabs.map(tab => (
            <li
              key={tab.key}
              role="presentation"
              className={activeTab === tab.key ? 'active' : ''}
            >
              <Link to={match.url + '/' + tab.key} replace={true}>{tab.label}</Link>
            </li>
          ))}
        </ul>

        <Switch>
          <Route path={match.url + '/update'}>
            <Update device={device} onSave={this.props.reloadDevice} />
          </Route>

          <Route path={match.url + '/info'}>
            <Info device={device} />
          </Route>

          <Route path={match.url + '/trips'}>
            <Trips device={device} />
          </Route>

          <Route path={match.url}>
            <Map device={device} />
          </Route>
        </Switch>
      </div>
    )
  }
}

export default withTranslation()(ReadMobile);
