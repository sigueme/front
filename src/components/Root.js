import React, { Component } from 'react';
import { HashRouter } from 'react-router-dom';
import ToastProvider from './ToastProvider';
import AlarmWatcher from '../utils/AlarmWatcher';

import App from './App.jsx';

// API instance
import API from '../utils/api';
import settings from '../settings';

window.api = new API({
  location: window.location.hostname,
  secure: settings.secure_api,
  subdomain: settings.subdomain,
  hostname: settings.hostname,
});

// Set alarm watcher
const alarmWatcher = new AlarmWatcher();

export default class Root extends Component {
  render() {
    return (
      <HashRouter>
        <ToastProvider>
          <App />
        </ToastProvider>
      </HashRouter>
    );
  }
}
