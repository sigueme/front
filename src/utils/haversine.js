function haversine(lon1, lat1, lon2, lat2) {
  // convert decimal degrees to radians
  const r = 0.017453292519943;
  lon1 *= r;
  lat1 *= r;
  lon2 *= r;
  lat2 *= r;

  // haversine formula
  const dlon = lon2 - lon1;
  const dlat = lat2 - lat1;
  const a = Math.pow(Math.sin(dlat/2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon/2), 2);
  const c = 2 * Math.asin(Math.sqrt(a));
  const m = 6367 * c * 1000;

  return m;
}

export default haversine;
