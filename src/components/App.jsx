import React, { Component } from 'react';
import AuthRoute from '../Routing/AuthRoute';
import HeadRouter from '../Routing/HeadRouter';
import {
  Switch,
  Route
} from 'react-router-dom';

import Home from '../Sections/Home';
import OrgRedirector from '../Routing/OrgRedirector';
import Auth from '../Sections/Auth';
import Geofences from '../Sections/Geofences';
import Routes from '../Sections/Routes';
import Devices from '../Sections/Devices';
import Users from '../Sections/Users';
import Fleets from '../Sections/Fleets';
import Trips from '../Sections/Trips';
import Guides from '../Sections/Guides';
import Logs from '../Sections/Logs';
import RealTime from '../Sections/RealTime';
import Me from '../Sections/Me';
import Settings from '../Sections/Settings';
import Reports from '../Sections/Reports';

import Delete from './Delete';

class App extends Component {
  constructor(props) {
    super(props);

    this.update = this.update.bind(this);
  }

  componentDidMount() {
    api.addEventListener('signin', this.update);
    api.addEventListener('signout', this.update);
    api.addEventListener('change_permissions', this.update);
  }

  componentWillUnmount() {
    api.removeEventListener('signin', this.update);
    api.removeEventListener('signout', this.update);
    api.removeEventListener('change_permissions', this.update);
  }

  update() {
    this.forceUpdate();
  }

  render() {
    if (api.organization === null) {
      return (
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/signin" component={OrgRedirector} />
          <Route exact path="/signup" component={Auth.SignUp} />
          <Route path="/signout" component={Auth.SignOut} />
        </Switch>
      );
    } else {
      return (
        <Switch>
          <AuthRoute exact path="/" private={HeadRouter} />

          <AuthRoute path="/getting-started" private={Guides.NewDevice} />

          <Route path="/trip/:hash/track" component={Trips.Tracking} />
          <AuthRoute path="/trip/:id/update" private={Trips.Update} />
          <AuthRoute path="/trip/:id/delete" private={Delete} />
          <AuthRoute path="/trip/:id" private={Trips.Read} />
          <AuthRoute path="/trips/new" private={Trips.Create} />
          <AuthRoute path="/trips" private={Trips.List} />

          <AuthRoute path="/geofence/:id/update" private={Geofences.Update} />
          <AuthRoute path="/geofence/:id/delete" private={Delete} />
          <AuthRoute path="/geofence/:id" private={Geofences.Read} />
          <AuthRoute path="/geofences/new" private={Geofences.Create} />
          <AuthRoute path="/geofences" private={Geofences.List} />

          <AuthRoute path="/route/:id/update" private={Routes.Update} />
          <AuthRoute path="/route/:id/delete" private={Delete} />
          <AuthRoute path="/route/:id" private={Routes.Read} />
          <AuthRoute path="/routes/new" private={Routes.Create} />
          <AuthRoute path="/routes" private={Routes.List} />

          <AuthRoute path="/user/:id/update" private={Users.Update} />
          <AuthRoute path="/user/:id/delete" private={Delete} />
          <AuthRoute path="/user/:id" private={Users.Read} />
          <AuthRoute path="/users/new" private={Users.Create} />
          <AuthRoute path="/users" private={Users.List} />

          <AuthRoute path="/fleet/:id/update" private={Fleets.Update} />
          <AuthRoute path="/fleet/:id/delete" private={Delete} />
          <AuthRoute path="/fleet/:id" private={Fleets.Read} />
          <AuthRoute path="/fleets/new" private={Fleets.Create} />
          <AuthRoute path="/fleets" private={Fleets.List} />

          <AuthRoute path="/device/:id/delete" private={Delete} />
          <AuthRoute path="/device/:id" private={Devices.Read} />
          <AuthRoute path="/devices" private={Devices.List} />

          <AuthRoute path="/reports/new" private={Reports.Create} />
          <AuthRoute path="/reports" private={Reports.List} />

          <AuthRoute path="/itinerary" private={Trips.Itinerary} />

          <AuthRoute path="/logs" private={Logs.List} />
          <AuthRoute path="/map" private={RealTime} />

          <AuthRoute path="/me/subscriptions" private={Me.Subscriptions} />

          <AuthRoute path="/settings" private={Settings} />

          <Route path="/signin/:api_key" component={Auth.SignInWithApikey} />

          <AuthRoute path="/signin" public={Auth.SignIn} />
          <AuthRoute path="/forgot-password" public={Auth.ForgotPassword} />

          <Route path="/signout" component={Auth.SignOut} />
        </Switch>
      );
    }
  }
}

export default App;
