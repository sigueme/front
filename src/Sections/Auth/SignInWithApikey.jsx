import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

class SignInWithApiKey extends Component {
  componentDidMount() {
    const { match, history } = this.props;
    const { api_key } = match.params;

    api.signin({ api_key })
      .then(data => {
        // Go ahead
        history.replace('/');
      })
      .catch(errors => {
        // Api key was wrong
        history.replace('/signin');
      })
  }

 render() {
    const { t } = this.props;

    return (
      <div className="container">
        <h4>{t('sign_in.signing')}</h4>
      </div>
    )
  }
};

export default withTranslation()(SignInWithApiKey);
