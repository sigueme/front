import React, { Component } from 'react';

import { Sidebar } from '../../Partials/Layout';
import Loading from '../../Partials/Loading';

import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import moment from 'moment';

import ListItem from '../../Partials/Trips/ListItem';

class Itinerary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      trips: []
    };
  }

  componentDidMount() {
    this.loadTrips();

    setInterval(() => {
      if (this && typeof this.loadTrips === 'function') {
        this.loadTrips();
      }
    }, 1000);
  }

  loadTrips() {
    const { id } = api.user;

    api.get(`/v1/:organization/user/${id}/scheduled_trips`)
      .then(body => {
        this.setState({
          loading: false,
          trips: body.data
        });
      })
      .catch(errors => {
        console.error(errors);
      })
  }

  renderOngoing() {
    const trips = this.state.trips.filter(t => t.status === 'ONGOING');

    if (trips.length === 0) {
      return (
        <div>
          <h4>En curso</h4>
          <div style={{ textAlign: 'center' }}>
            <div>No tienes viajes en curso</div>
            <br />
            <br />
          </div>
        </div>
      );
    }

    return (
      <div>
        <h4>En curso</h4>
        <div className="panel-group">
          {trips.map(trip => <ListItem
            onChange={this.loadTrips.bind(this)}
            key={trip.id}
            item={trip}
          />)}
        </div>
      </div>
    );
  }

  renderScheduled() {
    const trips = this.state.trips.filter(t => t.status === 'SCHEDULED');

    if (trips.length === 0) {
      return (
        <div>
          <h4>Programados</h4>
          <div style={{ textAlign: 'center' }}>
            <div>No tienes viajes programados</div>
            <br />
            <br />
          </div>
        </div>
      );
    }

    return (
      <div>
        <h4>Programados</h4>
        <div className="panel-group">
          {trips.map(trip => <ListItem
            onChange={this.loadTrips.bind(this)}
            key={trip.id}
            item={trip}
          />)}
        </div>
      </div>
    );
  }

  render() {
    if (this.state.loading) {
      return <Loading />;
    }

    const { trips } = this.state;

    return (
      <div>
        <Sidebar />

        <div className="has-sidebar">
          <div className="container">
            <div className="list-header">
              <div className="title">Itinerario</div>
              <div className="clearfix"></div>
            </div>

            <br />

            {this.renderOngoing()}
            {this.renderScheduled()}
          </div>
        </div>
      </div>
    )
  }
}

export default withTranslation()(Itinerary);
