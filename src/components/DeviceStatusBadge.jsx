import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

class DeviceStatusBadge extends Component {

  render() {
    const { device, t } = this.props;

    const statusLabels = {
      offline: 'label label-darkgray',
      moving: 'label label-mint',
      stopped: 'label label-grapefruit',
      alarm: 'label label-sunflower',
    };

    let content = t('statuses.device.' + device.status);

    if (device.status == 'alarm') {
      content += ` (${device.alarm_type})`;
    }

    return (
      <span className={statusLabels[device.status]}>{content}</span>
    );
  }

}

export default withTranslation()(DeviceStatusBadge);
