import PermissionTree from './permissionTree';

describe('PermissionTree', function() {
  it('should answer queries', function() {
    const instance = new PermissionTree([
      'a/view',
      'a:b:c',
    ]);

    expect(instance.allowedActions('a:b')).toMatchObject(['view']);
    expect(instance.allowedActions('a:b:c')).toMatchObject(['admin']);
  });

  it('should answer queries with custom verbs', function() {
    const instance = new PermissionTree([
      'a/read',
      'a:b/create',
      'a:b:c',
    ]);

    expect(instance.allowedActions('a')).toMatchObject(['read']);
    expect(instance.allowedActions('a:b:d')).toMatchObject(['read', 'create']);
    expect(instance.allowedActions('a:b:c')).toMatchObject(['admin']);
  });

  it('should answer queries by common prefix', function() {
    const instance = new PermissionTree(['a/view']);

    expect(instance.allowedActions('a:b:c:d:e:f:g:h:i:j')).toMatchObject(['view']);
  });

  it('should count direct childs', function() {
    const instance = new PermissionTree([
      'a/view',
      'a:b:c',
      'a:b:d',
    ]);

    const node = instance.getNode('a:b');
    expect(node.childs.size).toEqual(2);

    const res = instance.hasChilds('a:b');
    expect(res).toBeTruthy();
  });
});
