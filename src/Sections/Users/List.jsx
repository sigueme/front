import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

import { Sidebar } from '../../Partials/Layout';
import Loading from '../../Partials/Loading';
import Errors from '../Errors';
import PageTitle from '../../components/PageTitle';

import ListItem from '../../Partials/Users/ListItem';

class List extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      items: []
    };
  }

  componentDidMount() {
    api.get('/v1/:organization/user')
      .then(body => {
        const items = body.data.map(user => {
          user.permissions = api.permissions.allowedActions(api.organization + ':' + user.type + ':' + user.id);

          return user;
        });
        const visibleItems = items.filter(user => user.permissions.length);

        this.setState({
          loading: false,
          items: visibleItems
        });
      })
      .catch(errors => {
        if (!(errors instanceof Array)) {
          errors = [errors];
        }

        this.setState({
          loading: false,
          errors: errors
        });
      });
  }

  render() {
    if (this.state.loading) {
      return <Loading />
    }

    if (this.state.errors) {
      return <Errors errors={this.state.errors} />
    }

    const { t } = this.props;
    const { items } = this.state;

    const columns = [{
      Header: t('forms.name'),
      accessor: 'attributes.name'
    }, {
      Header: t('forms.name'),
      accessor: 'attributes.name'
    }, {
      Header: t('forms.name'),
      accessor: 'attributes.name'
    }, {
      Header: t('forms.name'),
      accessor: 'attributes.name'
    }];

    let createLink = null;
    if (api.permissions.allowedActions(api.organization + ':user').indexOf('admin') !== -1) {
      createLink = (
        <div className="actions">
          <Link to="/users/new" className="btn btn-lg btn-mint">{ t('users.create') }</Link>
        </div>
      );
    }

    return (
      <div>
        <PageTitle title={t("users.title")} />
        <Sidebar />

        <div className="has-sidebar">
          <div className="container">
            <div className="list-header">
              <div className="title">{ t('users.title') }</div>
              {createLink}
              <div className="clearfix"></div>
            </div>

            <br />

            <div className="panel-group">
              {this.state.items.map(item => <ListItem
                key={item.id}
                item={item}
              />)}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withTranslation()(List);
