import Read from './Read.jsx';
import Tracking from './Tracking.jsx';
import List from './List.jsx';
import Create from './Create.jsx';
import Update from './Update.jsx';
import Itinerary from './Itinerary.jsx';

const namespace = new Object({
  Create,
  Itinerary,
  List,
  Read,
  Tracking,
  Update,
});

export default namespace;
