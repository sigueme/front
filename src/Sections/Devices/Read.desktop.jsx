import React, { Component } from 'react';
import GoogleMapsLoader from 'google-maps';
import { withTranslation } from 'react-i18next';
import {
  Link,
  Switch,
  Route
} from 'react-router-dom';
import moment from 'moment';

import settings from '../../settings';
import { Sidebar } from '../../Partials/Layout';
import DeviceMarkerBuilder from '../../utils/DeviceMarker';

// Device partials
import Update from '../../Partials/Devices/Update';
import Info from '../../Partials/Devices/Info';
import Trips from '../../Partials/Devices/Trips';
import History from '../../Partials/Devices/History';

class ReadDesktop extends Component {
  constructor(props) {
    super(props);

    const { t } = props;

    this.tabs = [{
      key: 'info',
      label: t('devices.info_tab'),
    }, {
      key: 'history',
      label: t('devices.history_tab'),
    }, {
      key: 'trips',
      label: t('devices.trips_tab'),
    }];

    this.state = {
      map_master: true
    };
  }

  componentDidMount() {
    const { maps, device, t } = this.props;
    const DeviceMarker = DeviceMarkerBuilder(maps, t);

    const last_pos = new maps.LatLng(device.last_pos.lat, device.last_pos.lon);

    const map = this.map = new maps.Map(this.refs.map, {
      center: last_pos,
      zoom: 15,
      options: {
        streetViewControl: true,
        mapTypeControl: true,
        fullscreenControl: false,
        mapTypeControlOptions: {
          style: 2,
          position: 3
        }
      }
    });

    this.resizeMap = this.resizeMap.bind(this);
    maps.event.addDomListener(window, 'resize', this.resizeMap);

    const marker = this.marker = new DeviceMarker({
      id: device.id,
      name: device.name || device.code,
      status: device.status,
      course: device.last_course,
      last_speed: device.last_speed,
      last_report: moment(new Date(device.last_update)),
      visibility: 'visible',

      map: map,
      position: last_pos,
    });
  }

  componentDidUpdate() {
    if (this.state.map_master) {
      const { device, maps } = this.props;

      const last_pos = new maps.LatLng(device.last_pos.lat, device.last_pos.lon);
      this.marker.setOptions({
        id: device.id,
        name: device.name || device.code,
        status: device.status,
        course: device.last_course,
        last_speed: device.last_speed,
        last_report: moment(new Date(device.last_update)),
        visibility: 'visible',
      });
      this.marker.setPosition(last_pos);
    }
  }

  componentWillUnmount() {
    const { maps } = this.props;
    maps.event.clearListeners(window, 'resize');
  }

  resizeMap() {
    const { map } = this;
    const { maps } = this.props;

    const center = map.getCenter();
    maps.event.trigger(map, 'resize');
    map.setCenter(center);
  }

  render() {
    const { device, match, location } = this.props;
    const { name, code } = device;

    let activeTab = location.pathname.split('/').pop();
    if (match.isExact) {
      activeTab = 'info';
    }

    return (
      <div id="map-viewport" className="has-sidebar">
        <Sidebar />

        <div id="omnibox">
          <div className="widget-box mint">
            <div className="widget-padding">
              <h4 className="widget-title">
                { name || code }
              </h4>
            </div>
          </div>

          <div className="widget-box">
            <ul className="nav nav-tabs">
              {this.tabs.map(tab => (
                <li
                  key={tab.key}
                  role="presentation"
                  className={activeTab === tab.key ? 'active' : ''}
                >
                  <Link to={match.url + '/' + tab.key} replace={true}>{tab.label}</Link>
                </li>
              ))}
            </ul>

            <Switch>
              <Route path={match.url + '/update'}>
                <Update device={device} onSave={this.props.reloadDevice} />
              </Route>

              <Route path={match.url + '/trips'}>
                <Trips device={device} />
              </Route>

              <Route path={match.url + '/history'}>
                <History
                  map={this.map}
                  maps={this.props.maps}
                  device={device}
                />
              </Route>

              <Route path={match.url}>
                <Info device={device} />
              </Route>
            </Switch>
          </div>
        </div>

        <div className="full-height" ref="map" />
      </div>
    );
  }
}

class Container extends Component {
  constructor(props) {
    super(props);

    this.state = {
      maps: null
    };
  }

  componentDidMount() {
    GoogleMapsLoader.release();
    GoogleMapsLoader.LIBRARIES = [];
    GoogleMapsLoader.KEY = settings.google_maps.map_api_key;
    GoogleMapsLoader.load(google => {
      this.setState({
        maps: google.maps
      });
    });
  }

  render() {
    const { props } = this;

    if (!this.state.maps) {
      return null;
    }

    return (
      <ReadDesktop
        maps={this.state.maps}
        {...props}
      />
    );
  }
}

export default withTranslation()(Container);
