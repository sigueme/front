import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class SearchItem extends Component {
  render() {
    const { model } = this.props;

    return (
      <Link to={`/fleet/${model.id}`} className="search-item">
        <span className="material-icons">group_work</span>
        &nbsp;
        {model.abbr} {model.name}
      </Link>
    );
  }
}

export default SearchItem;
