import kdt from 'kd.tree';
import haversine from './haversine';
import Cluster from './Cluster';

class ClusterEngine {
  constructor(props) {
    this.props = props;

    this.tree = kdt.createKdTree([], (a,b) => haversine(a.lon, a.lat, b.lon, b.lat), ['lat', 'lon']);

    const viewportRef = new props.maps.OverlayView();
    viewportRef.draw = function() {};
    viewportRef.setMap(props.map);
    this.viewportRef = viewportRef;

    this.lastClusterId = -1;
    this.clusters = new Map();
    this.parentCluster = new Map();

    if (props.devices instanceof Array) {
      props.devices.forEach(device => this.upsert(device));
    }
  }

  destructor() {
    const clusterIterator = this.clusters.entries();

    for (let [ix, cluster] of clusterIterator) {
      cluster.destructor();
    }

    this.clusters = null;
    this.tree = null;
    this.viewportRef = null;
    this.devCluster = null;
  }

  viewportDistance(a, b) {
    return Math.abs(a.x-b.x) + Math.abs(a.y-b.y);
  }

  calcPixelDistance(P1, P2) {
    const { maps } = this.props;
    const projection = this.viewportRef.getProjection();

    let distance;
    if (projection !== undefined) {
      let A = new maps.LatLng(P1.lat, P1.lon);
      A = projection.fromLatLngToDivPixel(A);

      let B = new maps.LatLng(P2.lat, P2.lon);
      B = projection.fromLatLngToDivPixel(B);

      distance = Math.abs(A.x - B.x) + Math.abs(A.y - B.y);
    } else {
      distance = 1e9;
    }

    return distance;
  }

  upsert(device, options) {
    if (this.parentCluster.has(device.id)) {
      this.update(device, options);
    } else {
      this.insert(device, options);
    }
  }

  insert(device, options) {
    if (this.parentCluster.has(device.id)) {
      return this.parentCluster.get(device.id);
    }

    let doRender = true;
    if (options && options.render) {
      doRender = options.render;
    }

    const [ nearest ] = this.tree.nearest({
      lat: device.last_pos.lat,
      lon: device.last_pos.lon,
    }, 1);

    let distance;
    if (nearest !== undefined) {
      distance = this.calcPixelDistance(device.last_pos, nearest[0]);
    } else {
      distance = 1e9;
    }

    if (distance < 50) {
      let node = nearest[0];

      // Fetch cluster
      const cluster = this.clusters.get(node.id);

      // Insert device into cluster
      cluster.insert(device);

      // Set cluster/device references
      this.parentCluster.set(device.id, cluster);

      if (doRender) {
        cluster.render();
      }

      return cluster;
    } else {
      // Build cluster
      const cluster = new Cluster({
        maps: this.props.maps,
        map: this.props.map,
        t: this.props.t,
        tree: this.tree
      });
      cluster.id = ++this.lastClusterId;

      // Insert device into cluster
      cluster.insert(device);

      cluster.addEventListener('click', () => {
        // Zoom into cluster
        const { maps, map } = this.props;

        let bounds = new maps.LatLngBounds();
        for (device of cluster._devices.values()) {
          bounds.extend({
            lat: device.last_pos.lat,
            lng: device.last_pos.lon,
          });
        }

        map.fitBounds(bounds);
        maps.event.trigger(map, 'zoom_changed');
      });

      // Set cluster/device references
      this.parentCluster.set(device.id, cluster);
      this.clusters.set(cluster.id, cluster);

      if (doRender) {
        cluster.render();
      }

      return cluster;
    }
  }

  update(device, options) {
    if (!this.parentCluster.has(device.id)) {
      return;
    }

    let doRender = true;
    if (options && options.render) {
      doRender = options.render;
    }

    // Update parent cluster and tree ref
    let cluster = this.parentCluster.get(device.id);
    cluster.update(device);

    // Device went to far from cluster
    const distance = this.calcPixelDistance(device.last_pos, cluster.center());

    if (distance < 40) {
      // Everything it's ok
      if (doRender) {
        cluster.render();
      }
    } else {
      // Separate cluster
      cluster.remove(device);
      this.parentCluster.delete(device.id);
      if (cluster._count === 0) {
        cluster.destructor();
        cluster = null;
      } else {
        if (doRender) {
          cluster.render();
        }
      }

      cluster = this.insert(device, { render: false });
      if (doRender) {
        cluster.render();
      }
    }

    // If two clusters are close enough, merge
    const nearests = this.tree.nearest(cluster.kdtree_node, 2);
    if (nearests && nearests.length === 2) {
      let ref = 0;
      if (nearests[0][0].id === cluster.id) {
        ref = 1;
      }

      let [ nearClusterNode ] = nearests[ref];
      let nearCluster = this.clusters.get(nearClusterNode.id);

      if (nearCluster === undefined) {
        // this sould not happend
        log.error(`kd-tree cluster ref: ${nearClusterNode.id} is inconsistent, removing`);

        this.tree.remove(nearClusterNode);
        return;
      }

      const distance = this.calcPixelDistance(cluster.center(), nearCluster.center());
      if (distance < 65) {
        /* merge clusters */

        // Update merged cluster center
        const cen1 = cluster._center;
        const cnt1 = cluster._count;

        const cen2 = nearCluster._center;
        const cnt2 = nearCluster._count;

        cluster._center.lat = (cen1.lat * cnt1 + cen2.lat * cnt2) / (cnt1 + cnt2);
        cluster._center.lon = (cen1.lon * cnt1 + cen2.lon * cnt2) / (cnt1 + cnt2);

        // Update merged cluster summary
        for (let attr in nearCluster._summary) {
          cluster._summary[attr] += nearCluster._summary[attr];
        }

        // Update merged cluster count
        cluster._count = cnt1 + cnt2;

        // Move devices to merged cluster
        let nearDevices = nearCluster._devices.entries();
        for (let [ deviceId, model ] of nearDevices) {
          cluster._devices.set(deviceId, model);
          this.parentCluster.delete(deviceId);
          this.parentCluster.set(deviceId, cluster);
        }

        this.clusters.delete(nearCluster.id);
        nearCluster.destructor();
        nearCluster = null;

        cluster.syncTree();
        if (doRender) {
          cluster.render();
        }
      }
    }
  }

  render() {
    this.clusters.forEach(c => c.render());
  }
}

export default ClusterEngine;
