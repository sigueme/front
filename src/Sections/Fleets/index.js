import List from './List';
import Read from './Read';
import Create from './Create';
import Update from './Update';

const namespace = new Object({
  List,
  Read,
  Create,
  Update,
});

export default namespace;
