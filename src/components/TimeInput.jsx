import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

class TimeInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.value,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      value: nextProps.value,
    });
  }

  parse(time) {
    time = time.replace(new RegExp(/[^0-9:]/g), "");

    if (time.length > 5) {
      time = time.substr(0, 5);
    }

    let matches = null;

    do {
      matches = time.match(new RegExp(/^[0-9][0-9]?:?([0-5][0-9]?)?$/g));

      if (matches === null) {
        time = time.substr(0, time.length-1);
      }
    } while (!matches && time.length > 0);

    if (time.length > 0) {
      let pieces = time.split(':').filter(x => x);

      if (parseInt(pieces[0]) > 23) {
        time = '';
      }
    }

    return time;
  }

  handleChange(event) {
    let time = this.parse(event.target.value);

    this.setState({ value: time });
  }

  handleBlur(event) {
    let time = this.parse(event.target.value);

    this.props.onChange(time);
  }

  render() {
    return (
      <input
        type="text"
        className="form-control"
        placeholder='HH:MM'
        value={this.state.value}
        onChange={this.handleChange.bind(this)}
        onBlur={this.handleBlur.bind(this)}
      />
    );
  }
}

export default withTranslation()(TimeInput);
