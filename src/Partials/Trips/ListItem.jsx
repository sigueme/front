import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import moment from 'moment';

import settings from '../../settings';

class ListItem extends Component {
  getTitle() {
    const { item } = this.props;

    let tripName = [];
    if (item.origin) {
      tripName.push(item.origin);
    }

    if (item.destination) {
      tripName.push(item.destination);
    }

    return (
      <h5>{tripName[0]} <i className="material-icons">arrow_forward</i> {tripName[1]}</h5>
    );
  }

  getSchedule() {
    const { item } = this.props;

    if (!item.scheduled_departure || !item.scheduled_arrival) {
      return null;
    }

    const departure = moment(item.scheduled_departure);
    const arrival = moment(item.scheduled_arrival);
    const diff = arrival.diff(departure);

    let content;
    if (diff <= 86400000 && departure.date() === arrival.date()) {
      content = departure.format('lll') + ' – ' + arrival.format('LT');
    } else {
      content = departure.format('lll') + ' – ' + arrival.format('lll');
    }

    return (
      <div><i className="material-icons">event</i> {content}</div>
    );
  }

  getDevice() {
    const { device } = this.props.item;

    if (!device) {
      return null;
    }

    return (
      <div><i className="material-icons">directions_car</i> {device.name || device.code}</div>
    );
  }

  startTrip() {
    const { item } = this.props;

    api.put(`/v1/:organization/trip/${item.id}/control`, {
      status: 'ONGOING'
    }).then(res => {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(res.data);
      }
    });
  }

  finishTrip() {
    const { item } = this.props;

    api.put(`/v1/:organization/trip/${item.id}/control`, {
      status: 'FINISHED'
    }).then(res => {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(res.data);
      }
    });
  }

  renderFooter() {
    const { item, t } = this.props;
    const { status } = item;

    let actions = [];
    if (status === 'ONGOING' || status === 'FINISHED') {
      actions.push(
        <Link key="trip-read" to={`/trip/${item.id}`} className="action">
          <i className="material-icons">arrow_forward</i>
          &nbsp;
          {t('trips.open')}
        </Link>
      );
    }

    if (status === 'SCHEDULED') {
      if (item.device_id) {
        actions.push(
          <li key="trip-start" className="action" onClick={this.startTrip.bind(this)}>
            <i className="material-icons">play_circle_outline</i>
            &nbsp;
            {t('trips.start')}
          </li>
        );
      }
    } else if (status === 'ONGOING') {
      actions.push(
        <li key="trip-finish" className="action" onClick={this.finishTrip.bind(this)}>
          <i className="material-icons">pause_circle_outline</i>
          &nbsp;
          {t('trips.finish')}
        </li>
      );
    }

    const perm = `${api.organization}:trip:${item.id}`;
    const allowedActions = api.permissions.allowedActions(perm);

    if (allowedActions.indexOf('admin') !== -1) {
      actions.push(
        <Link key="trip-update" to={`/trip/${item.id}/update`} className="action">
          <i className="material-icons">mode_edit</i>
          &nbsp;
          {t('commons.update')}
        </Link>
      );
      actions.push(
        <Link key="trip-delete" to={`/trip/${item.id}/delete`} className="action">
          <i className="material-icons">delete</i>
          &nbsp;
          {t('commons.delete')}
        </Link>
      );
    }

    return (
      <ul className="footer-actions">{actions}</ul>
    );
  }

  renderStatus() {
    const { item, t } = this.props;

    const statusLabels = {
      SCHEDULED: 'label label-info',
      ONGOING: 'label label-success',
      FINISHED: 'label label-primary',
    };

    return (
      <div className="pull-right">
        <span className={statusLabels[item.status]}>{t('statuses.trip.' + item.status)}</span>
      </div>
    );
  }

  renderPolyline() {
    const { item } = this.props;

    if (!item.polyline) {
      return null;
    }

    let thumbnail = 'https://maps.googleapis.com/maps/api/staticmap?size=128x128&sensor=true&path=weight:3|color:0x206D5AAA|enc:';

    thumbnail += encodeURIComponent(item.polyline);
    thumbnail += '&key=' + encodeURIComponent(settings.google_maps.staticmap_api_key);

    return(
      <div className="media-left">
        <img width="128" height="128" src={thumbnail} />
      </div>
    );
  }

  render() {
    const { item } = this.props;

    return (
      <div className="panel panel-default">
        <div className="panel-body">
          <div className="media">
            <div className="media-left">
              {this.renderPolyline()}
            </div>
            <div className="media-body">
              {this.renderStatus()}
              {this.getTitle()}
              {this.getSchedule()}
              {this.getDevice()}
            </div>
          </div>
        </div>
        <div className="panel-footer">
          {this.renderFooter()}
        </div>
      </div>
    );
  }
}

export default withTranslation()(ListItem);
