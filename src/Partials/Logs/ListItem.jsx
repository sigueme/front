import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import moment from 'moment';

class ListItem extends Component {

  renderChannel() {
    const { item } = this.props;

    return item.channel.split(':').slice(1).join(':');
  }

  render_geofence_create(data) {
    const { t } = this.props;

    return (<span>{t('events.geofence_create')} <Link to={`/geofence/${data.id}`}>{data.name}</Link></span>);
  }

  render_fleet_alarm(data) {
    const { t } = this.props;

    return (<span>
      {t('events.fleet_alarm')} <strong>{t('events.alarm.'+data.type)}</strong> {t('events.fleet_alarm_for')} <Link to={`/device/${data.device.id}`}>{data.device.name}</Link>
    </span>);
  }

  render_fleet_geofence_enter(data) {
    const { t } = this.props;

    return (<span>
      {t('events.the_device')} <Link to={`/device/${data.device.id}`}>{data.device.name}</Link> {t('events.has')} <strong>{t('events.entered')}</strong> {t('events.to_the_geofence')} <Link to={`/geofence/${data.geofence.id}`}>{data.geofence.name}</Link>
    </span>);
  }

  render_fleet_geofence_leave(data) {
    const { t } = this.props;

    return (<span>
      {t('events.the_device')} <Link to={`/device/${data.device.id}`}>{data.device.name}</Link> {t('events.has')} <strong>{t('events.left')}</strong> {t('events.from_the_geofence')} <Link to={`/geofence/${data.geofence.id}`}>{data.geofence.name}</Link>
    </span>);
  }

  renderEvent() {
    const { item, t } = this.props;
    const event = item.event.replace('-', '_');
    const channel = item.channel.split(':')[1];

    const function_name = `render_${channel}_${event}`;

    if (typeof this[function_name] == 'function') {
      return this[function_name](item.data);
    }

    return t(`events.${channel}.${event}`, item.data);
  }

  renderCreatedAt() {
    const { item } = this.props;
    const created_at = moment(item.created_at).format('lll');

    return created_at;
  }

  render() {
    const { item } = this.props;
    let titleClassName = "log-title";

    titleClassName += ' ' + item.event;

    return (
      <div className="log-item">
        <div className={titleClassName}>
          {this.renderChannel()}
        </div>
        <div className="log-body">
          {this.renderEvent()}
        </div>
        <div className="log-time">
          {this.renderCreatedAt()}
        </div>
      </div>
    );
  }
}

export default withTranslation()(ListItem);
