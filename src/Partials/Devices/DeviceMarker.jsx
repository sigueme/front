import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import moment from 'moment';

class DeviceMarker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    };
  }

  open() {
    this.setState({ open: true });
  }

  close() {
    this.setState({ open: false });
  }

  renderInfowindow() {
    const { device, t } = this.props;
    const { code, name, status, last_pos } = device;
    const last_update = moment(new Date(device.last_update));

    const googleMapLink = 'https://maps.google.com/maps/place/'+last_pos.lat+','+last_pos.lon;

    const data = [{
      key: 'device-detail',
      title: t('devices.detail'),
      icon: 'arrow_forward',
      value: (
        <Link to={`/device/${device.id}`}>{t('devices.go_to_detail')}</Link>
      ),
    }, {
      key: 'device-status',
      title: t('devices.status'),
      icon: 'info',
      value: t('statuses.device.'+status),
    }, {
      key: 'device-location',
      title: t('devices.location'),
      icon: 'pin_drop',
      value: (
        <a target="_blank" href={googleMapLink}>{last_pos.lat.toFixed(4) + ',' + last_pos.lon.toFixed(4)}</a>
      )
    }, {
      key: 'device-last-report',
      title: t('devices.last_report'),
      icon: 'refresh',
      value: last_update.format('YYYY-MM-DD HH:mm:ss')
    }];

    let lastSpeedValue;
    if ((device.status === 'alarm' || device.status === 'moving') && device.last_speed) {
      lastSpeedValue = (+device.last_speed).toFixed(2) + ' km/hr';
    } else {
      lastSpeedValue = t('commons.not_set') + ' km/hr';
    }

    data.push({
      key: 'device-last-speed',
      title: t('devices.last_speed'),
      icon: 'network_check',
      value: lastSpeedValue,
    });

    let style = {};
    if (device.status === 'moving' &&  device.last_course) {
      style.transform = `rotate(${device.last_course}rad)`;
    }

    return (
      <div>
        <div style={style} className="dot" onClick={this.close.bind(this)} />
        <div className="infowindow">
          <div className="heading">
            <div className="window-close" onClick={this.close.bind(this)}>
              <i className="material-icons">close</i>
            </div>

            <div className="heading-title" title={name || code}>
              {name || code}
            </div>
          </div>
          <div className="info-content">
            {data.map(row => (
              <p key={row.key}>
                <i className="material-icons" title={row.title}>{row.icon}</i>
                &nbsp;
                {row.value}
              </p>
            ))}
          </div>
          <div className="caret" />
        </div>
      </div>
    );
  }

  renderMarker() {
    const { device } = this.props;
    const { code, name } = device;

    let style = {};
    if (device.status === 'moving' &&  device.last_course) {
      style.transform = `rotate(${device.last_course}rad)`;
    }

    return (
      <div>
        <div style={style} className="dot" onClick={this.open.bind(this)} />
        <div className="prev" onClick={this.open.bind(this)}>
          <div className="name" title={name || code}>{name || code}</div>
          <div className="caret" />
        </div>
      </div>
    );
  }

  render() {
    const { device } = this.props;
    const { status } = device;

    const className = [
      'device-marker',
      'status-' + status
    ].join(' ');

    let content;
    if (this.state.open) {
      content = this.renderInfowindow();
    } else {
      content = this.renderMarker();
    }

    return (
      <div className={className}>
        {content}
      </div>
    );
  }
}

export default withTranslation()(DeviceMarker);
