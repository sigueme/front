import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import Loading from '../../Partials/Loading';
import DeviceMap from '../../components/DeviceMap';

class Read extends Component {
  constructor(props) {
    super(props);

    this.state = {
      subscribed: false,
      loading: true,
      items: [],
    }
  }

  componentWillReceiveProps(nextProps) {
    const fleetIds = [this.props, nextProps].map(s => s.match.params.id);

    if (fleetIds[0] !== fleetIds[1]) {
      this.setState({
        loading: false,
        items: []
      });

      this.unsubscribe();
    }
  }

  componentDidMount() {
    this.subscribe();
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  componentDidUpdate() {
    if (!this.state.subscribed) {
      this.subscribe();
    }
  }

  fetchModel(callback) {
    const { id } = this.props.match.params;

    api.get(`v1/:organization/fleet/${id}?embed=devices`)
      .then(body => {
        const fleet = body.data;
        const items = fleet.devices;

        if (items.length === 0) {
          this.props.history.replace(`/fleet/${id}/update`);
          return;
        }

        if (typeof callback === 'function') {
          callback(fleet);
        }

        this.setState({
          loading: false,
          items: items,
        });
      })
      .catch(error => {
        console.error(error);

        this.setState({
          loading: false,
          items: [],
        });
      });
  }

  socketHandler(eventData) {
    const model = eventData.data;

    switch (eventData.event) {
      case 'device-update':
      case 'update':
        this.state.items.forEach(device => {
          if (device.id === model.id) {
            device = Object.assign(device, model);
            device.last_update = new Date(device.last_update);

            log.debug(`Updating device ${device.id} status ${model.status}`);
            if (this.clusters) {
              this.clusters.upsert(device);
            }
          }
        });
        break;
    }
  }

  subscribe() {
    // fetch model
    this.fetchModel(fleet => {
      // Create socket
      this.socket = api.socket(
        [`${api.organization}:fleet:${fleet.id}`],
        this.socketHandler.bind(this)
      );
    });

    // re-fetch model every 5 minutes
    this.subscribeInterval = setInterval(() => {
      this.fetchModel();
    }, 300000);

    this.setState({
      subscribed: true
    });
  }

  unsubscribe() {
    clearInterval(this.subscribeInterval);

    if (this.socket && typeof this.socket.disconnect == 'function') {
      this.socket.disconnect();
    }

    this.setState({
      subscribed: false
    });
  }

  render() {
    if (this.state.loading) {
      return <Loading />
    }

    const { props } = this;
    const { t } = this.props;

    if (this.state.error) {
      return (
        <div className="container container-small list-empty">
          <h2>{ t('errors.http404.title') }</h2>
          { t('errors.http404.description') }
          <br />
          <Link to="/" className="btn btn-lg btn-mint">{ t('errors.http404.button') }</Link>
        </div>
      );
    }

    return (
      <DeviceMap
        items={this.state.items}
        OnClustersLoad={clusters => this.clusters = clusters}
        {...this.props}
      />
    );
  }
}

export default withTranslation()(Read);
