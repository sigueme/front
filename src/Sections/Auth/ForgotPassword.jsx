import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

import { Header } from '../../Partials/Layout';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
    }
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render() {
    const { t } = this.props;

    return (
      <div>
        <Header />

        <div className="container container-micro">
          <div className="panel">
            <div className="panel-body">
              <h3>
                { t('forgot_password.title') }
              </h3>
              <br />
              <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="form-group">
                  <label htmlFor="inputEmail" className="control-label">
                    { t('forms.email') }
                  </label>
                  <input
                    id="inputEmail"
                    type="text"
                    className="form-control with-border"
                    placeholder={ t('forms.email_placeholder') }
                    value={this.state.email}
                    onChange={event => this.setState({ email: event.target.value })}
                  />
                </div>

                <br />
                <div className="pull-right">
                  <button className="btn btn-lg btn-primary">
                    { t('forgot_password.submit') }
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(ForgotPassword);
