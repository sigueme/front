import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

const HeadRouter = (props) => {
  const perms = api.permissions;
  const org = api.organization;

  if (api.user.empty_account) {
    return <Redirect to="/getting-started" />
  }

  if (perms.allowedActions(org + ':fleet').length) {
    return <Redirect to="/map" />
  }

  const fleetNode = perms.getNode(org + ':fleet');

  if (fleetNode !== null) {
    const size = fleetNode.childs.size;
    if (size === 1) {
      let iter = fleetNode.childs.keys();
      return <Redirect to={'/fleet/'+iter.next().value} />
    } else if (size > 1) {
      return <Redirect to="/fleets" />
    }
  }

  if (perms.allowedActions(org + ':user').length) {
    return <Redirect to="/users" />
  }

  if (perms.allowedActions(org + ':geofence').length) {
    return <Redirect to="/geofences" />
  }

  return <Redirect to="/itinerary" />
}

export default HeadRouter
